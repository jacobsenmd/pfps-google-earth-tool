/********************************************************************************/
/* Class:   GET.Core.Statics                                                    */
/*                                                                              */
/* Statics is a static class that contains miscallaneous static data.           */
/*                                                                              */
/* Public Properties:                                                           */
/*  int DefaultAltitudeAGL      A default AGL altitude to use for any           */
/*                              GETObject that specifies an altitude            */
/*  int DefaultAltitudeMSL      A default MSL altitude to use for any           */
/*                              GETObject that specifies an altitude            */
/*  AltType DefaultAltitudeType The default type of altitude to use for any     */
/*                              GETObject that specifies an altitude            */
/********************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace GET.Core
{
    public static class Statics
    {
        public static int DefaultAltitudeAGL = 500;
        public static int DefaultAltitudeMSL = 3000;
        public static string PFPSPath = @"C:\PFPS";
        public static string GoogleEarthPath = @"C:\Program Files\Google\Google Earth";
        public static string ApplicationPath = "";
        public static AltType DefaultAltitudeType = AltType.ClampToGround;
        public static int CoordinateStyle = 2;
    }
}
