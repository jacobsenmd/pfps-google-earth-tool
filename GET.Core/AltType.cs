/********************************************************************************/
/* Enum:   GET.Core.AltType                                                     */
/*                                                                              */
/* This enumerates the different types of altitudes Google Earth can use:       */
/*                                                                              */
/*  ClampToGround           Points/paths stay glued to terrain.  Automatically  */
/*                          corrects for curvature of the earth, unlike other   */
/*                          settings.                                           */
/*  FixedAGL                Each point is rendered at a fixed height above the  */
/*                          ground.  Path lines between points are drawn in a   */
/*                          straight line, so may clip through terrain over     */
/*                          long distances.                                     */
/*  FixedMSL                Each point is rendered at a fixed height above sea  */
/*                          level.  Path lines between points are drawn in a    */
/*                          straight line, so may clip through terrain over     */
/*                          long distances.                                     */
/********************************************************************************/

namespace GET.Core
{
    public enum AltType { ClampToGround, FixedAGL, FixedMSL };
}