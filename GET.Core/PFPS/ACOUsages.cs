using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GET.Core.PFPS
{
    public class ACOUsages
    {
        private int NumUsages = 116;
        public ACOUsage[] Usages;

        public void Initialize()
        {
            Usages = new ACOUsage[NumUsages];
            Usages[0].Initialize("AAR", "Air-to-Air Refuel Area", Color.Red, Color.DarkRed);
            Usages[1].Initialize("ABC", "Airborne Command & Control Area", Color.Red, Color.DarkRed);
            Usages[2].Initialize("ACA", "Airspace Coordination Area", Color.Magenta, Color.DarkMagenta);
            Usages[3].Initialize("ACP", "Air Control Point", Color.Cyan, Color.DarkCyan);
            Usages[4].Initialize("ACSS", "Airspace Control Subarea/Sector", Color.Yellow, Color.Gold);
            Usages[5].Initialize("ADAA", "Air Defense Action Area", Color.Green, Color.DarkGreen);
            Usages[6].Initialize("ADIZ", "Air Defense Identification Zone", Color.Orange, Color.DarkOrange);
            Usages[7].Initialize("ADVRTE", "Advisory Route", Color.Blue, Color.DarkBlue);
            Usages[8].Initialize("ADZ", "Amphibious Defense Zone", Color.Green, Color.DarkGreen);
            Usages[9].Initialize("AEW", "Airborne Early Warning", Color.Red, Color.DarkRed);
            Usages[10].Initialize("AIRCOR", "Air Corridor", Color.SkyBlue, Color.SkyBlue);
            Usages[11].Initialize("AIRRTE", "Air Route", Color.SkyBlue, Color.SkyBlue);
            Usages[12].Initialize("ALERTA", "Alert Area", Color.Yellow, Color.Gold);
            Usages[13].Initialize("ALTRV", "Altitude Reservation", Color.Magenta, Color.DarkMagenta);
            Usages[14].Initialize("AOA", "Amphibious Objective Area", Color.Green, Color.DarkGreen);
            Usages[15].Initialize("APPCOR", "Approach Corridor", Color.Green, Color.DarkGreen);
            Usages[16].Initialize("ARWY", "Airway", Color.Blue, Color.DarkBlue);
            Usages[17].Initialize("ASCA", "Airspace Control Area", Color.Yellow, Color.Gold);
            Usages[18].Initialize("ATSRTE", "ATS Route", Color.Blue, Color.DarkBlue);
            Usages[19].Initialize("BDZ", "Base Defense Zone", Color.Orange, Color.DarkOrange);
            Usages[20].Initialize("BNDRY", "Boundary", Color.Magenta, Color.DarkMagenta);
            Usages[21].Initialize("BULL", "Bullseye", Color.Cyan, Color.DarkCyan);
            Usages[22].Initialize("BZ", "Buffer Zone", Color.Orange, Color.DarkOrange);
            Usages[23].Initialize("CADA", "Coordinated Air Defense Area", Color.Green, Color.DarkGreen);
            Usages[24].Initialize("CAP", "Combat Air Patrol", Color.Red, Color.DarkRed);
            Usages[25].Initialize("CAS", "Close Air Support Holding Area", Color.Red, Color.DarkRed);
            Usages[26].Initialize("CBA", "Cross-Border Area", Color.Blue, Color.DarkBlue);
            Usages[27].Initialize("CCZONE", "Carrier Control Zone", Color.Green, Color.DarkGreen);
            Usages[28].Initialize("CDR", "Conditional Route", Color.Blue, Color.DarkBlue);
            Usages[29].Initialize("CFL", "Coordinated Fire Line", Color.Magenta, Color.DarkMagenta);
            Usages[30].Initialize("CL", "Coordination Level", Color.Magenta, Color.DarkMagenta);
            Usages[31].Initialize("CLSA", "Class A Airspace", Color.Blue, Color.DarkBlue);
            Usages[32].Initialize("CLSB", "Class B Airspace", Color.Blue, Color.DarkBlue);
            Usages[33].Initialize("CLSC", "Class C Airspace", Color.Blue, Color.DarkBlue);
            Usages[34].Initialize("CLSD", "Class D Airspace", Color.Blue, Color.DarkBlue);
            Usages[35].Initialize("CLSE", "Class E Airspace", Color.Blue, Color.DarkBlue);
            Usages[36].Initialize("CLSF", "Class F Airspace", Color.Blue, Color.DarkBlue);
            Usages[37].Initialize("CLSG", "Class G Airspace", Color.Blue, Color.DarkBlue);
            Usages[38].Initialize("CONTZN", "Control Zone", Color.Blue, Color.DarkBlue);
            Usages[39].Initialize("COZ", "Crossover Zone", Color.Green, Color.DarkGreen);
            Usages[40].Initialize("CP", "Contact Point", Color.Cyan, Color.DarkCyan);
            Usages[41].Initialize("CTA", "Control Area", Color.Blue, Color.DarkBlue);
            Usages[42].Initialize("DA", "Danger Area", Color.Blue, Color.DarkBlue);
            Usages[43].Initialize("DBSL", "Deep Battle Synchronization Line", Color.Magenta, Color.DarkMagenta);
            Usages[44].Initialize("DZ", "Drop Zone", Color.Red, Color.DarkRed);
            Usages[45].Initialize("EC", "Electronic Combat", Color.Red, Color.DarkRed);
            Usages[46].Initialize("EG", "Entry/Exit Gate", Color.Cyan, Color.DarkCyan);
            Usages[47].Initialize("FACA", "Force Air Coordination Area", Color.Yellow, Color.Gold);
            Usages[48].Initialize("FARP", "Forward Arming and Refueling Point", Color.Yellow, Color.Gold);
            Usages[49].Initialize("FEBA", "Forward Edge of the Battle Area", Color.Magenta, Color.DarkMagenta);
            Usages[50].Initialize("FFA", "Free Fire Area", Color.Magenta, Color.DarkMagenta);
            Usages[51].Initialize("FIR", "Flight Information Region", Color.Blue, Color.DarkBlue);
            Usages[52].Initialize("FIRUB", "Fire Umbrella", Color.Green, Color.DarkGreen);
            Usages[53].Initialize("FLOT", "Forward Line of Own Troops", Color.Magenta, Color.DarkMagenta);
            Usages[54].Initialize("FOL", "Forward Operating Location", Color.Yellow, Color.Gold);
            Usages[55].Initialize("FRAD", "Falcon Radials", Color.Green, Color.DarkGreen);
            Usages[56].Initialize("FSCL", "Fire Support Coordination Line", Color.Magenta, Color.DarkMagenta);
            Usages[57].Initialize("GNDAOR", "Ground Area of Responsibility", Color.Magenta, Color.DarkMagenta);
            Usages[58].Initialize("HG", "Hand Over Gate", Color.Cyan, Color.DarkCyan);
            Usages[59].Initialize("HIDACZ", "High Density Airspace Control Zone", Color.Orange, Color.DarkOrange);
            Usages[60].Initialize("HIMEZ", "High-Altitude Missile Engagement Zone", Color.Orange, Color.DarkOrange);
            Usages[61].Initialize("IFFOFF", "IFF Switch Off Line", Color.Magenta, Color.DarkMagenta);
            Usages[62].Initialize("IFFON", "IFF Switch On Line", Color.Magenta, Color.DarkMagenta);
            Usages[63].Initialize("ISP", "Identification Safety Point", Color.Cyan, Color.DarkCyan);
            Usages[64].Initialize("ISR", "Identification Range", Color.Green, Color.DarkGreen);
            Usages[65].Initialize("JEZ", "Joint Engagement Zone", Color.Orange, Color.DarkOrange);
            Usages[66].Initialize("JOA", "Joint Operations Area", Color.Orange, Color.DarkOrange);
            Usages[67].Initialize("KILLB", "Killbox", Color.Yellow, Color.Gold);
            Usages[68].Initialize("KILLZ", "Kill Zone", Color.Orange, Color.DarkOrange);
            Usages[69].Initialize("LFEZ", "Land Fighter Engagement Zone", Color.Orange, Color.DarkOrange);
            Usages[70].Initialize("LMEZ", "Land Missile Engagement Zone", Color.Orange, Color.DarkOrange);
            Usages[71].Initialize("LOMEZ", "Low-Altitude Engagement Zone", Color.Orange, Color.DarkOrange);
            Usages[72].Initialize("LZ", "Landing Zone", Color.Red, Color.DarkRed);
            Usages[73].Initialize("MFEZ", "Maritime Fighter Engagement Zone", Color.Green, Color.DarkGreen);
            Usages[74].Initialize("MG", "Marshalling Gate", Color.Cyan, Color.DarkCyan);
            Usages[75].Initialize("MISARC", "Missile Arc", Color.Green, Color.DarkGreen);
            Usages[76].Initialize("MMEZ", "Maritime Missile Engagement Zone", Color.Green, Color.DarkGreen);
            Usages[77].Initialize("MOA", "Military Operations Area", Color.Yellow, Color.Gold);
            Usages[78].Initialize("MP", "Marshall Point", Color.Cyan, Color.DarkCyan);
            Usages[79].Initialize("MRR", "Minimum Risk Route", Color.Cyan, Color.DarkCyan);
            Usages[80].Initialize("NAVRTE", "Area Navigation Route", Color.Blue, Color.DarkBlue);
            Usages[81].Initialize("NFA", "No Fire Area", Color.Yellow, Color.Gold);
            Usages[82].Initialize("NOFLY", "No Fly Area", Color.Yellow, Color.Gold);
            Usages[83].Initialize("OTR", "Other", Color.Gray, Color.DarkGray);
            Usages[84].Initialize("PIRAZ", "Positive Identification Radar Advisory Zone", Color.Green, Color.DarkGreen);
            Usages[85].Initialize("PROHIB", "Prohibited Area", Color.Blue, Color.DarkBlue);
            Usages[86].Initialize("PZ", "Pickup Zone", Color.Red, Color.DarkRed);
            Usages[87].Initialize("RA", "Restricted Area", Color.Blue, Color.DarkBlue);
            Usages[88].Initialize("RCA", "Reduced Coordination", Color.Blue, Color.DarkBlue);
            Usages[89].Initialize("RECCE", "Reconaissance Area", Color.Red, Color.DarkRed);
            Usages[90].Initialize("RFA", "Restricted Fire Area", Color.Magenta, Color.DarkMagenta);
            Usages[91].Initialize("RFL", "Restricted Fire Line", Color.Magenta, Color.DarkMagenta);
            Usages[92].Initialize("ROA", "Restricted Operations Area", Color.Red, Color.DarkRed);
            Usages[93].Initialize("RTF", "Return to Force", Color.Green, Color.DarkGreen);
            Usages[94].Initialize("SAAFR", "Standard use Army Aircraft Flight Route", Color.Cyan, Color.DarkCyan);
            Usages[95].Initialize("SAFE", "Safe Area for Evasion", Color.Magenta, Color.DarkMagenta);
            Usages[96].Initialize("SAFES", "Safety Sector", Color.Green, Color.DarkGreen);
            Usages[97].Initialize("SARDOT", "Search and Rescue Point", Color.Cyan, Color.DarkCyan);
            Usages[98].Initialize("SC", "Special Corridor", Color.Cyan, Color.DarkCyan);
            Usages[99].Initialize("SCZ", "Ship Control Zone", Color.Green, Color.DarkGreen);
            Usages[100].Initialize("SEMA", "Special Electronics Mission Area", Color.Red, Color.DarkRed);
            Usages[101].Initialize("SHORAD", "Short-Range Air Defense Engagement Zone", Color.Orange, Color.DarkOrange);
            Usages[102].Initialize("SL", "Safe Lane", Color.Cyan, Color.DarkCyan);
            Usages[103].Initialize("SOF", "Special Operations Forces", Color.Red, Color.DarkRed);
            Usages[104].Initialize("SSMS", "Surface-to-Surface Missile System", Color.Yellow, Color.Gold);
            Usages[105].Initialize("TC", "Transit Corridor", Color.Cyan, Color.DarkCyan);
            Usages[106].Initialize("TCA", "Terminal Control Area", Color.Blue, Color.DarkBlue);
            Usages[107].Initialize("TL", "Traverse Level", Color.Magenta, Color.DarkMagenta);
            Usages[108].Initialize("TMRR", "Temporary Minimum Risk Route", Color.Cyan, Color.DarkCyan);
            Usages[109].Initialize("TR", "Transit Route", Color.Cyan, Color.DarkCyan);
            Usages[110].Initialize("TRNG", "Training Area", Color.Red, Color.DarkRed);
            Usages[111].Initialize("TRSA", "Terminal Radar Service Area", Color.Blue, Color.DarkBlue);
            Usages[112].Initialize("TSA", "Temporary Segregated Area", Color.Blue, Color.DarkBlue);
            Usages[113].Initialize("UAV", "Unmanned Aerial Vehicle", Color.Red, Color.DarkRed);
            Usages[114].Initialize("WARN", "Warning Area", Color.Blue, Color.DarkBlue);
            Usages[115].Initialize("WFZ", "Weapons Free Zone", Color.Orange, Color.DarkOrange);
        }

        public string GetDescription(string code)
        {
            for (int i = 0; i < NumUsages; i++ )
            {
                if (Usages[i].Code == code)
                    return Usages[i].Description;
            }
            return "Unknown";
        }

        public Color GetForeColor(string code)
        {
            for (int i = 0; i < NumUsages; i++ )
            {
                if (Usages[i].Code == code)
                    return Usages[i].ForeColor;
            }
            return Color.Gray;
        }

        public Color GetBackColor(string code)
        {
            for (int i = 0; i < NumUsages; i++ )
            {
                if (Usages[i].Code == code)
                    return Usages[i].BackColor;
            }
            return Color.DarkGray;
        }
    }
}
