using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using GET.Core.Objects;
using GET.Core.PFPS.Internal;

namespace GET.Core.PFPS
{
    public class ThreatFile
    {
        #region Private Member Data
        private List<ThreatObject> threats = new List<ThreatObject>();
        private Folder objectFolder;        
        private string title;        
        #endregion

        #region Public Properties
        public Folder ObjectFolder
        {
            get { return objectFolder; }
            set { objectFolder = value; }
        }
        public string PFPSPath = @"C:\PFPS";

        public List<ThreatObject> Threats
        {
            get { return threats; }
            set { threats = value; }
        }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        #endregion

        #region Methods
        public ThreatFile()
        {
        }

        public ThreatFile(string pfpsPath)
        {
            PFPSPath = pfpsPath;
        }

        public bool Load(string filename)
        {
            FileInfo dwfileinfo = new FileInfo(filename);
            string[] dwfileparts = dwfileinfo.Name.Split(new char[] { '.' });
            title = dwfileparts[0];

            // Open the FalconView Threat File (stored as an Access DB)
            OleDbConnection connection;
            try
            {
                connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename);
                connection.Open();
            }
            catch
            {
                return false;
            }

            // Load the threat instances
            OleDbCommand threatCommand = connection.CreateCommand();
            threatCommand.CommandText = "SELECT ID, CORRELATION_CODE, MILSTD_ID, LATITUDE_DEG, LONGITUDE_DEG, DATE_TIME, OFFICIAL_NAME, APPROVED_NICKNAME, ELLIPSE_ANGLE_DEG, ELLIPSE_SMAJ_NMI, ELLIPSE_SMIN_NMI, INFORMATION, SHOW_THREAT, SHOW_ELLIPSES, ENABLE_EDIT, SOURCE, OB_TYPE, LABEL_TEXT_LEFT, LABEL_TEXT_RIGHT FROM THREATS";
            OleDbDataReader threatReader = threatCommand.ExecuteReader();
            while (threatReader.Read())
            {
                ReadObject(threatReader);
            }
            threatReader.Close();            

            // Load the detection and engagement zones
            OleDbCommand ringsCommand = connection.CreateCommand();
            foreach (ThreatObject threat in this.threats)
            {
                ringsCommand.CommandText = "SELECT ID, SHOW_RANGE_RINGS, RANGE_NMI, AGL_NOT_MSL FROM THREATRADAR WHERE ID = ";
                ringsCommand.CommandText += threat.Id.ToString();
                OleDbDataReader ringsReader = ringsCommand.ExecuteReader();

                // The first entry is the engagement ring
                if (!ringsReader.Read())
                    break;

                try { threat.Engagement_range = (double)ringsReader["RANGE_NMI"]; }
                catch { }
                threat.Engagement_height = 0;
                try { threat.Show_engagement_ring = (bool)ringsReader["SHOW_RANGE_RINGS"]; }
                catch { }
                try { threat.Engagement_agl = (bool)ringsReader["AGL_NOT_MSL"]; }
                catch { }

                // The second entry is the detection ring
                if (!ringsReader.Read())                
                    break;
                try { threat.Detection_range = (double)ringsReader["RANGE_NMI"]; }
                catch { }
                threat.Detection_height = 0;
                try { threat.Show_detection_ring = (bool)ringsReader["SHOW_RANGE_RINGS"]; }
                catch { }
                try { threat.Detection_agl = (bool)ringsReader["AGL_NOT_MSL"]; }
                catch { }

                ringsReader.Close();
            }
            connection.Close();

            // Load the icon names from the parametrics database
            // Open the FalconView Drawing File (stored as an Access DB)
            OleDbConnection parametricConnection;
            try
            {
                parametricConnection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + PFPSPath + @"\falcon\data\intel\ThreatDb.prm");
                parametricConnection.Open();
            }
            catch
            {
                return false;
            }
            OleDbCommand parametricCommand = parametricConnection.CreateCommand();
            foreach (ThreatObject threat in threats)
            {
                parametricCommand.CommandText = "SELECT ICON_FILENAME FROM PARAMETRICS WHERE CORRELATION_CODE = " + threat.Correlation;
                OleDbDataReader parametricReader = parametricCommand.ExecuteReader();
                parametricReader.Read();
                threat.Icon_filename = PFPSPath + @"\falcon\data\icons\threat\" + (string)parametricReader["ICON_FILENAME"] + ".ico";
                parametricReader.Close();
            }
            parametricConnection.Close();

            return true;
        }

        private void ReadObject(OleDbDataReader reader)
        {
            ThreatObject threat = new ThreatObject();
            try { threat.Approved_nickname = (string)reader["APPROVED_NICKNAME"]; }
            catch { }
            try { threat.Correlation = (int)reader["CORRELATION_CODE"]; }
            catch { }
            try { threat.Datetime = (string)reader["DATE_TIME"]; }
            catch { }
            try { threat.Ellipse_angle_deg = (float)reader["ELLIPSE_ANGLE_DEG"]; }
            catch { }
            try { threat.Ellipse_smaj_nmi = (float)reader["ELLIPSE_SMAJ_NMI"]; }
            catch { }
            try { threat.Ellipse_smin_nmi = (float)reader["ELLIPSE_SMIN_NMI"]; }
            catch { }
            try { threat.Enable_edit = (bool)reader["ENABLE_EDIT"]; }
            catch { }
            try { threat.Id = (int)reader["ID"]; }
            catch { }
            try { threat.Information = (string)reader["INFORMATION"]; }
            catch { }
            try { threat.Label_text_left = (string)reader["LABEL_TEXT_LEFT"]; }
            catch { }
            try { threat.Label_text_right = (string)reader["LABEL_TEXT_RIGHT"]; }
            catch { }
            try { threat.Latitude = (double)reader["LATITUDE_DEG"]; }
            catch { }
            try { threat.Longitude = (double)reader["LONGITUDE_DEG"]; }
            catch { }
            try { threat.Milstd_id = (string)reader["MILSTD_ID"]; }
            catch { }
            try { threat.Ob_type = (short)reader["OB_TYPE"]; }
            catch { }
            try { threat.Official_name = (string)reader["OFFICIAL_NAME"]; }
            catch { }
            try { threat.Show_ellipse = (bool)reader["SHOW_ELLIPSES"]; }
            catch { }
            try { threat.Show_threat = (bool)reader["SHOW_THREAT"]; }
            catch { }
            try { threat.Source = (string)reader["SOURCE"]; }
            catch { }

            threats.Add(threat);
        }

        public Folder Convert()
        {
            // Create the parent folder
            ObjectFolder = new Folder();
            ObjectFolder.Name = title;
            ObjectFolder.Snippet = "FalconView Threat File";            

            // Convert/load all the points
            foreach (ThreatObject threat in this.threats)                            
                ObjectFolder.AddChild(threat.Convert());            

            return ObjectFolder;
        }
        #endregion
    }
}
