using System;
using System.Collections.Generic;
using System.Text;
using GET.Core;
using GET.Core.Objects;

namespace GET.Core.PFPS.Internal
{
    public class LocalPointObject
    {
        #region Private Member Data
        private string id;
        private string description;
        private double latitude;
        private double longitude;
        private int elevation;
        private string elevation_source;
        private decimal pt_quality;
        private string area;
        private string country_code;
        private string dtd_id;
        private float horz_accuracy;
        private float vert_accuracy;
        private string link_name;
        private string icon_name = "";
        private string comment;
        private string group_name;        
        #endregion
        
        #region Public Properties
        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }
        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }
        public int Elevation
        {
            get { return elevation; }
            set { elevation = value; }
        }
        public string Elevation_source
        {
            get { return elevation_source; }
            set { elevation_source = value; }
        }
        public decimal Pt_quality
        {
            get { return pt_quality; }
            set { pt_quality = value; }
        }
        public string Area
        {
            get { return area; }
            set { area = value; }
        }
        public string Country_code
        {
            get { return country_code; }
            set { country_code = value; }
        }
        public string Dtd_id
        {
            get { return dtd_id; }
            set { dtd_id = value; }
        }
        public float Horz_accuracy
        {
            get { return horz_accuracy; }
            set { horz_accuracy = value; }
        }
        public float Vert_accuracy
        {
            get { return vert_accuracy; }
            set { vert_accuracy = value; }
        }
        public string Link_name
        {
            get { return link_name; }
            set { link_name = value; }
        }
        public string Icon_name
        {
            get { return icon_name; }
            set { icon_name = value; }
        }
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }
        public string Group_name
        {
            get { return group_name; }
            set { group_name = value; }
        }
        #endregion

        #region Methods
        public Waypoint Convert()
        {
            Waypoint wp = new Waypoint();
            wp.Name = this.Id;
            wp.Snippet = this.Dtd_id;            
            wp.Coordinates.Assign(this.Latitude, this.Longitude);

            if (this.Elevation > 0)
            {
                wp.Altitude = this.Elevation;
                wp.AltitudeType = AltType.FixedAGL;
            }
            else
                wp.AltitudeType = AltType.ClampToGround;

            if(this.Icon_name != "")
                wp.StyleOptions.IconPath = @"C:\PFPS\falcon\data\icons\localpnt\" + this.Icon_name + ".ico";
            else
                wp.StyleOptions.IconPath = "http://maps.google.com/mapfiles/kml/shapes/donut.png";

            wp.StyleOptions.IconScale = 0.8;
            wp.Description = "<b>DESCRIPTION: </b>" + this.description + "<br>";
            wp.Description += "<b>DTD ID: </b>" + this.Dtd_id + "<br>";
            wp.Description += "<b>COMMENT: </b>" + this.comment + "<br>";
            wp.Description += "<b>ELEVATION: </b>" + this.elevation + "<br>";
            wp.Description += "<b>ELEVATION SOURCE: </b>" + this.Elevation_source + "<br>";
            wp.Description += "<b>HORZ ACCURACY: </b>" + this.Horz_accuracy + "<br>";
            wp.Description += "<b>VERT ACCURACY: </b>" + this.Vert_accuracy + "<br>";
            wp.Description += "<b>POINT QUALITY: </b>" + this.pt_quality + "<br>";
            wp.Description += "<b>AREA: </b>" + this.Area + "<br>";
            wp.Description += "<b>COUNTRY CODE: </b>" + this.Country_code + "<br>";
            wp.Description += "<b>GROUP: </b>" + this.Group_name + "<br>";
            wp.Description += "<b>LINK: </b>" + this.Link_name + "<br>";

            return wp;
        }
        #endregion
    }
}
