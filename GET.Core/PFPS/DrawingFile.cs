/********************************************************************************/
/* Class:   GET.Core.PFPS.DrawingFile()                                         */
/*                                                                              */
/* Each instance of this class corresponds to a single FalconView Drawing File. */
/* Interacing is handled IAW the FalconView Drawing File Interface Control      */
/* Document, included in the FalconView SDK.  Load() loads a Drawing File into  */
/* a series of DrawingFileObject() objects.  The Convert() function converts    */
/* these into a series of GETObjects() and stores them in a single Folder()     */
/* object called "ObjectFolder."                                                */
/*                                                                              */
/* Public Properties:                                                           */
/*                                                                              */
/* Methods:                                                                     */
/*                                                                              */
/* bool Load(string filename, DrawingFileImportOptions options)                 */
/*  STATUS: IN PROGRESS                                                         */
/*                                                                              */
/*      This function loads a FalconView Drawing File into a series of          */
/*      DrawingFileObject() objects.  It uses the specified options to          */
/*      determine how the import should occur and what default optiosn should   */
/*      be used for new objects.                                                */
/*                                                                              */
/* Folder Convert(DrawingFileImportOptions options)                             */
/*  STATUS: IN PROGRESS                                                         */
/*                                                                              */
/*      This function converts all the DrawingFileObject() objects into         */
/*      GETObject() objects.  These are stored in a single folder called        */
/*      ObjectFolder, which is also returned by the function.                   */
/********************************************************************************/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Text;
using GET.Core;
using GET.Core.Objects;
using GET.Core.PFPS.Internal;

namespace GET.Core.PFPS
{
    public class DrawingFile
    {        
        public AltType DefaultAltitudeType = AltType.FixedAGL;
        public int DefaultAltitude = 1500;
        public List<DrawingFileObject> DrawingObjects = new List<DrawingFileObject>();
        public Folder ObjectFolder = new Folder();
        public int DebugCode = 0;

        public bool Load(string filename)
        {
            // Get filename info.  This will be used to name the ObjectFolder
            FileInfo dwfileinfo = new FileInfo(filename);
            string[] dwfileparts = dwfileinfo.Name.Split(new char[] { '.' });

            // Open the FalconView Drawing File (stored as an Access DB)
            OleDbConnection connection;
            try
            {
                connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filename);
                connection.Open();
            }
            catch
            {
                DebugCode = 1;
                return false;
            }
            // Create the database connection
            OleDbCommand command = connection.CreateCommand();

            // this index tracks what object we're reading.  It gets incremented
            // when a new object is read.
            int currentItem = -1;
            DrawingFileObject currentObject = null;

            command.CommandText = "SELECT ItemNum, DataType, Data FROM Main";

            OleDbDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                // read the line.  Each line has three fields:
                // (1) ItemNum
                // (2) DataType
                // (3) Data
                // An object will have multiple lines, so this loop keeps
                // adding new lines of data to the currentObject until a 
                // new ItemNum is encountered.  When that happens, a new
                // object is created and the process continues.
                Int32 intItemNum = (Int32)reader["ItemNum"];
                Int16 intDataType = (Int16)reader["DataType"];
                string stringData = "";
                
                if (reader["Data"] is DBNull)
                    stringData = "";
                else
                    stringData = (string)reader["Data"];                

                // If this is a new item, create an object, add it to the list of 
                // objects, and increment the object counter
                
                if (intItemNum > currentItem)
                {                    
                    currentObject = new DrawingFileObject();
                    currentObject.ID = intItemNum;
                    currentObject.AltitudeType = this.DefaultAltitudeType;
                    currentObject.AltitudeCeiling = this.DefaultAltitude;
                    DrawingObjects.Add(currentObject);
                    currentItem++;
                }
                
                // error check... if currentObject is null, there's a problem
                if (currentObject == null)
                {
                    DebugCode = 2;
                    return false;
                }

                // Add the line of data to the object
                currentObject.AddLine(intDataType, stringData);                                
            }
            connection.Close();
            return true;
        }

        public Folder Convert()
        {
            // Clear any existing objects
            //this.ObjectFolder = options.FolderOptions.GetCopy();
            this.ObjectFolder.Children.Clear();

            // Loop through all the drawing objects, convert them, and
            // add the resulting GETObject to the objct folder
            foreach(DrawingFileObject drawObject in this.DrawingObjects)
            {
                GETObject getObject = drawObject.Convert();
                if(getObject != null)
                    this.ObjectFolder.AddChild(getObject);
            }
            return ObjectFolder;
        }
    }
}
