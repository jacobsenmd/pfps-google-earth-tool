using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using GET.Core.Objects;

namespace GET.Core.PFPS
{
    public class ACO
    {
        #region Private Member Data
        private List<ACOObject> objects = new List<ACOObject>();        
        #endregion

        #region Public Properties
        public List<ACOObject> Objects
        {
            get { return objects; }
            set { objects = value; }
        }
        #endregion

        public bool Load(List<string> filenames)
        {
            // Here's the big picture of how this file works:
            // 1. We loop through each file in the list
            // 2. We read the entire file at once into a string called 'entireFile'
            // 3. We split this string into an array of substrings called 'acoLines'
            //    These lines are delimited by "//"
            // 4. We remove any extraneous \r or \n characters from each acoLine
            //    The results of that purching are called 'cleanLine'
            // 5. We break each 'cleanLine' into substrings called 'acoElements'
            //    These elements are delimited by a single "/"
            // 6. If the first element is "ACMID", we've reached a new object.
            //    Create a new ACOObject.
            // 7. Pass the data element to the current ACOObject

            // Create a StreamReader object and open the file
            StreamReader reader;
            string[] separator = new string[] { "//", "\r\n" };
            foreach (string filename in filenames)
            {                
                try
                {
                    reader = new StreamReader(filename);
                }
                catch
                {
                    return false;
                }

                ACOObject currentObject = null;

                
                // Loop through each line of the ACO
                string entireFile = reader.ReadToEnd();                                
                string[] acoLines = entireFile.Split(separator, StringSplitOptions.None);
                foreach (string acoLine in acoLines)
                {
                    // Break the current line into individual word elements
                    string cleanLine = acoLine.TrimStart(new char[] {'\r','\n'});
                    string[] acoElements = cleanLine.Split(new char[] { '/' });

                    // If the line begins with "ACMID" we are reading a new object.
                    // Create the new object and add it to the list of objects.
                    if (acoElements[0] == "ACMID")
                    {
                        currentObject = new ACOObject();
                        objects.Add(currentObject);                        
                    }
                    // Parse the current line and add the details to the current object
                    if (currentObject != null)
                        currentObject.AddLine(cleanLine);
                }
                reader.Close();
            }
            return true;
        }

        public Folder Convert(Folder folderOptions)
        {
            // Create a new folder, based on the specified options.  Ensure it is empty
            // of child objects
            Folder newFolder = folderOptions.GetCopy();
            newFolder.Children.Clear();

            Folder[] subFolders = new Folder[118];
            subFolders[0].Name = "Air-to-Air Refueling Areas";
            subFolders[1].Name = "Airborne Command & Control Areas";
            subFolders[2].Name = "Airspace Coordination Areas";
            subFolders[3].Name = "Air Control Points";
            subFolders[4].Name = "Airspace Control Subareas/Sectors";
            subFolders[5].Name = "Air Defense Action Areas";
            subFolders[6].Name = "Air Defense Identification Zones";
            subFolders[7].Name = "Advisory Routes";
            subFolders[8].Name = "Amphibious Defense Zones";
            subFolders[9].Name = "Airborne Early Warning";
            
            // Loop through all the ACO objects, convert them, and
            // add the resulting GETObject to the object folder
            
            foreach(ACOObject obj in this.objects)
            {
                GETObject getObject = obj.Convert();
                if (getObject != null)
                    newFolder.AddChild(getObject);
            }
            return newFolder;
        }
    }
}
