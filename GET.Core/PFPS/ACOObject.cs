using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using GET.Core;
using GET.Core.Objects;

namespace GET.Core.PFPS
{
    public class ACOObject
    {
        #region Private Member Data
        private int altCeiling;
        private int altFloor;
        private AltType altitudeType = AltType.ClampToGround;
        private string altString = "";
        private decimal corridorWidth;
        private DateTime endTime;
        private string id;
        private string remarks;
        private string contAuth;
        private double radius;
        private string shape;
        private DateTime startTime;
        private string type;
        private string usage;
        private Color foreColor;
        private Color backColor;

        
        private List<LatLon> points = new List<LatLon>();

        #endregion

        #region Public Properties
        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }
        public string ID
        {
            get { return id; }
            set { id = value; }
        }
        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }
        public string Shape
        {
            get { return shape; }
            set { shape = value; }
        }
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        public string Usage
        {
            get { return usage; }
            set { usage = value; }
        }       
        #endregion

        #region Methods
        public void AddLine(string line)
        {
            // Break the current line into individual word elements
            string cleanLine = line.TrimStart(new char[] { '\r', '\n' });
            string[] acoElements = cleanLine.Split(new char[] { '/' });

            if (acoElements[0] == "ACMID")
            {
                ParseACMID(cleanLine);
                GetAirspaceColor();
            }
            else if (acoElements[0] == "APOINT")
                ParseAPoint(cleanLine);
            else if (acoElements[0] == "CIRCLE")
                ParseCircle(cleanLine);
            else if (acoElements[0] == "POLYGON")
                ParsePolygon(cleanLine);
            else if (acoElements[0] == "CORRIDOR")
                ParseCorridor(cleanLine);
            else if (acoElements[0] == "EFFLEVEL")
                ParseEffLevel(cleanLine);
            else if (acoElements[0] == "CONTAUTH")
                ParseContAuth(cleanLine);
            else if (acoElements[0] == "AMPN")
                ParseAmpn(cleanLine);

        }

        public GETObject Convert()
        {
            // This function converts the current ACOObject into a GETObject and returns the
            // result.  This is a simple switching function.  Different ACO object shapes
            // will create different kinds of GETObjects, so this function calls the appropriate
            // function.
            GETObject obj = null;
            if (this.Shape == "CORRIDOR" || this.Shape == "TRACK")
                obj = ConvertRoute();
            else if (this.Shape == "POINT")
                obj = ConvertWaypoint();
            else
                obj = ConvertAirspaceRegion();

            obj.StyleOptions.FillColor = this.backColor;
            obj.StyleOptions.LineColor = this.foreColor;
            obj.StyleOptions.IconColor = this.foreColor;
            obj.StyleOptions.LabelColor = this.foreColor;
            obj.StyleOptions.IconScale = 0.5;
            obj.StyleOptions.LabelScale = 0.5;

            return obj;
        }

        private void GetAirspaceColor()
        {
            switch (this.Usage)
            {
                case "AAR":
                case "ABC":
                case "AEW":
                case "CAP":
                case "CAS":
                case "DZ":
                case "EC":
                case "LZ":
                case "PZ":
                case "RECCE":
                case "ROA":
                case "SEMA":
                case "SOF":
                case "TRNG":
                case "UAV":
                    this.foreColor = Color.Red;
                    this.backColor = Color.DarkRed;
                    break;
                case "ACA":
                case "ALTRV":
                case "BNDRY":
                case "CFL":
                case "CL":
                case "DBSL":
                case "FEBA":
                case "FFA":
                case "FLOT":
                case "FSCL":
                case "GNDAOR":
                case "IFFOFF":
                case "IFFON":
                case "RFA":
                case "RFL":
                case "SAFE":
                case "TL":
                    this.foreColor = Color.Magenta;
                    this.backColor = Color.DarkMagenta;
                    break;
                case "ACP":
                case "BULL":
                case "CP":
                case "EG":
                case "HG":
                case "ISP":
                case "MG":
                case "MP":
                case "SARDOT":
                    this.foreColor = Color.Cyan;
                    this.backColor = Color.DarkCyan;
                    break;
                case "ACSS":
                case "ALERTA":
                case "ACSA":
                case "FACA":
                case "FARP":
                case "FOL":
                case "KILLB":
                case "MOA":
                case "NFA":
                case "NOFLY":
                case "SSMS":
                    this.foreColor = Color.Yellow;
                    this.backColor = Color.Gold;
                    break;
                case "AADA":
                case "ADZ":
                case "AOA":
                case "APPCOR":
                case "CADA":
                case "CCZONE":
                case "COZ":
                case "FIRUB":
                case "FRAD":
                case "ISR":
                case "MFEZ":
                case "MISARC":
                case "MMEZ":
                case "PIRAZ":
                case "RTF":
                case "SAFES":
                case "SCZ":
                    this.foreColor = Color.Green;
                    this.backColor = Color.DarkGreen;
                    break;
                case "ADIZ":
                case "BDZ":
                case "BZ":
                case "HIDACZ":
                case "HIMEZ":
                case "JEZ":
                case "JOA":
                case "KILLZ":
                case "LFEZ":
                case "LMEZ":
                case "LOMEZ":
                case "SHORAD":
                case "WFZ":
                    this.foreColor = Color.Orange;
                    this.backColor = Color.DarkOrange;
                    break;
                case "ADVRTE":
                case "ARWY":
                case "ATSRTE":
                case "CBA":
                case "CDR":
                case "CLSA":
                case "CLSB":
                case "CLSC":
                case "CLSD":
                case "CLSE":
                case "CLSF":
                case "CLSG":
                case "CONTZN":
                case "CTA":
                case "DA":
                case "FIR":
                case "NAVRTE":
                case "PROHIB":
                case "RA":
                case "RCA":
                case "TCA":
                case "TRSA":
                case "TSA":
                case "WARN":
                    this.foreColor = Color.Blue;
                    this.backColor = Color.DarkBlue;
                    break;
                default:
                    this.foreColor = Color.Gray;
                    this.backColor = Color.DarkGray;
                    break;
            }
        }

        private void ParseACMID(string cleanLine)
        {            
            string[] acoElements = cleanLine.Split(new char[] { '/' });

            this.Type = acoElements[1];
            this.ID = acoElements[2];            
            this.Shape = acoElements[3];
            this.Usage = acoElements[4];
        }

        private void ParseAPoint(string cleanLine)
        {
            string[] acoElements = cleanLine.Split(new char[] { '/' });

            points.Add(ParseLatLon(acoElements[1]));
        }

        private void ParseCircle(string cleanLine)
        {
            string[] acoElements = cleanLine.Split(new char[] { '/' });

            points.Add(ParseLatLon(acoElements[1]));

            acoElements[2] = acoElements[2].Trim(new char[] { 'N', 'M' });
            this.radius = System.Convert.ToDouble(acoElements[2]);
            
        }

        private void ParsePolygon(string cleanLine)
        {
            string[] acoElements = cleanLine.Split(new char[] { '/' });

            for (int i = 1; i < acoElements.Length; i++)
                points.Add(ParseLatLon(acoElements[i]));
        }

        private void ParseCorridor(string cleanLine)
        {
            string[] acoElements = cleanLine.Split(new char[] { '/' });
            acoElements[1] = acoElements[1].Trim(new char[] {'N', 'M' });
            corridorWidth = System.Convert.ToDecimal(acoElements[1]);

            for(int i = 2; i < acoElements.Length - 2; i++)
                points.Add(ParseLatLon(acoElements[i]));
        }

        private void ParseEffLevel(string cleanLine)
        {
            string[] acoElements = cleanLine.Split(new char[] { '/' });
            string[] altElements = acoElements[1].Split(new char[] { '-' });
            char[] alphabet = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ' };

            this.altString = acoElements[1];

            // Floor
            if (altElements[0][0] == 'F')
            {
                altElements[0].Trim();
                altElements[0] = altElements[0].Trim(alphabet);
                altFloor = System.Convert.ToInt32(altElements[0]);
                altFloor *= 100;
            }
            else
            {
                altElements[0].Trim();
                altElements[0] = altElements[0].Trim(alphabet);
                try { altFloor = System.Convert.ToInt32(altElements[0]); }
                catch { altFloor = 0; }
            }

            // Ceiling
            if (altElements[1][0] == 'F')
            {
                altElements[1] = altElements[1].Trim(alphabet);
                altCeiling = System.Convert.ToInt32(altElements[1]);
                altCeiling *= 100;
                altitudeType = AltType.FixedMSL;
            }
            else
            {
                if(altElements[1].Contains("M"))
                    altitudeType = AltType.FixedMSL;
                else if(altElements[1].Contains("A"))
                    altitudeType = AltType.FixedAGL;
                altElements[1].Trim();
                altElements[1] = altElements[1].Trim(alphabet);
                try { altCeiling = System.Convert.ToInt32(altElements[1]); }
                catch { altFloor = 0; }
            }
        }

        private void ParseContAuth(string cleanLine)
        {
            string[] acoElements = cleanLine.Split(new char[] { '/' }, 2);
            this.contAuth += acoElements[1] + "\r\n";
        }

        private void ParseAmpn(string cleanLine)
        {
            string[] acoElements = cleanLine.Split(new char[] { '/' }, 2);
            this.remarks += acoElements[1] + "\r\n";
        }

        private LatLon ParseLatLon(string acoElement)
        {
            string[] acoSubElement = acoElement.Split(new char[] { 'N', 'S' }, 2);
            acoSubElement[1] = acoSubElement[1].Trim(new char[] { 'E', 'W' });

            LatLon coord = new LatLon();
            double LatDD = System.Convert.ToDouble(acoSubElement[0].Substring(0, 2));
            double LatMM = System.Convert.ToDouble(acoSubElement[0].Substring(2,acoSubElement[0].Length - 2));
            double LonDD = System.Convert.ToDouble(acoSubElement[1].Substring(0, 3));
            double LonMM = System.Convert.ToDouble(acoSubElement[1].Substring(3, acoSubElement[1].Length - 3));

            double LatDouble = LatDD + LatMM / 60;
            double LonDouble = LonDD + LonMM / 60;

            if (acoElement.Contains("S"))
                LatDouble = -LatDouble;
            if (acoElement.Contains("W"))
                LonDouble = -LonDouble;

            coord.Assign(LatDouble, LonDouble);
            return coord;
        }

        private AirspaceRegion ConvertAirspaceRegion()
        {
            AirspaceRegion newRegion = new AirspaceRegion();
            newRegion.Name = this.ID;
            newRegion.Snippet = this.usage;
            newRegion.Description = "ALTITUDES: " + this.altString + "\r\n";
            newRegion.Description += "REMARKS:\r\n" + this.remarks + "\r\nCONTROL AUTHORITY:\r\n" + this.contAuth;
            newRegion.AltitudeFloor = this.altFloor;
            newRegion.AltitudeCeiling = this.altCeiling;
            newRegion.AltitudeType = this.altitudeType;

            if (this.Shape == "POLYGON")
            {
                newRegion.AirspaceRegionType = AirspaceType.Polygon;
                foreach (LatLon coord in this.points)
                    newRegion.PolygonPoints.Add(coord);
            }
            else if (this.Shape == "CIRCLE")
            {
                newRegion.AirspaceRegionType = AirspaceType.Circle;
                newRegion.OuterRadius = (decimal)this.radius;
                newRegion.InnerRadius = 0;
                newRegion.CenterPoint = this.points[0];
            }
            return newRegion;
        }

        private Route ConvertRoute()
        {
            Route newRoute = new Route();
            newRoute.Name = this.ID;            
            newRoute.Snippet = this.usage;
            newRoute.Description = "ALTITUDES: " + this.altString + "\r\n";
            newRoute.AltitudeType = this.altitudeType;

            newRoute.Description += "DESCRIPTION:\r\n" + this.remarks + "\r\nCONTROL AUTHORITY:\r\n" + this.contAuth;
            int i = 1;
            foreach (LatLon coord in this.points)
            {
                Waypoint wp = new Waypoint();
                wp.Coordinates = coord;
                wp.Name = this.ID + " - " + i;
                newRoute.Points.Add(wp);
                i++;
            }
            return newRoute;
        }

        private Waypoint ConvertWaypoint()
        {
            Waypoint newPoint = new Waypoint();
            newPoint.Name = this.ID;
            newPoint.Snippet = this.usage;
            newPoint.Description = "ALTITUDES: " + this.altString + "\r\n";
            newPoint.Description += "DESCRIPTION:\r\n" + this.remarks + "\r\nCONTROL AUTHORITY:\r\n" + this.contAuth;
            newPoint.Coordinates = this.points[0];
            return newPoint;
        }




        private bool Load(string filename)
        {
            return true;
        }
        #endregion
    }
}
