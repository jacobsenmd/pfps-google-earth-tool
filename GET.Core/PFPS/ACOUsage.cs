using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GET.Core.PFPS
{
    public class ACOUsage
    {
        public string Description;
        public string Code;
        public Color ForeColor;
        public Color BackColor;

        public void Initialize(string code, string description, Color foreColor, Color backColor)
        {
            Description = description;
            Code = code;
            ForeColor = foreColor;
            BackColor = backColor;
        }
    }
}
