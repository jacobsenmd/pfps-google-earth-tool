using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using GET.Core.Objects;
using GET.Core.PFPS.Internal;

namespace GET.Core.PFPS
{
    public class CrudFile
    {
        #region Private Member Data                
        private List<Route> getRoutes = new List<Route>();      
        private List<CrudRoute> pfpsRoutes = new List<CrudRoute>();          
        #endregion

        #region Public Properties
        public string Filename;
        public List<Route> GETRoutes
        {
            get { return getRoutes; }
            set { getRoutes = value; }
        }
        public List<CrudRoute> PFPSRoutes
        {
            get { return pfpsRoutes; }
            set { pfpsRoutes = value; }
        }
        #endregion

        #region Methods

        // This function converts all PFPS routes into GET routes
        public bool ConvertData()
        {
            // Only convert if there are PFPS routes loaded
            if (PFPSRoutes.Count == 0)
                return false;

            // Clear any old data
            GETRoutes.Clear();

            // loop through each PFPS route in this file and create a
            // GET route out of it
            foreach (CrudRoute sourceRoute in PFPSRoutes)
            {
                Route newRoute = sourceRoute.ConvertData();
                GETRoutes.Add(newRoute);
            }
            return true;
        }

        // This function opens a .CRD file and loads all routes
        public bool Load(string filename)
        {
            Filename = filename;
            XmlTextReader InputFile = null;

            // Load the .CRD file and open an XML text reader
            try
            {
                InputFile = new XmlTextReader(filename);
            }
            catch (System.IO.FileNotFoundException)
            {                
                InputFile = null;
                return false;
            }
            
            // Clear any existing data in this PFPSRouteFile object
            pfpsRoutes.Clear();

            // Now we start reading the file.  We loop until we hit
            // the "ROUTE" tag, which begins a new route.  When we
            // find a new route, we create a PFPSRoute object, call
            // its import function, then add it to the list of
            // routes in this PFPSRouteFile object.
            while (InputFile.Read())
            {
                if (InputFile.NodeType == XmlNodeType.Element)
                {
                    if (InputFile.Name == "ROUTE")
                    {
                        CrudRoute newRoute = new CrudRoute();
                        newRoute.Import(InputFile);
                        pfpsRoutes.Add(newRoute);
                    }
                }
            }
            return true;

        }

        #endregion

    }
}
