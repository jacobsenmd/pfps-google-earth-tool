using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using GET.Core.Objects;

namespace GET.Core.PFPS.Internal
{
    public class CrudRoute
    {
        #region Private Member Data
        private string name;        
        private List<CrudPoint> routePoints = new List<CrudPoint>();        
        #endregion

        #region Public Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }        
        public List<CrudPoint> RoutePoints
        {
            get { return routePoints; }
            set { routePoints = value; }
        }
        #endregion

        #region Methods

        public Route ConvertData()
        {
            // This is the same function as below, but with fewer calling
            // arguments.  It uses default Route Options.
            Style NewCustomStyle = new Style();
            return ConvertData(NewCustomStyle);
        }

        public Route ConvertData(Style style)
        {
            Route newRoute = new Route();
            // Copy the easy parameters
            newRoute.Name = this.Name;
            // TODO: add more fields to the description (variation, TAC, etc.)

            // If a style is specified, use it... if not, use defaults
            if (style != null)
                newRoute.StyleOptions = style;
            else
                newRoute.StyleOptions = new Style();

            // Copy each point            
            foreach (CrudPoint p in this.RoutePoints)
            {
                Waypoint newpt = new Waypoint();
                newpt = p.ConvertData();
                newRoute.Points.Add(newpt);
            }
            return newRoute;
        }        

        public bool Import(XmlTextReader InputFile)
        {
            // PFPS .CRD files use really goofy logic for storing point.  At the
            // start of a .CRD file, under the tag "POINTS", is a list of all the
            // points that may appear in the route.  However, these are not
            // necessarily the same points in the user's route.  To get the actual
            // route, you have to look further down in the .CRD file under the tag
            // "ROUTE_POINTS", which lists the order of the route points and the
            // reference of the corresponding POINT structure.  What a mess.

            // This importer stores all the points under the POINTS tag in
            // the "rawPoints" list.  When it comes to the ROUTE_POINTS section,
            // it stores the proper order of references in the "pointOrder" list
            // below.  At the end of the function, these two lists are used to
            // generate the final "routePoints" list, which contains all the
            // POINT structures in the route.
            //
            // pointOrder and rawPoints are no longer needed once "routePoints"
            // is generated, hence they are kept as internal variables.
            List<CrudPoint> rawPoints = new List<CrudPoint>();
            List<int> pointOrder = new List<int>();
            routePoints.Clear();

            while (InputFile.Read())
            {
                switch (InputFile.NodeType)
                {
                    case XmlNodeType.EndElement: // we've reached the end of this route
                        if (InputFile.Name == "ROUTE")
                        {
                            // this is the code for generating "routePoints"
                            // based on "rawPoints" and "pointOrder"
                            // It loops through each point reference in the route,
                            // finds the patching PFPSPoint object, and adds that
                            // PFPSPoint object to "routePoints"
                            for(int i = 0; i < pointOrder.Count; i++)
                            {
                                foreach(CrudPoint p in rawPoints)
                                {
                                    if (p.ID == pointOrder[i])
                                    {
                                        routePoints.Add(p);
                                        break;
                                    }
                                }
                            }
                            return true;
                        }
                        break;
                    case XmlNodeType.Element:
                        if (InputFile.Name == "NAME")
                            this.name = InputFile.ReadString();
                        else if (InputFile.Name == "POINT")
                        {
                            CrudPoint newPoint = new CrudPoint();
                            newPoint.Import(InputFile);
                            rawPoints.Add(newPoint);
                        }
                        else if (InputFile.Name == "ROUTE_POINT")
                        {
                            // This gets the reference to the next point in
                            // the route and adds it to the pointOrder.  By
                            // the end of the file read, "pointOrder" will list
                            // all the references in order
                            pointOrder.Add(GetRoutePointReference(InputFile));
                        }
                        break;
                }
            }
            return true;
        }

        // This function is broken out for clarity.  It gets the reference
        // of the next point in the route.
        private int GetRoutePointReference(XmlTextReader InputFile)
        {        
            int pointRef;
            while (InputFile.Read())
            {
                if (InputFile.Name == "POINT_REFERENCE")
                {
                    pointRef = Convert.ToInt32(InputFile.ReadString());
                    return pointRef;
                }
            }
            return 0;
        }
        #endregion
    }
}
