using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using GET.Core.Objects;

namespace GET.Core.PFPS.Internal
{
    public class CrudPoint
    {
        #region Private Member Data
        private int id;
        private string datum;
        private LatLon coordinates = new LatLon();        
        private string description;
        private int elevation;
        private string magneticVariation;
        private string name;
        private string slaveVariation;
        private string source;
        private string tacanChannel;
        private string vorFrequency;
        #endregion

        #region Public Properties
        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public LatLon Coordinates
        {
            get { return coordinates; }
            set { coordinates = value; }
        }
        public string Datum
        {
            get { return datum; }
            set { datum = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public int Elevation
        {
            get { return elevation; }
            set { elevation = value; }
        }
        public string MagneticVariation
        {
            get { return magneticVariation; }
            set { magneticVariation = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public string SlaveVariation
        {
            get { return slaveVariation; }
            set { slaveVariation = value; }
        }
        public string Source
        {
            get { return source; }
            set { source = value; }
        }
        public string TacanChannel
        {
            get { return tacanChannel; }
            set { tacanChannel = value; }
        }
        public string VORFrequency
        {
            get { return vorFrequency; }
            set { vorFrequency = value; }
        }        
        #endregion

        #region Methods

        public Waypoint ConvertData()
        {
            Waypoint waypoint = new Waypoint();
            waypoint.Coordinates = this.Coordinates.GetCopy();
            waypoint.Name = this.Name;
            waypoint.Description = this.Description;                        
            // NOTE: don't see a need for elevation to carry over
            return waypoint;
        }

        public void Import(XmlTextReader InputFile)
        {
            string pfpsLatitude = "";
            string pfpsLongitude = "";

            while (InputFile.Read())
            {
                switch (InputFile.NodeType)
                {
                    case XmlNodeType.EndElement: // we've reached the end of the point
                        if (InputFile.Name == "POINT")
                        {
                            this.coordinates.Assign(pfpsLatitude, pfpsLongitude);
                            return;
                        }
                        break;
                    case XmlNodeType.Element:
                        if (InputFile.Name == "ID")
                            this.ID = Convert.ToInt32(InputFile.ReadString());
                        else if (InputFile.Name == "NAME")
                            this.Name = InputFile.ReadString();
                        else if (InputFile.Name == "SOURCE")
                            this.Source = InputFile.ReadString();
                        else if (InputFile.Name == "DESCRIPTION")
                            this.Description = InputFile.ReadString();
                        else if (InputFile.Name == "DATUM")
                            this.Datum = InputFile.ReadString();
                        else if (InputFile.Name == "MAGNETIC_VARIATION")
                            this.MagneticVariation = InputFile.ReadString();
                        else if (InputFile.Name == "SLAVE_VARIATION")
                            this.SlaveVariation = InputFile.ReadString();
                        else if (InputFile.Name == "LATITUDE")
                            pfpsLatitude = InputFile.ReadString();
                        else if (InputFile.Name == "LONGITUDE")
                            pfpsLongitude = InputFile.ReadString();
                        else if (InputFile.Name == "VOR_FREQUENCY")
                            this.VORFrequency = InputFile.ReadString();
                        else if (InputFile.Name == "TACAN_CHANNEL")                                                        
                            this.TacanChannel = InputFile.ReadString();
                        else if (InputFile.Name == "VALUE")
                            this.Elevation = Convert.ToInt32(InputFile.ReadString());
                        break;
                }
            }            
        }
        #endregion
    }
}
