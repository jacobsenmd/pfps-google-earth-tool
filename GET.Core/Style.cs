/********************************************************************************/
/* Class:   GET.Core.Style                                                      */
/*                                                                              */
/* A "Style" is a reusable set of preferences that determine how objects will   */
/* look in Google Earth.  Every GETObject has a reference of type "Style"       */
/* called "StyleOptions" that determines appearance.                            */
/*                                                                              */
/* Public Properties:                                                           */
/*  string Name                 A name for the style.  Every reusable style     */
/*                              must have a unique name.                        */
/*  bool Extrude                Toggles whether or not paths and points will be */
/*                              extruded down to the surface.                   */
/*  Color FillColor             The fill color for polygons and extruded        */
/*                              regions of paths                                */
/*  string FillColorHex         A read-only property that gets the hexadecimal  */
/*                              conversion of the fill color.  Used to get      */
/*                              the color into a format .KML can understand.    */
/*  int FillOpacity             The opacity/transparency of polygons and        */
/*                              extruded regions (0-100)                        */
/*  Color IconColor             The color of the icon                           */
/*  string IconColorHex         A read-only property that gets the hexadecimal  */
/*                              conversion of the icon color.  Used to get      */
/*                              the color into a format .KML can understand.    */
/*  int IconOpacity             The opacity/transparency of icons (0-100)       */
/*  string IconPath             The path to the icon that will be used          */
/*  double IconScale            How large icons appear                          */
/*  Color LabelColor            The color of textual labels                     */
/*  string LabelColorHex        A read-only property that gets the hexadecimal  */
/*                              conversion of the label color.  Used to get     */
/*                              the color into a format .KML can understand.    */
/*  int LabelOpacity            The opacity/transparency of text labels (0-100) */
/*  double LabelScale           How large textual labels appear                 */
/*  Color LineColor             The color of paths and polygon wireframes       */
/*  string LineColorHex         A read-only property that gets the hexadecimal  */
/*                              conversion of the line color.  Used to get      */
/*                              the color into a format .KML can understand.    */
/*  double LineWidth            The width of paths and polygon wireframe lines  */
/*                                                                              */
/* Methods:                                                                     */
/*                                                                              */
/* Waypoint GetCopy() - COMPLETE                                                */
/*                                                                              */
/*      Most GETObject data types have a GetCopy() function, which generates    */
/*      and returns a deep copy of the object.  NOTE: because "Style" is not a  */
/*      GETObject, I don't believe this function is ever used.  It's included   */
/*      as a legacy function.                                                   */
/*                                                                              */
/* bool Export(XmlTextWriter OutputFile) - IN PROGRESS                          */
/*                                                                              */
/*      Writes the style to the specified .KML file.  Usually called from       */
/*      Project.CreateKML(), which creates the XmlTextWriter                    */
/********************************************************************************/


using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace GET.Core
{
    [Serializable]
    public class Style
    {
        #region Private Member Data        
        private string name = "<Custom>";
        private bool extrude = true;
        private Color lineColor = Color.Green;
        private Color fillColor = Color.DarkGreen;
        private Color labelColor = Color.White;
        private Color iconColor = Color.White;
        private double lineWidth = 1.0;
        private double labelScale = 1.0;
        private double iconScale = 1.0;
        private string iconPath = "http://maps.google.com/mapfiles/kml/shapes/donut.png";
        private int fillOpacity = 50;
        private int iconOpacity = 100;
        private int labelOpacity = 100;
        private int lineOpacity = 100;
        #endregion

        #region Public Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public bool Extrude
        {
            get { return extrude; }
            set { extrude = value; }
        }

        public Color LineColor
        {
            get { return lineColor; }
            set { lineColor = value; }
        }
        public Color FillColor
        {
            get { return fillColor; }
            set { fillColor = value; }
        }
        public Color LabelColor
        {
            get { return labelColor; }
            set { labelColor = value; }
        }
        public Color IconColor
        {
            get { return iconColor; }
            set { iconColor = value; }
        }

        public string LineColorHex
        {
            get
            {
                int convertedOpacity = lineOpacity * 255 / 100;
                return (convertedOpacity.ToString("x2") + lineColor.B.ToString("x2") + lineColor.G.ToString("x2") + lineColor.R.ToString("x2"));
            }
        }
        public string IconPath
        {
            get { return iconPath; }
            set { iconPath = value; }
        }
        public string FillColorHex
        {
            get
            {
                int convertedOpacity = fillOpacity * 255 / 100;
                return (convertedOpacity.ToString("x2") + fillColor.B.ToString("x2") + fillColor.G.ToString("x2") + fillColor.R.ToString("x2"));
            }
        }
        public string LabelColorHex
        {
            get
            {
                int convertedOpacity = labelOpacity * 255 / 100;
                return (convertedOpacity.ToString("x2") + labelColor.B.ToString("x2") + labelColor.G.ToString("x2") + labelColor.R.ToString("x2"));
            }
        }
        public string IconColorHex
        {
            get
            {
                int convertedOpacity = iconOpacity * 255 / 100;
                return (convertedOpacity.ToString("x2") + iconColor.B.ToString("x2") + iconColor.G.ToString("x2") + iconColor.R.ToString("x2"));
            }
        }

        public double LineWidth
        {
            get { return lineWidth; }
            set { lineWidth = value; }
        }
        public double LabelScale
        {
            get { return labelScale; }
            set { labelScale = value; }
        }
        public double IconScale
        {
            get { return iconScale; }
            set { iconScale = value; }
        }

        public int FillOpacity
        {
            get { return fillOpacity; }
            set { fillOpacity = value; }
        }
        public int IconOpacity
        {
            get { return iconOpacity; }
            set { iconOpacity = value; }
        }
        public int LabelOpacity
        {
            get { return labelOpacity; }
            set { labelOpacity = value; }
        }
        public int LineOpacity
        {
            get { return lineOpacity; }
            set { lineOpacity = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return this.Name;
        }

        /* Legacy function, no longer used
        public void CopyStyle(Style oldStyle)
        {
            this.Name = oldStyle.Name;
            this.Extrude = oldStyle.Extrude;
            this.FillColor = oldStyle.FillColor;
            this.FillOpacity = oldStyle.FillOpacity;
            this.IconColor = oldStyle.IconColor;
            this.IconOpacity = oldStyle.IconOpacity;
            this.IconScale = oldStyle.IconScale;
            this.LabelColor = oldStyle.LabelColor;
            this.LabelOpacity = oldStyle.LabelOpacity;
            this.LabelScale = oldStyle.LabelScale;
            this.LineColor = oldStyle.LineColor;
            this.LineOpacity = oldStyle.LineOpacity;
            this.LineWidth = oldStyle.LineWidth;
        }*/

        public Style GetCopy()
        {
            Style newStyle = new Style();
            newStyle.Name = this.Name;
            newStyle.Extrude = this.Extrude;
            newStyle.FillColor = this.FillColor;
            newStyle.FillOpacity = this.FillOpacity;
            newStyle.IconColor = this.IconColor;
            newStyle.IconOpacity = this.IconOpacity;
            newStyle.IconPath = this.IconPath;
            newStyle.IconScale = this.IconScale;
            newStyle.LabelColor = this.LabelColor;
            newStyle.LabelOpacity = this.LabelOpacity;
            newStyle.LabelScale = this.LabelScale;
            newStyle.LineColor = this.LineColor;
            newStyle.LineOpacity = this.LineOpacity;
            newStyle.LineWidth = this.LineWidth;
            return newStyle;
        }

        public void Export(XmlTextWriter OutputFile)
        {
            // If code calls this function with an overrideName, a custom
            // string can be used.  This allows for Custom.Name labels

            // This function saves the "Style" portion of the route

            // STEP 1: HEADER INFO
            OutputFile.WriteStartElement("Style");

            // These 2 lines take some explanation.  Style.Export() can be
            // called in two ways.  First, it can be called directly from
            // Project.CreateKML() for reusable Styles.  In this case, we
            // want to save an id name that references the reusable style.
            // Any GETObjects that use that style will simply reference
            // the id.
            //
            // The second way to call Style.Export() is for GETObjects
            // with custom styles to call it from within their own Export()
            // function.  A custom style will only be used once, so it
            // doesn't need an id tag.
            if(this.Name != "<Custom>")
                OutputFile.WriteAttributeString("id", this.Name);

            // Icon
            OutputFile.WriteStartElement("IconStyle");
            OutputFile.WriteStartElement("Icon");
            OutputFile.WriteStartElement("href");
            OutputFile.WriteString(iconPath);
            OutputFile.WriteEndElement(); // href
            OutputFile.WriteEndElement(); // Icon

            OutputFile.WriteStartElement("scale");
            OutputFile.WriteString(this.IconScale.ToString());
            OutputFile.WriteEndElement(); // scale

            OutputFile.WriteStartElement("color");
            OutputFile.WriteString(this.IconColorHex);
            OutputFile.WriteEndElement(); // color


            OutputFile.WriteEndElement(); // IconStyle

            // Line Style
            OutputFile.WriteStartElement("LineStyle");

            OutputFile.WriteStartElement("color");
            OutputFile.WriteString(this.LineColorHex);
            OutputFile.WriteEndElement(); // color

            OutputFile.WriteStartElement("width");
            OutputFile.WriteString(this.LineWidth.ToString());
            OutputFile.WriteEndElement(); // width

            OutputFile.WriteEndElement(); // LineStyle

            OutputFile.WriteStartElement("PolyStyle");
            OutputFile.WriteStartElement("color");
            OutputFile.WriteString(this.FillColorHex);
            OutputFile.WriteEndElement(); // color

            OutputFile.WriteStartElement("fill");
            OutputFile.WriteString("1");
            OutputFile.WriteEndElement(); // fill
            OutputFile.WriteEndElement(); // PolyStyle            

            OutputFile.WriteStartElement("LabelStyle");

            OutputFile.WriteStartElement("color");
            OutputFile.WriteString(this.LabelColorHex);
            OutputFile.WriteEndElement(); // color

            OutputFile.WriteStartElement("scale");
            OutputFile.WriteString(LabelScale.ToString());
            OutputFile.WriteEndElement(); // scale

            OutputFile.WriteEndElement(); // LabelStyle            
            OutputFile.WriteEndElement(); // Style
        }
        #endregion
    }
}
