// LatLon.cs
//
// This class represents a latitude-longitude coordinate.  The core of the class are two doubles: latitudeDouble and
// longitudeDouble.  Values are assigned using the Assign() function, which has a number of different implementations
// that accept different formats.
//
// A whole variety of Properties allow calling code to retrieve this value in different formats.  The computations to
// generate this data are done on the spot, within the property code.
//
// Properties:
//      double LatitudeDouble
//      double LongitudeDouble
//      string LatitudeDirection
//      string LongitudeDirection
//      int    LatitudeDegrees;
//      int    LongitudeDegrees;
//      int    LatitudeMinutes;
//      int    LongitudeMinutes;
//      double UnroundedLatitudeMinutes;
//      double UnroundedLongitudeMinutes;
//      double LatitudeSeconds;
//      double LongitudeSeconds;
//
// Assignment Functions:
//      Assign(double latDouble, double lonDouble)
//      Assign(string latitudePFPS, string longitudePFPS)
//      Assign(double latDir, double latDD, double latMM, double latSS, double LonDD, double LonMM, double LonSS)

using System;
using System.Collections.Generic;
using System.Text;

namespace GET.Core
{    
    // Latitude input must be in this format:  N32 5339.550
    // Longitude input must be in this format: W0800216.060
    // This is the PFPS standard.
    [Serializable]
    public class LatLon
    {
        #region Private Member Data
        private double latitudeDouble;
        private double longitudeDouble;        
        #endregion

        #region Public Properties
        
        public string LatitudeDirection
        {
            get
            {
                if (latitudeDouble >= 0)
                    return "N";
                else
                    return "S";
            }            
        }

        public string LongitudeDirection
        {
            get
            {
                if (longitudeDouble >= 0)
                    return "E";
                else
                    return "W";
            }
        }

        public int LatitudeDegrees
        {
            get
            {
                double latitudeAbs = Math.Abs(latitudeDouble);
                int wholeLatDegrees = Convert.ToInt32(Math.Floor(latitudeAbs));               
                return wholeLatDegrees;
            }           
        }


        public int LongitudeDegrees
        {
            get
            {
                double longitudeAbs = Math.Abs(longitudeDouble);
                int wholeLonDegrees = Convert.ToInt32(Math.Floor(longitudeAbs));
                return wholeLonDegrees;
            }
        }

        public int LatitudeMinutes
        {
            get
            {   // To get LatitudeMinutes we take MM.MMM and drop everything after the decimal place.
                // i.e. 47.82 returns 47.             
                int wholeLatMinutes = Convert.ToInt32(Math.Floor(UnroundedLatitudeMinutes));                
                return wholeLatMinutes;
            }            
        }

        public int LongitudeMinutes
        {
            get
            {   // To get LongitudeMinutes we take MM.MMM and drop everything after the decimal place.
                // i.e. 47.82 returns 47.             
                int wholeLonMinutes = Convert.ToInt32(Math.Floor(UnroundedLongitudeMinutes));
                return wholeLonMinutes;
            }
        }

        public double UnroundedLatitudeMinutes
        {
            get
            {
                // To get the minutes, we need to multiply the decimal portion of latitudeDouble by 60.
                // i.e. for 23.597 we need to multiply 0.597 by 60.
                // Formula is: (LatitudeDouble - LatitudeDegrees) * 60
                double latitudeAbs = Math.Abs(latitudeDouble);
                double unroundedLatMinutes = (latitudeAbs - (double)this.LatitudeDegrees) * 60;
                return unroundedLatMinutes;
            }
        }

        public double UnroundedLongitudeMinutes
        {
            get
            {
                // To get the minutes, we need to multiply the decimal portion of longitudeDouble by 60.
                // i.e. for 23.597 we need to multiply 0.597 by 60.
                // Formula is: (LongitudeDouble - LongitudeDegrees) * 60
                double longitudeAbs = Math.Abs(longitudeDouble);
                double unroundedLonMinutes = (longitudeAbs - (double)this.LongitudeDegrees) * 60;
                return unroundedLonMinutes;
            }
        }

        public double LatitudeSeconds
        {
            get
            {
                // To get seconds, we multiply the decimal portion of minutes by 60
                double latitudeSeconds = (UnroundedLatitudeMinutes - LatitudeMinutes) * 60;
                return latitudeSeconds;
            }         
        }       

        public double LongitudeSeconds
        {
            get
            {
                double longitudeSeconds = (UnroundedLongitudeMinutes - LongitudeMinutes) * 60;
                return longitudeSeconds;
            }          
        }

        public double LatitudeDouble
        {
            get { return latitudeDouble; }         
        }
        public double LongitudeDouble
        {
            get { return longitudeDouble; }
        }
        #endregion

        #region Methods

        // Empty constructor
        public LatLon()
        {
        }

        // Constructor that allows for initialization of coordinates
        public LatLon(double lat, double lon)
        {
            Assign(lat, lon);
        }

        public void Assign(double lat, double lon)
        {            
            this.latitudeDouble = lat;
            this.longitudeDouble = lon;
        }

        public void Assign(string pfpsLatitude, string pfpsLongitude)
        {            
            string LatDegString = pfpsLatitude.Substring(1, 2);
            double latitudeDegrees = Convert.ToInt32(LatDegString);
            string LatMinString = pfpsLatitude.Substring(4, 2);
            double latitudeMinutes = Convert.ToInt32(LatMinString);
            string LatSecString = pfpsLatitude.Substring(6);
            double latitudeSeconds = Convert.ToDouble(LatSecString);
            latitudeDouble = latitudeDegrees + (latitudeMinutes + latitudeSeconds / 60) / 60;
            if (pfpsLatitude[0] == 'S')            
                latitudeDouble = -latitudeDouble;
            
            string LonDegString = pfpsLongitude.Substring(1, 3);
            double longitudeDegrees = Convert.ToInt32(LonDegString);
            string LonMinString = pfpsLongitude.Substring(4, 2);
            double longitudeMinutes = Convert.ToInt32(LonMinString);
            string LonSecString = pfpsLongitude.Substring(6);
            double longitudeSeconds = Convert.ToDouble(LonSecString);
            longitudeDouble = longitudeDegrees + (longitudeMinutes + longitudeSeconds / 60) / 60;
            if (pfpsLongitude[0] == 'W')            
                longitudeDouble = -longitudeDouble;                
        }

        public void Assign(string LatDir, double LatD, double LatM, double LatS,
            string LonDir, double LonD, double LonM, double LonS)
        {                        
            latitudeDouble = LatD + (LatM + LatS / 60) / 60;
            if (LatDir == "S")
                latitudeDouble = -latitudeDouble;

            longitudeDouble = LonD + (LonM + LonS / 60) / 60;
            if (LonDir == "W")
                longitudeDouble = -longitudeDouble;
        }

        public LatLon GetCopy()
        {
            // Creates and returns a deep copy
            LatLon newCoords = new LatLon();
            newCoords.Assign(this.LatitudeDouble, this.LongitudeDouble);
            return newCoords;
        }

        public double GetBearingTo(LatLon target)
        {
            // This gets the bearing to the target coordinate in degrees
            double subtractLatitude = (target.LatitudeDouble - this.LatitudeDouble);
            double subtractLongitude = (target.LongitudeDouble - this.LongitudeDouble);
            double yNM = subtractLatitude * 60;
            double xNM = subtractLongitude * (60 * Math.Cos(target.latitudeDouble * Math.PI/180));
            double range = Math.Sqrt(xNM * xNM + yNM * yNM);
            double bearing = Math.Atan(Math.Abs(xNM)/Math.Abs(yNM)) * 180/Math.PI;

            // Put the bearing into the proper quadrant
            if (xNM < 0 && yNM > 0)
                bearing = 0 - bearing;
            else if (xNM < 0 && yNM < 0)
                bearing = 180 + bearing;
            else if (xNM > 0 && yNM < 0)
                bearing = 180 - bearing;

            // Correction for the errors that arctan causes, registering 0 or 90 as 180 and 270 respectively
            if (bearing == 0 && target.LatitudeDouble < this.LatitudeDouble)
                bearing = 180;
            if (bearing == 90 && target.LongitudeDouble < this.LongitudeDouble)
                bearing = 270;

            return bearing;
        }

        public double GetRangeTo(LatLon target)
        {
            // This gets the range in NM to a target LatLon.  It essentially is based off the RadialDME()
            // function, but works in reverse.
            double subtractLatitude = (target.LatitudeDouble - this.LatitudeDouble);
            double subtractLongitude = (target.LongitudeDouble - this.LongitudeDouble);
            double yNM = subtractLatitude * 60;
            double xNM = subtractLongitude * (60 * Math.Cos(target.latitudeDouble * Math.PI/180));
            double range = Math.Sqrt(xNM * xNM + yNM * yNM);
            return range;
        }

        public string GetLatitudeDeg()
        {
            string s;
            if (latitudeDouble > 0)
                s = "N";
            else
                s = "S";
            double AbsLatitudeDouble = Math.Abs(latitudeDouble);
            s += AbsLatitudeDouble.ToString("00.0000000");
            return s;
        }
        public string GetLatitudeDegMin()
        {
            string s;
            if (latitudeDouble > 0)
                s = "N";
            else
                s = "S";            
            s += LatitudeDegrees.ToString("00") + " " + UnroundedLatitudeMinutes.ToString("00.000");
            return s;
        }

        public string GetLatitudeDegMinSec()
        {
            string s;            
            if (latitudeDouble > 0)
                s = "N";
            else
                s = "S";
            s += LatitudeDegrees.ToString("00") + " " + LatitudeMinutes.ToString("00") + " " + LatitudeSeconds.ToString("00.000");
            return s;
        }

        public string GetLongitudeDeg()
        {
            string s;
            if (longitudeDouble > 0)
                s = "E";
            else
                s = "W";
            double AbsLongitudeDouble = Math.Abs(LongitudeDouble);
            s += AbsLongitudeDouble.ToString("000.0000000");
            return s;
        }

        public string GetLongitudeDegMin()
        {
            string s;
            if (longitudeDouble > 0)
                s = "E";
            else
                s = "W";            
            s += LongitudeDegrees.ToString("000") + " " + UnroundedLongitudeMinutes.ToString("00.000");
            return s;
        }

        public string GetLongitudeDegMinSec()
        {
            string s;
            if (longitudeDouble > 0)
                s = "E";
            else
                s = "W";
            s += LongitudeDegrees.ToString("000") + " " + LongitudeMinutes.ToString("00") + " " + LongitudeSeconds.ToString("00.000");
            return s;
        }

        public LatLon RadialDME(double radial, double DME)
        {
            // This function returns the lat-lon of a radial/DME based
            // off this position.

            // Convert from polar coordinates to cartesian coordinates
            double xNM = Math.Sin(radial * Math.PI / 180) * DME;
            double yNM = Math.Cos(radial * Math.PI / 180) * DME;        

            // Find how much lat/lon we need to shift the new point by.
            // Latitude is easy.  For each mile, we need to add one minute or 1/60 degrees.
            // Longitude is harder.  We must use the formula below.
            double addLatitude = yNM / 60;
            double addLongitude = xNM / (60 * Math.Cos(this.LatitudeDouble * Math.PI/180));

            // Create a new LatLon called 'result' which will add these additives
            LatLon result = new LatLon();
            result.Assign(this.latitudeDouble + addLatitude, this.longitudeDouble + addLongitude);


            return result;
        }

        public override string ToString()
        {
            string s = "";
            if (Statics.CoordinateStyle == 0)
            {
                // DD MM SS
                s = GetLatitudeDegMinSec() + ", " + GetLongitudeDegMinSec();
            }
            else if (Statics.CoordinateStyle == 1)
            {
                //DD MM.MMM
                s = GetLatitudeDegMin() + ", " + GetLongitudeDegMin();
            }
            else if (Statics.CoordinateStyle == 2)
            {
                // DD.DDDDD
                s = GetLatitudeDeg() + ", " + GetLongitudeDeg();
            }
            return s;
        }
    }

        #endregion
}
