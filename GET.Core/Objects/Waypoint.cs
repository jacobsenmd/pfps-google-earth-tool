/********************************************************************************/
/* Class:   GET.Core.Objects.Waypoint                                           */
/*                                                                              */
/* Waypoint is inherited from GETObject and is one of the simplest GET data     */
/* types.  Beyond the basic data of GETObject, it contains a lat-lon, an        */
/* altitude, and an altitude type.                                              */
/*                                                                              */
/* Public Properties:                                                           */
/*  int Altitude                Altitude/Elevation associated with the          */
/*                              waypoint.  Whether it's MSL or AGL will depend  */
/*                              on AltitudeType.                                */
/*  AltType AltitudeType        Specifies whether the waypoint is clamped to    */
/*                              to the ground or whether it's an MSL or AGL alt */
/*  List<GetObject> Children    Child objects which this object contains        */
/*  LatLon Coordinates          The lat-lon position of the waypoint            */
/*  string Description          A description for the object                    */
/*  DateTime EndTime            The end time associated with this feature.      */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  bool IncludeInExport        Toggles whether the object and its descendents  */
/*                              will be included in the .KML export             */
/*  string Name                 A name for the object                           */
/*  bool ShowVisible            Toggles whether the object is checked visible   */
/*                              when the .KML is loaded in GE                   */
/*  GETObject Parent            The parent object which owns this object        */
/*  string Snippet              In GE, a "snippet" is a short two-line          */
/*                              description that appears just beneath the name  */
/*                              of a feature.                                   */
/*  DateTime StartTime          The start time associated with this feature.    */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  Style StyleOptions          The "Style" object defining how this object     */
/*                              will appear in GE                               */
/*                                                                              */
/* Methods:                                                                     */
/*                                                                              */
/* Waypoint GetCopy() - COMPLETE                                                */
/*                                                                              */
/*      Most GETObject data types have a GetCopy() function, which generates    */
/*      and returns a deep copy of the object.                                  */
/*                                                                              */
/* bool Export(XmlTextWriter OutputFile) - COMPLETE                             */
/*                                                                              */
/*      Uses the waypoint data to write a GE Placemark to a .KML file.  This    */
/*      function will generally be called from Project.CreateKML(), which       */
/*      creates the .KML file and the XmlTextWriter.                            */
/*                                                                              */
/********************************************************************************/


using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace GET.Core.Objects
{
    [Serializable]
    public class Waypoint : GETObject
    {
        #region Private Member Data        
        private LatLon coordinates = new LatLon();                
        private AltType altitudeType = AltType.FixedAGL;
        private int altitude = 500;        
        #endregion
        
        #region Public Properties
        public LatLon Coordinates
        {
            get { return coordinates; }
            set { coordinates = value; }
        }                
        public AltType AltitudeType
        {
            get { return altitudeType; }
            set { altitudeType = value; }
        }
        public int Altitude
        {
            get { return altitude; }
            set { altitude = value; }
        }
        #endregion

        #region Methods
        public Waypoint()
        {
            Name = "New Waypoint";
            coordinates.Assign(0, 0);
        }

        public new Waypoint GetCopy()
        {
            Waypoint wcopy = new Waypoint();
            wcopy.Altitude = this.Altitude;
            wcopy.AltitudeType = this.AltitudeType;
            wcopy.Name = this.Name;
            wcopy.Description = this.Description;
            wcopy.Snippet = this.Snippet;
            wcopy.IncludeInExport = this.IncludeInExport;
            wcopy.ShowVisible = this.ShowVisible;
            wcopy.Coordinates = this.Coordinates.GetCopy();
            wcopy.StyleOptions = this.StyleOptions;
            wcopy.UseTimeStamp = this.UseTimeStamp;
            wcopy.StartTime = this.StartTime;
            wcopy.EndTime = this.EndTime;            

             foreach (GETObject obj in this.Children)
            {
                if (obj is Waypoint)
                {
                    Waypoint wp = ((Waypoint)obj).GetCopy();
                    wcopy.AddChild(wp);
                }
                else if (obj is Route)
                {
                    Route rte = ((Route)obj).GetCopy();
                    wcopy.AddChild(rte);
                }
                else if (obj is AirspaceRegion)
                {
                    AirspaceRegion ar = ((AirspaceRegion)obj).GetCopy();
                    wcopy.AddChild(ar);
                }
                else if(obj is Folder)
                {
                    Folder fold = ((Folder)obj).GetCopy();
                    wcopy.AddChild(fold);
                }
                else if (obj is Threat)
                {
                    Threat t = ((Threat)obj).GetCopy();
                    wcopy.AddChild(t);
                }
            }

            return wcopy;
        }                

        public override bool Export(XmlTextWriter OutputFile)
        {
            // Error check.  Only export if the OutputFile is valid.  Return
            // false if the file is invalid because an error exists.
            if (OutputFile == null)
                return false;
            // Check to see if the user wants this object included in the export.
            // If he doesn't, we still return "True" because the function is
            // working properly.
            if (this.IncludeInExport == false)
                return true;

            // Write basic data for the waypoint.
            OutputFile.WriteStartElement("Placemark");

            OutputFile.WriteStartElement("name");
            OutputFile.WriteString(this.Name);
            OutputFile.WriteEndElement(); // name

            OutputFile.WriteStartElement("Snippet");
            OutputFile.WriteString(this.Snippet);
            OutputFile.WriteEndElement();

            OutputFile.WriteStartElement("description");
            OutputFile.WriteString(this.Description);
            OutputFile.WriteEndElement(); // description

            OutputFile.WriteStartElement("visibility");
            if (this.ShowVisible == true)
                OutputFile.WriteString("1");
            else
                OutputFile.WriteString("0");
            OutputFile.WriteEndElement();

            // Timestamp
            if (this.UseTimeStamp == true)
            {
                OutputFile.WriteStartElement("TimeSpan");
                OutputFile.WriteStartElement("begin");
                OutputFile.WriteString(this.StartTimeKMLFormat);
                OutputFile.WriteEndElement(); // begin
                OutputFile.WriteStartElement("end");
                OutputFile.WriteString(this.EndTimeKMLFormat);
                OutputFile.WriteEndElement(); // end
                OutputFile.WriteEndElement(); // TimeSpan
            }

            // Now export the style.  This can take two different forms:
            // (1) Pre-defined Style is being used.  We need to reference the
            //     StyleID for the saved style.
            // (2) Custom style sheet name.  Export the style inline.
            if (this.StyleOptions.Name == "<Custom>")
            {
                // We have a custom style.  Export the style as inline.
                this.StyleOptions.Export(OutputFile);
            }
            else
            {
                // We have a reusable Style specified.  Export a reference
                // to the saved style.  See GET.Core.Project.Export() for
                // the code that exports the style.                
                OutputFile.WriteStartElement("styleUrl");
                OutputFile.WriteString("#" + this.StyleOptions.Name);
                OutputFile.WriteEndElement(); // styleUrl
            }                                                           
            
            // Now export the point data
            OutputFile.WriteStartElement("Point");
            OutputFile.WriteAttributeString("id", this.Name);

            OutputFile.WriteStartElement("extrude");
            if (this.StyleOptions.Extrude == true)
                OutputFile.WriteString("1");
            else
                OutputFile.WriteString("0");
            OutputFile.WriteEndElement(); // extrude

            OutputFile.WriteStartElement("altitudeMode");
            if (this.AltitudeType == AltType.FixedMSL)
                OutputFile.WriteString("absolute");
            else if (this.AltitudeType == AltType.ClampToGround)
                OutputFile.WriteString("clampToGround");
            else
                OutputFile.WriteString("relativeToGround");
            OutputFile.WriteEndElement(); // altitudeMode
            
            OutputFile.WriteStartElement("coordinates");
            double AltMeters;           // Altitude in meters (what GE uses)
            string exportAltitude = ""; // Altitude in feet, saved as a string            
            AltMeters = Convert.ToDouble(this.Altitude) / 3.2808399;            
            exportAltitude = AltMeters.ToString();
            OutputFile.WriteString(coordinates.LongitudeDouble + "," + coordinates.LatitudeDouble + "," + exportAltitude + " ");
            OutputFile.WriteEndElement(); // coordinates
            OutputFile.WriteEndElement(); // point
            OutputFile.WriteEndElement(); // placemark            

            return true;
        }
        #endregion
    }
}