/********************************************************************************/
/* Class:   GET.Core.Objects.Route                                              */
/*                                                                              */
/* This is derived from GETObject and contains all the data associated with a   */
/* PFPS route.  Beyond the basic data inherited from GETObject, it contains a   */
/* list of lat-lons and several options for how the data will be presented in   */
/* GE.  One of these route objects can produce two kinds of results in the      */
/* KML file: (1) a continous path from point to point and/or (2) a placemark at */
/* each turnpoint.                                                              */
/*                                                                              */
/* Public Properties:                                                           */
/*  int Altitude                Altitude/Elevation associated with the route.   */
/*                              Whether it's MSL or AGL will depend on          */
/*                              AltitudeType.                                   */
/*  AltType AltitudeType        Specifies whether the route is clamped to the   */
/*                              ground or whether it's an MSL or AGL altitude   */
/*  List<GetObject> Children    Child objects which this object contains        */
/*  string Description          A description for the object                    */
/*  DateTime EndTime            The end time associated with this feature.      */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  bool GenerateFullPath       Toggles whether a continuous path line will be  */
/*                              generated in the .KML file                      */
/*  bool GeneratePlacemarks     Toggles whether placemarks will be generated at */
/*                              each turnpoint                                  */
/*  bool IncludeInExport        Toggles whether the object and its descendents  */
/*                              will be included in the .KML export             */
/*  string Name                 A name for the object                           */
/*  List<LatLon> Points         A list of lat-lons that define the route        */
/*  bool ShowVisible            Toggles whether the object is checked visible   */
/*                              when the .KML is loaded in GE                   */
/*  GETObject Parent            The parent object which owns this object        */
/*  string Snippet              In GE, a "snippet" is a short two-line          */
/*                              description that appears just beneath the name  */
/*                              of a feature.                                   */
/*  DateTime StartTime          The start time associated with this feature.    */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  Style StyleOptions          The "Style" object defining how this object     */
/*                              will appear in GE                               */
/*                                                                              */
/* Route GetCopy() - COMPLETE                                                   */
/*                                                                              */
/*      Most GETObject data types have a GetCopy() function, which generates    */
/*      and returns a deep copy of the object.                                  */
/*                                                                              */
/* bool Export(XmlTextWriter OutputFile) - IN PROGRESS                          */
/*                                                                              */
/*      Generates .KML output to the XmlTextWriter based on the route data and  */
/*      the export settings.  This function will generally be called from       */
/*      Project.CreateKML(), which creates the .KML file and the XmlTextWriter. */
/*                                                                              */
/********************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using GET.Core.PFPS;
using GET.Core.PFPS.Internal;

namespace GET.Core.Objects
{
    [Serializable]
    public class Route : GETObject
    {
        #region Private Member Data
        private bool generateFullPath = true;
        private bool generatePlacemarks = true;                                  
        private List<Waypoint> points = new List<Waypoint>();
        private AltType altitudeType = AltType.FixedAGL;
        private int altitude = 500;
        #endregion

        #region Public Properties
        public List<Waypoint> Points
        {
            get { return points; }
            set { points = value; }
        }
        public bool GenerateFullPath
        {
            get { return generateFullPath; }
            set { generateFullPath = value; }
        }
        public bool GeneratePlacemarks
        {
            get { return generatePlacemarks; }
            set { generatePlacemarks = value; }
        }
        public AltType AltitudeType
        {
            get { return altitudeType; }
            set { altitudeType = value; }
        }
        public int Altitude
        {
            get { return altitude; }
            set { altitude = value; }
        }
        #endregion

        #region Methods
        public Route()
        {
            Reset();
        }        

        public new Route GetCopy()
        {           
            Route newRoute = new Route();
            newRoute.Altitude = this.Altitude;
            newRoute.AltitudeType = this.AltitudeType;            
            newRoute.Description = this.Description;
            newRoute.EndTime = this.EndTime;
            newRoute.GenerateFullPath = this.GenerateFullPath;
            newRoute.GeneratePlacemarks = this.GeneratePlacemarks;
            newRoute.IncludeInExport = this.IncludeInExport;
            newRoute.Name = this.Name;
            newRoute.ShowVisible = this.ShowVisible;
            newRoute.Snippet = this.Snippet;
            newRoute.StartTime = this.StartTime;
            newRoute.StyleOptions = this.StyleOptions;            
            foreach (Waypoint wp in this.Points)            
                newRoute.Points.Add(wp.GetCopy());            

             foreach (GETObject obj in this.Children)
            {
                if (obj is Waypoint)
                {
                    Waypoint wp = ((Waypoint)obj).GetCopy();
                    newRoute.AddChild(wp);
                }
                else if (obj is Route)
                {
                    Route rte = ((Route)obj).GetCopy();
                    newRoute.AddChild(rte);
                }
                else if (obj is AirspaceRegion)
                {
                    AirspaceRegion ar = ((AirspaceRegion)obj).GetCopy();
                    newRoute.AddChild(ar);
                }
                else if(obj is Folder)
                {
                    Folder fold = ((Folder)obj).GetCopy();
                    newRoute.AddChild(fold);
                }
                else if (obj is Threat)
                {
                    Threat t = ((Threat)obj).GetCopy();
                    newRoute.AddChild(t);
                }
            }

            return newRoute;
        }

        public void Reset()
        {
            Name = "New Route";
            Description = "";
            StyleOptions = new Style();
            points.Clear();
        }

       
                
        public override bool Export(XmlTextWriter OutputFile)
        {            
            // This function saves the data portion of the route to an XML KML file.
            // It is called from the KMLFile.Save() function.

            if (OutputFile == null)
                return false;

            // Both the path and all associated placemarks will use the name:
            // CUSTOM.ROUTE.NAME
            string urlName = "#" + this.StyleOptions.Name;
            if (this.StyleOptions.Name == "<Custom>")
                urlName = "#Custom.Route." + this.Name;
            
            OutputFile.WriteStartElement("Folder");
            
            OutputFile.WriteStartElement("name");
            OutputFile.WriteString(this.Name);
            OutputFile.WriteEndElement(); // name
            
            OutputFile.WriteStartElement("description");
            OutputFile.WriteString(this.Description);
            OutputFile.WriteEndElement(); // description

            OutputFile.WriteStartElement("Snippet");
            OutputFile.WriteString(this.Snippet);
            OutputFile.WriteEndElement(); // description

            OutputFile.WriteStartElement("visibility");
            if (this.ShowVisible == true)
                OutputFile.WriteString("1");
            else
                OutputFile.WriteString("0");
            OutputFile.WriteEndElement();

            // Timestamp
            if (this.UseTimeStamp == true)
            {
                OutputFile.WriteStartElement("TimeSpan");
                OutputFile.WriteStartElement("begin");
                OutputFile.WriteString(this.StartTimeKMLFormat);
                OutputFile.WriteEndElement(); // begin
                OutputFile.WriteStartElement("end");
                OutputFile.WriteString(this.EndTimeKMLFormat);
                OutputFile.WriteEndElement(); // end
                OutputFile.WriteEndElement(); // TimeSpan
            }

            if (this.GenerateFullPath == true)
            {
                OutputFile.WriteStartElement("Placemark");

                OutputFile.WriteStartElement("name");
                OutputFile.WriteString("Complete Route");
                OutputFile.WriteEndElement(); // name

                // Now export the style.  This can take two different forms:
                // (1) Pre-defined Style is being used.  We need to reference the
                //     StyleID for the saved style.
                // (2) Custom style sheet name.  Export the style inline.
                if (this.StyleOptions.Name == "<Custom>")
                {
                    // We have a custom style.  Export the style as inline.
                    this.StyleOptions.Export(OutputFile);
                }
                else
                {
                    // We have a reusable Style specified.  Export a reference
                    // to the saved style.  See GET.Core.Project.Export() for
                    // the code that exports the style.                
                    OutputFile.WriteStartElement("styleUrl");
                    OutputFile.WriteString("#" + this.StyleOptions.Name);
                    OutputFile.WriteEndElement(); // styleUrl
                }                                                           

                OutputFile.WriteStartElement("LineString");
                OutputFile.WriteAttributeString("id", this.Name + " - Full Path");

                OutputFile.WriteStartElement("extrude");
                if (this.StyleOptions.Extrude == true)
                    OutputFile.WriteString("1");
                else
                    OutputFile.WriteString("0");
                OutputFile.WriteEndElement(); // extrude

                OutputFile.WriteStartElement("altitudeMode");
                if (this.AltitudeType == AltType.FixedMSL)
                    OutputFile.WriteString("absolute");
                else if (this.AltitudeType == AltType.ClampToGround)
                    OutputFile.WriteString("clampToGround");
                else
                    OutputFile.WriteString("relativeToGround");
                OutputFile.WriteEndElement(); // altitudeMode

                OutputFile.WriteStartElement("tessellate");
                OutputFile.WriteString("1");
                OutputFile.WriteEndElement(); // tessellate

                OutputFile.WriteStartElement("coordinates");

                // This code block builds the lat/lon strings
                string s = "";
                foreach (Waypoint turnPoint in this.Points)
                {
                    string exportAltitude = "";
                    double AltMeters;                    
                    AltMeters = Convert.ToDouble(this.Altitude) / 3.2808399;                    
                    exportAltitude = AltMeters.ToString();
                    if (turnPoint.IncludeInExport == true)
                        s += turnPoint.Coordinates.LongitudeDouble + "," + turnPoint.Coordinates.LatitudeDouble + "," + exportAltitude + " ";
                }
                OutputFile.WriteString(s);
                OutputFile.WriteEndElement(); // coordinates
                OutputFile.WriteEndElement(); // LineString
                OutputFile.WriteEndElement(); // Placemark
                
            }

            // Now, write the placemarks associated with the route
            if (this.GeneratePlacemarks == true)
            {
                foreach (Waypoint gp in this.Points)
                {
                    if (gp.IncludeInExport == true)
                        gp.Export(OutputFile);
                }
            }

            OutputFile.WriteEndElement(); // Folder
            return true;
        }
        #endregion
    }
}
