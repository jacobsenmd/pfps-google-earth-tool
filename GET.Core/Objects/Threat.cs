using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace GET.Core.Objects
{
    [Serializable]
    public class Threat : GETObject
    {
        #region Private Member Data
        private LatLon coordinates = new LatLon();
        
        // Engagement ring
        private int engagementCeiling;
        private Color engagementFillColor;
        private Color engagementLineColor;        
        private double engagementRange;        
        private AltType engagementCeilingType;
        private bool showEngagementRing;        

        // Detection ring
        private int detectionCeiling;
        private Color detectionFillColor;
        private Color detectionLineColor;        
        private double detectionRange;
        private AltType detectionCeilingType;
        private bool showDetectionRing;

        // Probability ellipse
        private Color ellipseFillColor;
        private Color ellipseLineColor;        
        private double ellipseMajorAxis;
        private double ellipseMinorAxis;
        private int ellipseAngle;
        private bool showProbabilityEllipse;        
        #endregion

        #region Public Properties
        public LatLon Coordinates
        {
            get { return coordinates; }
            set { coordinates = value; }
        }
        public int EngagementCeiling
        {
            get { return engagementCeiling; }
            set { engagementCeiling = value; }
        }
        public double EngagementRange
        {
            get { return engagementRange; }
            set { engagementRange = value; }
        }
        public AltType EngagementCeilingType
        {
            get { return engagementCeilingType; }
            set { engagementCeilingType = value; }
        }
        public Color EngagementFillColor
        {
            get { return engagementFillColor; }
            set { engagementFillColor = value; }
        }
        public Color EngagementLineColor
        {
            get { return engagementLineColor; }
            set { engagementLineColor = value; }
        }
        public bool ShowEngagementRing
        {
            get { return showEngagementRing; }
            set { showEngagementRing = value; }
        }
        public int DetectionCeiling
        {
            get { return detectionCeiling; }
            set { detectionCeiling = value; }
        }
        public Color DetectionFillColor
        {
            get { return detectionFillColor; }
            set { detectionFillColor = value; }
        }
        public Color DetectionLineColor
        {
            get { return detectionLineColor; }
            set { detectionLineColor = value; }
        }
        public double DetectionRange
        {
            get { return detectionRange; }
            set { detectionRange = value; }
        }
        public AltType DetectionCeilingType
        {
            get { return detectionCeilingType; }
            set { detectionCeilingType = value; }
        }
        public bool ShowDetectionRing
        {
            get { return showDetectionRing; }
            set { showDetectionRing = value; }
        }
        public Color EllipseFillColor
        {
            get { return ellipseFillColor; }
            set { ellipseFillColor = value; }
        }
        public Color EllipseLineColor
        {
            get { return ellipseLineColor; }
            set { ellipseLineColor = value; }
        }
        public double EllipseMajorAxis
        {
            get { return ellipseMajorAxis; }
            set { ellipseMajorAxis = value; }
        }
        public double EllipseMinorAxis
        {
            get { return ellipseMinorAxis; }
            set { ellipseMinorAxis = value; }
        }
        public int EllipseAngle
        {
            get { return ellipseAngle; }
            set { ellipseAngle = value; }
        }
        public bool ShowProbabilityEllipse
        {
            get { return showProbabilityEllipse; }
            set { showProbabilityEllipse = value; }
        }

        #endregion

        #region Methods
        public Threat()
        {
            Name = "New Threat";
            showProbabilityEllipse = false;
            showDetectionRing = false;
            ShowEngagementRing = false;
            ellipseFillColor = Color.DarkBlue;
            ellipseLineColor = Color.Blue;
            detectionFillColor = Color.Gold;
            detectionLineColor = Color.Yellow;
            engagementFillColor = Color.DarkRed;
            engagementLineColor = Color.Red;
            showEngagementRing = true;
        }

        public new Threat GetCopy()
        {            
            Threat tcopy = new Threat();

            // Base properties
            tcopy.Name = this.Name;
            tcopy.Description = this.Description;
            tcopy.Snippet = this.Snippet;
            tcopy.IncludeInExport = this.IncludeInExport;
            tcopy.ShowVisible = this.ShowVisible;
            tcopy.Coordinates = this.Coordinates.GetCopy();
            tcopy.StyleOptions = this.StyleOptions;
            tcopy.UseTimeStamp = this.UseTimeStamp;
            tcopy.StartTime = this.StartTime;
            tcopy.EndTime = this.EndTime;

            // Other properties
            tcopy.Coordinates = this.Coordinates.GetCopy();

            tcopy.DetectionCeiling = this.DetectionCeiling;
            tcopy.DetectionCeilingType = this.DetectionCeilingType;
            tcopy.DetectionRange = this.DetectionRange;
            tcopy.DetectionFillColor = this.DetectionFillColor;
            tcopy.DetectionLineColor = this.DetectionLineColor;
            
            tcopy.EllipseAngle = this.EllipseAngle;
            tcopy.EllipseMajorAxis = this.EllipseMajorAxis;
            tcopy.EllipseMinorAxis = this.EllipseMinorAxis;
            tcopy.EllipseFillColor = this.EllipseFillColor;
            tcopy.EllipseLineColor = this.EllipseLineColor;
            
            tcopy.EngagementCeiling = this.EngagementCeiling;
            tcopy.EngagementCeilingType = this.EngagementCeilingType;
            tcopy.EngagementRange = this.EngagementRange;
            tcopy.EngagementFillColor = this.EngagementFillColor;
            tcopy.EngagementLineColor = this.EngagementLineColor;
            
            tcopy.ShowDetectionRing = this.ShowDetectionRing;
            tcopy.ShowEngagementRing = this.ShowEngagementRing;
            tcopy.ShowProbabilityEllipse = this.ShowProbabilityEllipse;

            foreach (GETObject obj in this.Children)
            {
                if (obj is Waypoint)
                {
                    Waypoint wp = ((Waypoint)obj).GetCopy();
                    tcopy.AddChild(wp);
                }
                else if (obj is Route)
                {
                    Route rte = ((Route)obj).GetCopy();
                    tcopy.AddChild(rte);
                }
                else if (obj is AirspaceRegion)
                {
                    AirspaceRegion ar = ((AirspaceRegion)obj).GetCopy();
                    tcopy.AddChild(ar);
                }
                else if (obj is Folder)
                {
                    Folder fold = ((Folder)obj).GetCopy();
                    tcopy.AddChild(fold);
                }
                else if (obj is Threat)
                {
                    Threat t = ((Threat)obj).GetCopy();
                    tcopy.AddChild(t);
                }
            }

            return tcopy;
        }

        public override bool Export(System.Xml.XmlTextWriter OutputFile)
        {
            // This function works differently from the Export() functions of waypoints, folders, and airspace regions.
            // Rather than creating a unique kind of object, it exports a folder that contains a waypoint, probability
            // ellipse, detection ring, and engagement ring.

            // Create the threat folder
            Folder threatFolder = new Folder();
            threatFolder.Name = this.Name;
            threatFolder.Description = this.Description;
            threatFolder.Snippet = this.Snippet;
            threatFolder.IncludeInExport = this.IncludeInExport;
            threatFolder.ShowVisible = this.ShowVisible;
            threatFolder.StyleOptions = this.StyleOptions;
            threatFolder.UseTimeStamp = this.UseTimeStamp;
            threatFolder.StartTime = this.StartTime;
            threatFolder.EndTime = this.EndTime;            

            // Create the waypoint
            Waypoint wp = new Waypoint();
            wp.Coordinates = this.Coordinates;
            wp.Description = this.Description;

            wp.Description += "<br><br><b>DETECTION RANGE: </b>" + this.DetectionRange.ToString() + " NM<br>";
            if (this.DetectionCeilingType == AltType.FixedAGL)
                wp.Description += "<b>DETECTION CEILING: </b>" + this.DetectionCeiling.ToString() + " AGL<br><br>";
            else
                wp.Description += "<b>DETECTION CEILING: </b>" + this.DetectionCeiling.ToString() + " MSL<br><br>";

            wp.Description += "<b>ENGAGEMENT RANGE: </b>" + this.EngagementRange.ToString() + " NM<br>";
            if (this.EngagementCeilingType == AltType.FixedAGL)
                wp.Description += "<b>ENGAGEMENT CEILING: </b>" + this.EngagementCeiling.ToString() + " AGL<br><br>";
            else
                wp.Description += "<b>ENGAGEMENT CEILING: </b>" + this.EngagementCeiling.ToString() + " MSL<br><br>";



            wp.Name = threatFolder.Name;
            wp.Snippet = "";
            wp.IncludeInExport = true;
            wp.ShowVisible = true;
            wp.StyleOptions = this.StyleOptions;
            wp.UseTimeStamp = this.UseTimeStamp;
            wp.StartTime = this.StartTime;
            wp.EndTime = this.EndTime;


            // Create the probability ellipse
            AirspaceRegion probabilityEllipse = new AirspaceRegion();
            probabilityEllipse.Name = "Probability Ellipse";
            probabilityEllipse.CenterPoint = this.Coordinates;
            probabilityEllipse.Snippet = "";
            probabilityEllipse.Description = "";
            probabilityEllipse.GeneratePlacemark = false;
            probabilityEllipse.StyleOptions = this.StyleOptions.GetCopy();
            probabilityEllipse.StyleOptions.FillColor = this.EllipseFillColor;
            probabilityEllipse.StyleOptions.LineColor = this.EllipseLineColor;
            probabilityEllipse.AirspaceRegionType = AirspaceType.Ellipse;
            probabilityEllipse.VerticalRadius = Convert.ToDecimal(this.EllipseMajorAxis);
            probabilityEllipse.HorizontalRadius = Convert.ToDecimal(this.ellipseMinorAxis);
            probabilityEllipse.Angle = this.EllipseAngle;
            probabilityEllipse.AltitudeType = AltType.ClampToGround;
            probabilityEllipse.ShowVisible = this.ShowProbabilityEllipse;
            probabilityEllipse.UseTimeStamp = this.UseTimeStamp;
            probabilityEllipse.StartTime = this.StartTime;
            probabilityEllipse.EndTime = this.EndTime;

            // Create the engagement ring
            AirspaceRegion engagementRing = new AirspaceRegion();
            engagementRing.Name = "Engagement Ring";
            engagementRing.CenterPoint = this.Coordinates;
            engagementRing.Snippet = "";
            engagementRing.Description = "";
            engagementRing.GeneratePlacemark = false;
            engagementRing.StyleOptions = this.StyleOptions.GetCopy();
            engagementRing.StyleOptions.FillColor = this.engagementFillColor;
            engagementRing.StyleOptions.LineColor = this.EngagementLineColor;
            engagementRing.AirspaceRegionType = AirspaceType.Circle;
            engagementRing.OuterRadius = Convert.ToDecimal(this.EngagementRange);
            engagementRing.AltitudeFloor = 0;
            engagementRing.AltitudeCeiling = this.EngagementCeiling;
            engagementRing.AltitudeType = this.EngagementCeilingType;
            engagementRing.ShowVisible = this.ShowEngagementRing;
            engagementRing.UseTimeStamp = this.UseTimeStamp;
            engagementRing.StartTime = this.StartTime;
            engagementRing.EndTime = this.EndTime;

            // Create the detection ring
            AirspaceRegion detectionRing = new AirspaceRegion();
            detectionRing.Name = "Detection Ring";
            detectionRing.CenterPoint = this.Coordinates;
            detectionRing.Snippet = "";
            detectionRing.Description = "";
            detectionRing.GeneratePlacemark = false;
            detectionRing.StyleOptions = this.StyleOptions.GetCopy();
            detectionRing.StyleOptions.FillColor = this.detectionFillColor;
            detectionRing.StyleOptions.LineColor = this.detectionLineColor;
            detectionRing.AirspaceRegionType = AirspaceType.Circle;
            detectionRing.OuterRadius = Convert.ToDecimal(this.DetectionRange);
            detectionRing.AltitudeFloor = 0;
            detectionRing.AltitudeCeiling = this.DetectionCeiling;
            detectionRing.AltitudeType = this.DetectionCeilingType;
            detectionRing.ShowVisible = this.ShowDetectionRing;
            detectionRing.UseTimeStamp = this.UseTimeStamp;
            detectionRing.StartTime = this.StartTime;
            detectionRing.EndTime = this.EndTime;

            threatFolder.AddChild(wp);
            if(this.EllipseMajorAxis > 0 && this.ellipseMinorAxis > 0)
                threatFolder.AddChild(probabilityEllipse);
            if(this.DetectionRange > 0)
                threatFolder.AddChild(detectionRing);
            if(this.EngagementRange > 0)
                threatFolder.AddChild(engagementRing);

            threatFolder.Export(OutputFile);
            return true;
        }

        #endregion
    }
}
