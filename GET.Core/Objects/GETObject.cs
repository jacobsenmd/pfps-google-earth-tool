/********************************************************************************/
/* Class:   GET.Core.Objects.GETObject                                          */
/*                                                                              */
/* A GETObject (Google Earth Tool Object) is the parent class for all other     */
/* Google Earth Objects used in the softwars (folders, waypoints, routes, etc). */
/* This class is rarely used directly.  However, it defines much of the data    */
/* shared by all object types.                                                  */
/*                                                                              */
/* Public Properties:                                                           */
/*  List<GetObject> Children    Child objects which this object contains        */
/*  string Description          A description for the object                    */
/*  DateTime EndTime            The end time associated with this feature.      */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  string EndTimeKMLFormat     Returns a string of the end time in the format  */
/*                              that KML uses. (get only)                       */
/*  bool IncludeInExport        Toggles whether the object and its descendents  */
/*                              will be included in the .KML export             */
/*  string Name                 A name for the object                           */
/*  bool ShowVisible            Toggles whether the object is checked visible   */
/*                              when the .KML is loaded in GE                   */
/*  GETObject Parent            The parent object which owns this object        */
/*  string Snippet              In GE, a "snippet" is a short two-line          */
/*                              description that appears just beneath the name  */
/*                              of a feature.                                   */
/*  DateTime StartTime          The start time associated with this feature.    */
/*                              Only applies if UseTimeStamp is set TRUE.       */
/*  string StarTimeKMLFormat    Returns a string of the start time in the       */
/*                              format that KML uses. (get only)                */
/*  Style StyleOptions          The "Style" object defining how this object     */
/*                              will appear in GE                               */
/*                                                                              */
/* void AddChild(GETObject obj) - COMPLETE                                      */
/*                                                                              */
/*      Adds a child object to this object.  This is the preferred way to add   */
/*      a child (rather than editing the "Children" list directly) because it   */
/*      also sets the child's Parent to this object.                            */
/*                                                                              */
/* bool Export(XmlTextWriter OutputFile) - COMPLETE                             */
/*                                                                              */
/*      This is a virtual function that does not accomplish anything.  It is    */
/*      included as a model for the Export() method of inherited data types.    */
/*                                                                              */
/* GETObject GetCopy() - COMPLETE                                               */
/*                                                                              */
/*      Generates and returns a deep copy of the object.                        */
/********************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace GET.Core.Objects
{
    [Serializable]
    public class GETObject
    {
        #region Private Member Data
        private string description;
        private DateTime endTime;        
        private bool includeInExport;        
        private string name;
        private bool showVisible;
        private string snippet;
        private DateTime startTime;        
        private Style styleOptions = new Style();
        private bool useTimeStamp;        
        #endregion

        #region Public Properties
        public List<GETObject> Children = new List<GETObject>();
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }
        public string EndTimeKMLFormat
        {
            get
            {
                string s = endTime.Year.ToString("0000") + "-" + endTime.Month.ToString("00") + "-" + endTime.Day.ToString("00");
                s += "T";
                s += endTime.Hour.ToString("00") + ":" + endTime.Minute.ToString("00") + ":" + endTime.Second.ToString("00");
                return s;
            }
        }
        public bool IncludeInExport
        {
            get { return includeInExport; }
            set { includeInExport = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public GETObject Parent;
        public bool ShowVisible
        {
            get { return showVisible; }
            set { showVisible = value; }
        }
        public string Snippet
        {
            get { return snippet; }
            set { snippet = value; }
        }
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }
        public string StartTimeKMLFormat
        {
            get
            {
                string s = startTime.Year.ToString("0000") + "-" + startTime.Month.ToString("00") + "-" + startTime.Day.ToString("00");
                s += "T";
                s += startTime.Hour.ToString("00") + ":" + startTime.Minute.ToString("00") + ":" + startTime.Second.ToString("00");
                return s;
            }
        }
        public Style StyleOptions
        {
            get { return styleOptions; }
            set { styleOptions = value; }
        }
        public bool UseTimeStamp
        {
            get { return useTimeStamp; }
            set { useTimeStamp = value; }
        }        
        #endregion

        #region Methods

        // GETObject()
        // This is the default constructor for a new object
        public GETObject()
        {
            description = "";            
            includeInExport = true;
            name = "New Object";
            Parent = null;            
            showVisible = true;
            snippet = "";            
            useTimeStamp = false;
            startTime = DateTime.Now;
            endTime = DateTime.Now;
        }            

        // AddChild()
        // This adds the specified object as a child, and sets
        // "this" as the child object's parent
        public bool AddChild(GETObject obj)
        {            
            if (obj == null)
                return false;

            obj.Parent = this;
            Children.Add(obj);
            return true;
        }

        // Export()
        // This virtual function most be overriden by all objects derived
        // from GETObject.  Given an output file, it exports this object in
        // KML format
        public virtual bool Export(XmlTextWriter OutputFile)
        {
            return true;
        }

        public GETObject GetCopy()
        {
            GETObject newObject = new GETObject();
            newObject.Description = this.Description;
            newObject.IncludeInExport = this.IncludeInExport;
            newObject.Name = this.Name;
            newObject.ShowVisible = this.ShowVisible;
            newObject.Snippet = this.Snippet;
            newObject.UseTimeStamp = this.UseTimeStamp;
            newObject.StyleOptions = this.StyleOptions;
            newObject.StartTime = this.StartTime;
            newObject.EndTime = this.EndTime;

            return newObject;
        }
                
        #endregion
    }
}
