// CONTROL: AltitudeSelector
//
// This control is used to adjust the altitude properties for a feature.  It allows
// the user to set two basic properties: the altitude and the altitude type.
//
// Public Properties:
//    Altitude                  Returns the altitude, in feet (either AGL or MSL),
//                              depending on the type of altitude selected
//    AltitudeAGL               Returns whatever is in the AGL text box
//    AltitudeMSL               Returns whatever is in the MSL text box
//    AltitudeType              Type "AltType".  Can be Fixed AGL, Fixed MSL, or
//                              Clamp to Ground -- the 3 kinds of altitude GE uses
//
// Methods:
//    AltitudeSelector()        Initialize to default settings (set in GET.Core.Statics)
//    AltitudeSelector(type,alt)  Initialize to a certain type and altitude

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GET.Core;

namespace GET.Controls
{
    public partial class AltitudeSelector : UserControl
    {
        #region Private Member Data        
        private int altitudeMSL;
        private int altitudeAGL;
        private AltType altitudeType;        
        #endregion

        #region Public Properties
        public override string Text
        {
            set
            {                
                groupBox1.Text = value;
            }
            get { return groupBox1.Text; }
        }

        public int AltitudeAGL
        {
            set
            {
                altitudeAGL = value;
                textBoxAGL.Text = altitudeAGL.ToString();
            }
            get { return altitudeMSL; }
        }

        public int AltitudeMSL
        {
            set
            {
                altitudeMSL = value;
                textBoxMSL.Text = altitudeMSL.ToString();
            }
            get { return altitudeMSL; }
        }

        public AltType AltitudeType
        {
            set
            {
                // If the user specifies an altitude type, we check the
                // appropriate radio button.  This will throw a Checked
                // Change event.  The handler for that function will
                // update the text boxes.
                altitudeType = value;
                if(altitudeType == AltType.ClampToGround)                
                    radioClamp.Checked = true;
                else if(altitudeType == AltType.FixedAGL)
                    radioAGL.Checked = true;
                else if(altitudeType == AltType.FixedMSL)
                    radioMSL.Checked = true;
            }
            get { return altitudeType; }
        }

        public int Altitude
        {
            set
            {
                if (altitudeType == AltType.FixedAGL)
                    textBoxAGL.Text = AltitudeAGL.ToString();
                else if (altitudeType == AltType.FixedMSL)
                    textBoxMSL.Text = altitudeMSL.ToString();                
            }
            get
            {
                if (altitudeType == AltType.FixedAGL)
                    return Convert.ToInt32(textBoxAGL.Text);
                else if (altitudeType == AltType.FixedMSL)
                    return Convert.ToInt32(textBoxMSL.Text);
                else
                    return 0;
            }
        }
        #endregion

        #region Methods

        public AltitudeSelector()
        {
            // Default constructor.  Creates the control using the default
            // values specified in GET.Core.Statics
            InitializeComponent();
            AltitudeAGL = GET.Core.Statics.DefaultAltitudeAGL;
            AltitudeMSL = GET.Core.Statics.DefaultAltitudeMSL;
            AltitudeType = GET.Core.Statics.DefaultAltitudeType;            
        }

        public AltitudeSelector(AltType altType, int altValue)
        {
            // This version of the constructor lets the user create the
            // control with an altitude type and altitude already
            // specified.            
            AltitudeType = altType;
            Altitude = altValue;
        }

        private void OnAltitudeTypeChanged(object sender, EventArgs e)
        {
            if (radioClamp.Checked == true)
            {
                altitudeType = AltType.ClampToGround;
                textBoxAGL.Enabled = false;
                textBoxMSL.Enabled = false;
            }
            else if (radioAGL.Checked == true)
            {
                altitudeType = AltType.FixedAGL;
                textBoxAGL.Enabled = true;
                textBoxMSL.Enabled = false;
            }
            else if (radioMSL.Checked == true)
            {
                altitudeType = AltType.FixedMSL;
                textBoxAGL.Enabled = false;
                textBoxMSL.Enabled = false;
            }
        }
        #endregion

        private void OnValidateTextBox(object sender, EventArgs e)
        {
            // Altitude options
            altitudeMSL = Convert.ToInt32(textBoxMSL.Text);
            altitudeAGL = Convert.ToInt32(textBoxAGL.Text);                           
        }
    }
}
