using System;
using System.Collections.Generic;
using System.Text;

namespace GET.Controls.Icons
{
    [Serializable]
    public class IconLibrary
    {
        public string Name;
        public string Path;
        public string LocalPath;

        public IconLibrary(string name, string path, string localPath)
        {
            Name = name;
            Path = path;
            LocalPath = localPath;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
