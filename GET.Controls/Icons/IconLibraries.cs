using System;
using System.Collections.Generic;
using System.Text;

namespace GET.Controls.Icons
{
    [Serializable]
    public class IconLibraries
    {
        public List<IconLibrary> Libraries = new List<IconLibrary>();

        public void Add(IconLibrary lib)
        {
            Libraries.Add(lib);
        }
    }
}
