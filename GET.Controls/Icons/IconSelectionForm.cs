using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Xml.Serialization;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using GET.Core;

namespace GET.Controls.Icons
{
    public partial class IconSelectionForm : Form
    {        
        public string IconPath;        

        private IconLibraries iconLibraries = new IconLibraries();
        private IconLibrary activeLibrary
        {
            get { return (IconLibrary)listBoxLibraries.SelectedItem; }
        }

        public IconSelectionForm()
        {            
            InitializeComponent();
            CreateLibraryData();           
            UpdateLibraryList();
            listBoxLibraries.SelectedIndex = 0;
        }

        private void CreateLibraryData()
        {
            iconLibraries.Add(new IconLibrary("Google Earth Paddles",@"http://maps.google.com/mapfiles/kml/paddle/",Statics.GoogleEarthPath + @"\res\paddle\"));
            iconLibraries.Add(new IconLibrary("Google Earth Pushpins",@"http://maps.google.com/mapfiles/kml/pushpin/",Statics.GoogleEarthPath + @"\res\pushpin\"));
            iconLibraries.Add(new IconLibrary("Google Earth Shapes",@"http://maps.google.com/mapfiles/kmol/shapes/",Statics.GoogleEarthPath + @"\res\shapes\"));
            iconLibraries.Add(new IconLibrary("NGA Symbols - ICAO", null, Statics.ApplicationPath + @"\Icons\ICAO\"));
            iconLibraries.Add(new IconLibrary("NGA Symbols - DVOF", null, Statics.ApplicationPath + @"\Icons\DVOF\"));
            iconLibraries.Add(new IconLibrary("NGA Symbols - GTRI", null, Statics.ApplicationPath + @"\Icons\GTRI\"));
            iconLibraries.Add(new IconLibrary("FalconView Local Points",null, Statics.PFPSPath + @"\falcon\data\icons\localpnt\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: AAA",null,Statics.PFPSPath + @"\falcon\data\icons\threat\aaa\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: AOB-25",null,Statics.PFPSPath + @"\falcon\data\icons\threat\aob-25\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: AOB-Fvw", null, Statics.PFPSPath + @"\falcon\data\icons\threat\aob-Fvw\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: Blue Force Tracking", null, Statics.PFPSPath + @"\falcon\data\icons\threat\blue_force_tracking\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: ELINT Air", null, Statics.PFPSPath + @"\falcon\data\icons\threat\elint\air\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: ELINT Generic", null, Statics.PFPSPath + @"\falcon\data\icons\threat\elint\generic\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: ELINT Gnd", null, Statics.PFPSPath + @"\falcon\data\icons\threat\elint\gnd\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: ELINT Jammer", null, Statics.PFPSPath + @"\falcon\data\icons\threat\elint\jammer\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: ELINT Nvl", null, Statics.PFPSPath + @"\falcon\data\icons\threat\elint\nvl\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: ELINT Space", null, Statics.PFPSPath + @"\falcon\data\icons\threat\elint\space\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: ELINT Tng", null, Statics.PFPSPath + @"\falcon\data\icons\threat\elint\tng\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: EQP", null, Statics.PFPSPath + @"\falcon\data\icons\threat\eqp\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: Generic", null, Statics.PFPSPath + @"\falcon\data\icons\threat\generic\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: GOB-CBT", null, Statics.PFPSPath + @"\falcon\data\icons\threat\gob-cbt\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: GOB-CS", null, Statics.PFPSPath + @"\falcon\data\icons\threat\gob-cs\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: GOB-CSS", null, Statics.PFPSPath + @"\falcon\data\icons\threat\gob-css\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: GOB-GND", null, Statics.PFPSPath + @"\falcon\data\icons\threat\gob-gnd\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: INS", null, Statics.PFPSPath + @"\falcon\data\icons\threat\ins\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: MOB Missile", null, Statics.PFPSPath + @"\falcon\data\icons\threat\mob\missile\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: MOB System", null, Statics.PFPSPath + @"\falcon\data\icons\threat\mob\system\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: MOB Tng", null, Statics.PFPSPath + @"\falcon\data\icons\threat\mob\tng\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: MOOTW", null, Statics.PFPSPath + @"\falcon\data\icons\threat\mootw\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: NOB-25", null, Statics.PFPSPath + @"\falcon\data\icons\threat\nob-25\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: NOB-Fvw", null, Statics.PFPSPath + @"\falcon\data\icons\threat\nob-fvw\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: SIGINT", null, Statics.PFPSPath + @"\falcon\data\icons\threat\sigint\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: SOF", null, Statics.PFPSPath + @"\falcon\data\icons\threat\sof\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: SPC", null, Statics.PFPSPath + @"\falcon\data\icons\threat\spc\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: TAC", null, Statics.PFPSPath + @"\falcon\data\icons\threat\tac\"));
            iconLibraries.Add(new IconLibrary("FalconView Threats: Other", null, Statics.PFPSPath + @"\falcon\data\icons\threat\"));
        }

        private void OpenLibraryData()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(IconLibraries));
            Stream fileStream;
            try
            {
                fileStream = new FileStream("Icon Libraries.dat",FileMode.Open, FileAccess.Read,FileShare.None);
            }
            catch
            {
                CreateLibraryData();
                return;
            }
            iconLibraries = (IconLibraries)serializer.Deserialize(fileStream);
            fileStream.Close();
        }

        private void SaveLibraryData()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(IconLibraries));
            Stream fileStream = new FileStream("Icon Libraries.dat",FileMode.Create, FileAccess.Write,FileShare.None);
            serializer.Serialize(fileStream, iconLibraries);
            fileStream.Close();
        }

        private void UpdateLibraryList()
        {
            foreach (IconLibrary lib in iconLibraries.Libraries)
            {
                listBoxLibraries.Items.Add(lib);
            }
        }

        private void OnIconLibraryChanged(object sender, EventArgs e)
        {            
            listViewIcons.Items.Clear();
            imageListActive.Images.Clear();            
            if (activeLibrary == null)
                return;
            try
            {
                DirectoryInfo di = new DirectoryInfo(activeLibrary.LocalPath);
                FileInfo[] fi = di.GetFiles();
                foreach (FileInfo fileInfo in fi)
                {

                    if (fileInfo.Extension == ".ico" ||
                        fileInfo.Extension == ".png" ||
                        fileInfo.Extension == ".jpg" ||
                        fileInfo.Extension == ".bmp" ||
                        fileInfo.Extension == ".gif" ||
                        fileInfo.Extension == ".tiff" ||
                        fileInfo.Extension == ".jpeg" ||
                        fileInfo.Extension == ".tga")
                    {
                        ListViewItem lvi = new ListViewItem();
                        lvi.Text = fileInfo.Name;
                        Image newImage = Image.FromFile(fileInfo.FullName);
                        imageListActive.Images.Add(newImage);
                        lvi.ImageIndex = imageListActive.Images.Count - 1;
                        listViewIcons.Items.Add(lvi);
                    }
                }
            }
            catch
            {
                MessageBox.Show("This library could not be found.  On the menu, please select Edit-->Options and ensure that you have correctly set the paths for FalconView and Google Earth.  For more information on using icons, please visit www.googleearthpilot.com", "Library Not Found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void OnIconChanged(object sender, EventArgs e)
        {
            if (listViewIcons.SelectedItems.Count > 0)
                IconPath = activeLibrary.LocalPath + listViewIcons.SelectedItems[0].Text;
            else
                IconPath = "";            
        }

        private void OnListViewDoubleClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}