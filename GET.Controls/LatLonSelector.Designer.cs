namespace GET.Controls
{
    partial class LatLonSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericLon3Minutes = new System.Windows.Forms.NumericUpDown();
            this.numericLon3Degrees = new System.Windows.Forms.NumericUpDown();
            this.textBoxLon3EW = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.numericLat3Minutes = new System.Windows.Forms.NumericUpDown();
            this.numericLat3Degrees = new System.Windows.Forms.NumericUpDown();
            this.textBoxLat3NS = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.radioButtonFormatDDMM = new System.Windows.Forms.RadioButton();
            this.numericLon2 = new System.Windows.Forms.NumericUpDown();
            this.numericLat2 = new System.Windows.Forms.NumericUpDown();
            this.numericLon1Seconds = new System.Windows.Forms.NumericUpDown();
            this.numericLon1Minutes = new System.Windows.Forms.NumericUpDown();
            this.numericLon1Degrees = new System.Windows.Forms.NumericUpDown();
            this.textBoxLon1EW = new System.Windows.Forms.TextBox();
            this.numericLat1Seconds = new System.Windows.Forms.NumericUpDown();
            this.numericLat1Minutes = new System.Windows.Forms.NumericUpDown();
            this.numericLat1Degrees = new System.Windows.Forms.NumericUpDown();
            this.radioButtonFormatDDDD = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.radioButtonFormatDDMMSS = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelLonEW = new System.Windows.Forms.Label();
            this.textBoxLat1NS = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon3Minutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon3Degrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat3Minutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat3Degrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon1Seconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon1Minutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon1Degrees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat1Seconds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat1Minutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat1Degrees)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericLon3Minutes);
            this.groupBox1.Controls.Add(this.numericLon3Degrees);
            this.groupBox1.Controls.Add(this.textBoxLon3EW);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.numericLat3Minutes);
            this.groupBox1.Controls.Add(this.numericLat3Degrees);
            this.groupBox1.Controls.Add(this.textBoxLat3NS);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.radioButtonFormatDDMM);
            this.groupBox1.Controls.Add(this.numericLon2);
            this.groupBox1.Controls.Add(this.numericLat2);
            this.groupBox1.Controls.Add(this.numericLon1Seconds);
            this.groupBox1.Controls.Add(this.numericLon1Minutes);
            this.groupBox1.Controls.Add(this.numericLon1Degrees);
            this.groupBox1.Controls.Add(this.textBoxLon1EW);
            this.groupBox1.Controls.Add(this.numericLat1Seconds);
            this.groupBox1.Controls.Add(this.numericLat1Minutes);
            this.groupBox1.Controls.Add(this.numericLat1Degrees);
            this.groupBox1.Controls.Add(this.radioButtonFormatDDDD);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.radioButtonFormatDDMMSS);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.labelLonEW);
            this.groupBox1.Controls.Add(this.textBoxLat1NS);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(479, 283);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Coordinates";
            // 
            // numericLon3Minutes
            // 
            this.numericLon3Minutes.DecimalPlaces = 3;
            this.numericLon3Minutes.Location = new System.Drawing.Point(329, 65);
            this.numericLon3Minutes.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericLon3Minutes.Name = "numericLon3Minutes";
            this.numericLon3Minutes.Size = new System.Drawing.Size(90, 20);
            this.numericLon3Minutes.TabIndex = 61;
            this.numericLon3Minutes.Validated += new System.EventHandler(this.OnValidation);
            // 
            // numericLon3Degrees
            // 
            this.numericLon3Degrees.Location = new System.Drawing.Point(273, 64);
            this.numericLon3Degrees.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericLon3Degrees.Name = "numericLon3Degrees";
            this.numericLon3Degrees.Size = new System.Drawing.Size(50, 20);
            this.numericLon3Degrees.TabIndex = 59;
            this.numericLon3Degrees.Validated += new System.EventHandler(this.OnValidation);
            // 
            // textBoxLon3EW
            // 
            this.textBoxLon3EW.Location = new System.Drawing.Point(243, 64);
            this.textBoxLon3EW.Name = "textBoxLon3EW";
            this.textBoxLon3EW.Size = new System.Drawing.Size(24, 20);
            this.textBoxLon3EW.TabIndex = 57;
            this.textBoxLon3EW.Validated += new System.EventHandler(this.OnValidation);
            this.textBoxLon3EW.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidatingEW);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(326, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 60;
            this.label11.Text = "MM";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(268, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(23, 13);
            this.label13.TabIndex = 58;
            this.label13.Text = "DD";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(240, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 56;
            this.label14.Text = "E/W";
            // 
            // numericLat3Minutes
            // 
            this.numericLat3Minutes.DecimalPlaces = 3;
            this.numericLat3Minutes.Location = new System.Drawing.Point(102, 63);
            this.numericLat3Minutes.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericLat3Minutes.Name = "numericLat3Minutes";
            this.numericLat3Minutes.Size = new System.Drawing.Size(94, 20);
            this.numericLat3Minutes.TabIndex = 55;
            this.numericLat3Minutes.Validated += new System.EventHandler(this.OnValidation);
            // 
            // numericLat3Degrees
            // 
            this.numericLat3Degrees.Location = new System.Drawing.Point(55, 63);
            this.numericLat3Degrees.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericLat3Degrees.Name = "numericLat3Degrees";
            this.numericLat3Degrees.Size = new System.Drawing.Size(41, 20);
            this.numericLat3Degrees.TabIndex = 53;
            this.numericLat3Degrees.Validated += new System.EventHandler(this.OnValidation);
            // 
            // textBoxLat3NS
            // 
            this.textBoxLat3NS.Location = new System.Drawing.Point(29, 63);
            this.textBoxLat3NS.Name = "textBoxLat3NS";
            this.textBoxLat3NS.Size = new System.Drawing.Size(24, 20);
            this.textBoxLat3NS.TabIndex = 51;
            this.textBoxLat3NS.Validated += new System.EventHandler(this.OnValidation);
            this.textBoxLat3NS.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidatingNS);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(99, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 54;
            this.label8.Text = "MM";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(52, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 52;
            this.label9.Text = "DD";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 50;
            this.label10.Text = "N/S";
            // 
            // radioButtonFormatDDMM
            // 
            this.radioButtonFormatDDMM.AutoSize = true;
            this.radioButtonFormatDDMM.Checked = true;
            this.radioButtonFormatDDMM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonFormatDDMM.Location = new System.Drawing.Point(6, 19);
            this.radioButtonFormatDDMM.Name = "radioButtonFormatDDMM";
            this.radioButtonFormatDDMM.Size = new System.Drawing.Size(200, 24);
            this.radioButtonFormatDDMM.TabIndex = 49;
            this.radioButtonFormatDDMM.TabStop = true;
            this.radioButtonFormatDDMM.Text = "Format: DD MM.MMM";
            this.radioButtonFormatDDMM.UseVisualStyleBackColor = true;
            this.radioButtonFormatDDMM.CheckedChanged += new System.EventHandler(this.OnFormatChange);
            // 
            // numericLon2
            // 
            this.numericLon2.DecimalPlaces = 7;
            this.numericLon2.Enabled = false;
            this.numericLon2.Location = new System.Drawing.Point(326, 137);
            this.numericLon2.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericLon2.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericLon2.Name = "numericLon2";
            this.numericLon2.Size = new System.Drawing.Size(112, 20);
            this.numericLon2.TabIndex = 48;
            this.numericLon2.Validated += new System.EventHandler(this.OnValidation);
            // 
            // numericLat2
            // 
            this.numericLat2.DecimalPlaces = 7;
            this.numericLat2.Enabled = false;
            this.numericLat2.Location = new System.Drawing.Point(100, 137);
            this.numericLat2.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericLat2.Minimum = new decimal(new int[] {
            90,
            0,
            0,
            -2147483648});
            this.numericLat2.Name = "numericLat2";
            this.numericLat2.Size = new System.Drawing.Size(124, 20);
            this.numericLat2.TabIndex = 46;
            this.numericLat2.Validated += new System.EventHandler(this.OnValidation);
            // 
            // numericLon1Seconds
            // 
            this.numericLon1Seconds.DecimalPlaces = 3;
            this.numericLon1Seconds.Enabled = false;
            this.numericLon1Seconds.Location = new System.Drawing.Point(377, 232);
            this.numericLon1Seconds.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericLon1Seconds.Name = "numericLon1Seconds";
            this.numericLon1Seconds.Size = new System.Drawing.Size(81, 20);
            this.numericLon1Seconds.TabIndex = 43;
            this.numericLon1Seconds.Validated += new System.EventHandler(this.OnValidation);
            // 
            // numericLon1Minutes
            // 
            this.numericLon1Minutes.Enabled = false;
            this.numericLon1Minutes.Location = new System.Drawing.Point(329, 233);
            this.numericLon1Minutes.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericLon1Minutes.Name = "numericLon1Minutes";
            this.numericLon1Minutes.Size = new System.Drawing.Size(41, 20);
            this.numericLon1Minutes.TabIndex = 41;
            this.numericLon1Minutes.Validated += new System.EventHandler(this.OnValidation);
            // 
            // numericLon1Degrees
            // 
            this.numericLon1Degrees.Enabled = false;
            this.numericLon1Degrees.Location = new System.Drawing.Point(273, 232);
            this.numericLon1Degrees.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericLon1Degrees.Name = "numericLon1Degrees";
            this.numericLon1Degrees.Size = new System.Drawing.Size(50, 20);
            this.numericLon1Degrees.TabIndex = 39;
            this.numericLon1Degrees.Validated += new System.EventHandler(this.OnValidation);
            // 
            // textBoxLon1EW
            // 
            this.textBoxLon1EW.Enabled = false;
            this.textBoxLon1EW.Location = new System.Drawing.Point(243, 232);
            this.textBoxLon1EW.Name = "textBoxLon1EW";
            this.textBoxLon1EW.Size = new System.Drawing.Size(24, 20);
            this.textBoxLon1EW.TabIndex = 37;
            this.textBoxLon1EW.Validated += new System.EventHandler(this.OnValidation);
            this.textBoxLon1EW.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidatingEW);
            // 
            // numericLat1Seconds
            // 
            this.numericLat1Seconds.DecimalPlaces = 3;
            this.numericLat1Seconds.Enabled = false;
            this.numericLat1Seconds.Location = new System.Drawing.Point(149, 231);
            this.numericLat1Seconds.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericLat1Seconds.Name = "numericLat1Seconds";
            this.numericLat1Seconds.Size = new System.Drawing.Size(81, 20);
            this.numericLat1Seconds.TabIndex = 35;
            this.numericLat1Seconds.Validated += new System.EventHandler(this.OnValidation);
            // 
            // numericLat1Minutes
            // 
            this.numericLat1Minutes.Enabled = false;
            this.numericLat1Minutes.Location = new System.Drawing.Point(102, 231);
            this.numericLat1Minutes.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.numericLat1Minutes.Name = "numericLat1Minutes";
            this.numericLat1Minutes.Size = new System.Drawing.Size(41, 20);
            this.numericLat1Minutes.TabIndex = 33;
            this.numericLat1Minutes.Validated += new System.EventHandler(this.OnValidation);
            // 
            // numericLat1Degrees
            // 
            this.numericLat1Degrees.Enabled = false;
            this.numericLat1Degrees.Location = new System.Drawing.Point(55, 231);
            this.numericLat1Degrees.Maximum = new decimal(new int[] {
            90,
            0,
            0,
            0});
            this.numericLat1Degrees.Name = "numericLat1Degrees";
            this.numericLat1Degrees.Size = new System.Drawing.Size(41, 20);
            this.numericLat1Degrees.TabIndex = 31;
            this.numericLat1Degrees.Validated += new System.EventHandler(this.OnValidation);
            // 
            // radioButtonFormatDDDD
            // 
            this.radioButtonFormatDDDD.AutoSize = true;
            this.radioButtonFormatDDDD.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonFormatDDDD.Location = new System.Drawing.Point(6, 101);
            this.radioButtonFormatDDDD.Name = "radioButtonFormatDDDD";
            this.radioButtonFormatDDDD.Size = new System.Drawing.Size(190, 24);
            this.radioButtonFormatDDDD.TabIndex = 44;
            this.radioButtonFormatDDDD.Text = "Format: DD.DDDDD";
            this.radioButtonFormatDDDD.UseVisualStyleBackColor = true;
            this.radioButtonFormatDDDD.CheckedChanged += new System.EventHandler(this.OnFormatChange);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(257, 139);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 47;
            this.label12.Text = "Longitude";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(41, 139);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "Latitude";
            // 
            // radioButtonFormatDDMMSS
            // 
            this.radioButtonFormatDDMMSS.AutoSize = true;
            this.radioButtonFormatDDMMSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonFormatDDMMSS.Location = new System.Drawing.Point(6, 174);
            this.radioButtonFormatDDMMSS.Name = "radioButtonFormatDDMMSS";
            this.radioButtonFormatDDMMSS.Size = new System.Drawing.Size(223, 24);
            this.radioButtonFormatDDMMSS.TabIndex = 25;
            this.radioButtonFormatDDMMSS.Text = "Format: DD MM SS.SSS";
            this.radioButtonFormatDDMMSS.UseVisualStyleBackColor = true;
            this.radioButtonFormatDDMMSS.CheckedChanged += new System.EventHandler(this.OnFormatChange);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(374, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "SS.SSS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(326, 215);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "MM";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(268, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "DD";
            // 
            // labelLonEW
            // 
            this.labelLonEW.AutoSize = true;
            this.labelLonEW.Location = new System.Drawing.Point(240, 215);
            this.labelLonEW.Name = "labelLonEW";
            this.labelLonEW.Size = new System.Drawing.Size(30, 13);
            this.labelLonEW.TabIndex = 36;
            this.labelLonEW.Text = "E/W";
            // 
            // textBoxLat1NS
            // 
            this.textBoxLat1NS.Enabled = false;
            this.textBoxLat1NS.Location = new System.Drawing.Point(29, 231);
            this.textBoxLat1NS.Name = "textBoxLat1NS";
            this.textBoxLat1NS.Size = new System.Drawing.Size(24, 20);
            this.textBoxLat1NS.TabIndex = 29;
            this.textBoxLat1NS.Validated += new System.EventHandler(this.OnValidation);
            this.textBoxLat1NS.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidatingNS);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(147, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "SS.SSS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(99, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "MM";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "DD";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "N/S";
            // 
            // LatLonSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "LatLonSelector";
            this.Size = new System.Drawing.Size(479, 283);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon3Minutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon3Degrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat3Minutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat3Degrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon1Seconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon1Minutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLon1Degrees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat1Seconds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat1Minutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLat1Degrees)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericLon2;
        private System.Windows.Forms.NumericUpDown numericLat2;
        private System.Windows.Forms.NumericUpDown numericLon1Seconds;
        private System.Windows.Forms.NumericUpDown numericLon1Minutes;
        private System.Windows.Forms.NumericUpDown numericLon1Degrees;
        private System.Windows.Forms.TextBox textBoxLon1EW;
        private System.Windows.Forms.NumericUpDown numericLat1Seconds;
        private System.Windows.Forms.NumericUpDown numericLat1Minutes;
        private System.Windows.Forms.NumericUpDown numericLat1Degrees;
        private System.Windows.Forms.RadioButton radioButtonFormatDDDD;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RadioButton radioButtonFormatDDMMSS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelLonEW;
        private System.Windows.Forms.TextBox textBoxLat1NS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericLon3Minutes;
        private System.Windows.Forms.NumericUpDown numericLon3Degrees;
        private System.Windows.Forms.TextBox textBoxLon3EW;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericLat3Minutes;
        private System.Windows.Forms.NumericUpDown numericLat3Degrees;
        private System.Windows.Forms.TextBox textBoxLat3NS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton radioButtonFormatDDMM;
    }
}
