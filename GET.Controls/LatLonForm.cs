using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GET.Core;

namespace GET.Controls
{
    public partial class LatLonForm : Form
    {
        public LatLon Coordinates
        {
            get { return this.latLonSelector1.Value; }
        }

        public LatLonForm(LatLon coords)
        {
            InitializeComponent();
            this.latLonSelector1.Value = coords;
        }
    }
}