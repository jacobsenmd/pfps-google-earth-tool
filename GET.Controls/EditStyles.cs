using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using GET.Core;
using GET.Controls.Icons;

namespace GET.Controls
{
    public partial class EditStyles : Form
    {
        Style editStyle = null;        

        public EditStyles(Style style, bool DisableNameChange)
        {            
            InitializeComponent();
            editStyle = style;

            // error check
            if (editStyle == null)
                return;

            // with this option toggled, don't allow name changes
            if (DisableNameChange == true)
                this.textBoxIdentifier.Enabled = false;

            UpdateFormValues();
        }

        private void UpdateFormValues()
        {
            this.textBoxIdentifier.Text = editStyle.Name;

            this.colorFill.BackColor = editStyle.FillColor;
            this.colorIcon.BackColor = editStyle.IconColor;
            this.colorLabel.BackColor = editStyle.LabelColor;
            this.colorLine.BackColor = editStyle.LineColor;

            this.numericFillOpacity.Value = (decimal)editStyle.FillOpacity;
            this.numericIconOpacity.Value = (decimal)editStyle.IconOpacity;
            this.numericLabelOpacity.Value = (decimal)editStyle.LabelOpacity;

            this.numericWidth.Value = (decimal)editStyle.LineWidth;
            this.numericIconScale.Value = (decimal)editStyle.IconScale;
            this.numericLabelScale.Value = (decimal)editStyle.LabelScale;

            this.textBoxIconPath.Text = editStyle.IconPath;
            this.checkBoxExtrude.Checked = editStyle.Extrude;

            OnValidateIconPath(null, null);
        }

        private void OnValidateProperty(object sender, EventArgs e)
        {
            editStyle.Name = this.textBoxIdentifier.Text;

            editStyle.FillColor = this.colorFill.BackColor;
            editStyle.IconColor = this.colorIcon.BackColor;
            editStyle.LineColor = this.colorLine.BackColor;
            editStyle.LabelColor = this.colorLabel.BackColor;

            editStyle.FillOpacity = Convert.ToInt32(this.numericFillOpacity.Value);
            editStyle.IconOpacity = Convert.ToInt32(this.numericIconOpacity.Value);
            editStyle.LabelOpacity = Convert.ToInt32(this.numericLabelOpacity.Value);
            
            editStyle.LineWidth = Convert.ToDouble(this.numericWidth.Value);
            editStyle.IconScale = Convert.ToDouble(this.numericIconScale.Value);
            editStyle.LabelScale = Convert.ToDouble(this.numericLabelScale.Value);
            
            editStyle.Extrude = this.checkBoxExtrude.Checked;
        }

        private void OnClickLineColor(object sender, EventArgs e)
        {
            colorDialog1.Color = this.colorLine.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                this.colorLine.BackColor = colorDialog1.Color;
                this.ValidateChildren();
            }
        }

        private void OnClickFillColor(object sender, EventArgs e)
        {
            colorDialog1.Color = this.colorFill.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                this.colorFill.BackColor = colorDialog1.Color;
                this.ValidateChildren();
            }
        }

        private void OnClickLabelColor(object sender, EventArgs e)
        {
            colorDialog1.Color = this.colorLabel.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                this.colorLabel.BackColor = colorDialog1.Color;
                this.ValidateChildren();
            }
        }

        private void OnClickIconColor(object sender, EventArgs e)
        {
            colorDialog1.Color = this.colorIcon.BackColor;
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                this.colorIcon.BackColor = colorDialog1.Color;
                this.ValidateChildren();
            }
        }

        private void OnClickHelp(object sender, EventArgs e)
        {
            Help.ShowHelp(this, "Help.chm", "styles.htm");
        }

        private void OnClickIconLibraries(object sender, EventArgs e)
        {
            IconSelectionForm frm = new IconSelectionForm();            
            if (frm.ShowDialog() == DialogResult.OK)
            {
                textBoxIconPath.Text = frm.IconPath;
                OnValidateIconPath(sender, e);
            }
        }

        private void OnClickIconBrowse(object sender, EventArgs e)
        {
            openFileDialogIcons.ShowDialog();
            textBoxIconPath.Text = openFileDialogIcons.FileName;
            OnValidateIconPath(sender, e);
        }

        private void OnValidateIconPath(object sender, EventArgs e)
        {
            editStyle.IconPath = textBoxIconPath.Text;
            // Try pictureBox first.  If it won't load (i.e. it's a webpage)
            // try using the webBrowser
            try                
            {
                pictureBox1.BackgroundImage = Image.FromFile(editStyle.IconPath);
            }
            catch
            {
                // If an exception is thrown it means the pictureBox can't load the image.
                // Use the webBrowser instead
                pictureBox1.Visible = false;
                webBrowserIcon.Visible = true;
                webBrowserIcon.Navigate(editStyle.IconPath);
                return;
            }
            // We get here if the pictureBox succeeded.
            webBrowserIcon.Visible = false;
            if (pictureBox1.BackgroundImage.Width > pictureBox1.Width ||
                pictureBox1.BackgroundImage.Height > pictureBox1.Height)
                pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
            else
                pictureBox1.BackgroundImageLayout = ImageLayout.Center;
            pictureBox1.Visible = true;            
        }
   }
}