using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GET.Core;
using GET.Core.Objects;

namespace GET.Controls
{
    public delegate void ObjectTreeControlEvent(GETObject obj);

    public partial class ObjectTreeControl : UserControl
    {
        #region Private Member Data
        private TreeNode selectedNode;

        private TreeNode projectNode = new TreeNode("Google Earth Tool Project");
        private TreeNode rootNode = new TreeNode("KML File Output");
        private TreeNode styleNode = new TreeNode("Styles");
        #endregion

        #region Public Properties
        public Project currentProject;
        public TreeNode SelectedNode
        {
            get { return selectedNode; }
            set { selectedNode = value; }
        }
        public GETObject SelectedObject
        {
            get
            {
                selectedNode = objectTree.SelectedNode;
                if (selectedNode == null)
                    return null;
                if (selectedNode.Tag == null)
                    return null;
                if (selectedNode.Tag is GETObject)
                    return (GETObject)selectedNode.Tag;
                else
                    return null;
            }
            set
            {
                selectedNode = objectTree.SelectedNode;
                if (selectedNode != null)
                {
                    selectedNode.Tag = value;
                    selectedNode.Text = ((GETObject)selectedNode.Tag).Name;
                }
            }
        }
        #endregion

        #region Event Handlers
        public event ObjectTreeControlEvent OnEditProperties;        
        #endregion

        #region Methods
        public ObjectTreeControl()
        {
            InitializeComponent();
        }

        public void Initialize(Project project)
        {
            projectNode.Nodes.Clear();
            rootNode.Nodes.Clear();
            styleNode.Nodes.Clear();

            currentProject = project;
            projectNode.ImageIndex = 7;
            projectNode.Nodes.Add(styleNode);
            projectNode.Nodes.Add(rootNode);
            projectNode.Tag = "Project";

            styleNode.ImageIndex = 1;
            styleNode.Tag = "Style";            

            rootNode.ImageIndex = 1;
            rootNode.Tag = currentProject.RootObject;

            RefreshTreeView();
        }      

        public void CreateAirspaceRegion(AirspaceType type)
        {
            // *** This function is reusable code to create different kinds
            // of airspaces.  The handlers for each "Create New..." toolbar
            // buttons call this function, passing the appropriate airspace
            // type as an argument.

            // Obtain information about the parent object
            TreeNode parentNode = GetAddNode();
            GETObject parentObject = (GETObject)parentNode.Tag;

            // Create a new region object
            AirspaceRegion newRegion = new AirspaceRegion();
            newRegion.Name = "New Airspace Region";
            newRegion.AirspaceRegionType = type;
            newRegion.Parent = parentObject;
            parentObject.Children.Add(newRegion);

            // Add this object to the TreeView control

            TreeNode newNode = new TreeNode("New Airspace Region");
            newNode.Tag = newRegion;
            /*if (type == AirspaceType.Circle)
                newNode.ImageIndex = 4;
            else if (type == AirspaceType.Rectangle)
                newNode.ImageIndex = 5;
            else if (type == AirspaceType.Polygonal)
                newNode.ImageIndex = 6;
            parentNode.Nodes.Add(newNode);*/

            // Expand the branch to make this object visible
            // and then select the new object.
            parentNode.Expand();
            objectTree.SelectedNode = newNode;         
        }

        public void CreateFolder()
        {
            // Obtain information about the parent object
            TreeNode parentNode = GetAddNode();
            GETObject parentObject = (GETObject)parentNode.Tag;

            Folder newFolder = new Folder();
            newFolder.Name = "New Folder";

            TreeNode newNode = new TreeNode();
            newNode.Text = "New Folder";
            newNode.ImageIndex = 1;
            newNode.Tag = newFolder;

            parentNode.Nodes.Add(newNode);
            parentObject.Children.Add(newFolder);

            parentNode.Expand();
            objectTree.SelectedNode = newNode;        
        }

        public void DeleteHighlightedObject()
        {
            // Get the currently selected node object
            TreeNode currentNode = this.objectTree.SelectedNode;

            // Error check... is an object selected?  If not, return
            if (currentNode == null)
                return;
            if (currentNode.Tag == null)
                return;
            if (currentNode.Tag == currentProject.RootObject)
                return;
            if(currentNode.Text == "Styles" || currentNode.Text == "Google Earth Tool Project")
                return;

            // Verify we really want to delete this object
            // We exclude styles, because a different dialog box will be used later
            if (currentNode.Tag is Style)
            {
            }
            else if (currentNode.Tag is Folder)
            {
                if (MessageBox.Show("Are you sure you wish to delete this folder?  All objects in the folder will be deleted.", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;
            }
            else if (MessageBox.Show("Are you sure you wish to delete this object?", "Confirm Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            // Remove the current object from the list            
            if (currentNode.Tag is Style)
            {
                /*
                if (MessageBox.Show("Are you sure you want to delete this style?  All objects using this style will revert back to defaults.", "Delete style?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return;
                Style currentStyle = (Style)currentNode.Tag;
                foreach (GeRoute r in currentProject.Routes)
                {
                    if (r.StyleOptions == currentStyle)
                        r.StyleOptions = new GeStyle();
                }
                foreach (Placemark p in currentProject.Placemarks)
                {
                    if (p.StyleOptions == currentStyle)
                        p.StyleOptions = new GeStyle();
                }
                foreach (AirspaceRegion ar in currentProject.Airspaces)
                {
                    if (ar.StyleOptions == currentStyle)
                        ar.StyleOptions = new GeStyle();
                }
                styleNode.Nodes.Remove(currentNode);
                currentProject.Styles.Remove(currentStyle);
                 * */
                // REWRITE CODE BLOCK TO RESET ALL OBJECTS WITH THIS STYLE
                // TO CUSTOM.  WILL NEED TO BE A RECURSIVE FUNCTION.
            }
            else
            {
                // this code applies to folders, routes, placemarks, regions, etc.
                //KMLObject ParentObject = ((KMLObject)currentNode.Tag).Parent;
                //ParentObject.Children.Remove((KMLObject)currentNode.Tag);
                TreeNode parentNode = currentNode.Parent;
                ((GETObject)parentNode.Tag).Children.Remove((GETObject)currentNode.Tag);
                currentNode.Remove();
            }
        }

        public void CreateStyle()
        {
            // Create a new placemark object
            Style newStyle = new Style();
            newStyle.Name = "New Style";
            currentProject.Styles.Add(newStyle);

            // Add this object to the TreeView control
            TreeNode newNode = new TreeNode("New Style");
            newNode.Tag = newStyle;
            styleNode.Nodes.Add(newNode);

            // Expand the branch to make this object visible
            // and then select the new object.
            styleNode.Expand();
            objectTree.SelectedNode = newNode;
        }

        public void CreateWaypoint()
        {
            // Obtain information about the parent object
            TreeNode parentNode = GetAddNode();
            GETObject parentObject = (GETObject)parentNode.Tag;

            // Create a new placemark object and add it to
            // the kmlFile object
            Waypoint newWaypoint = new Waypoint();
            newWaypoint.Name = "New Waypoint";
            newWaypoint.Parent = parentObject;
            parentObject.Children.Add(newWaypoint);

            // Add this object to the TreeView control
            TreeNode newNode = new TreeNode("New Waypoint");
            newNode.Tag = newWaypoint;
            newNode.ImageIndex = 3;
            parentNode.Nodes.Add(newNode);

            // Expand the branch to make this object visible
            // and then select the new object.
            parentNode.Expand();
            objectTree.SelectedNode = newNode;
        }

        public void AddObjectToTree(GETObject obj, TreeNode parentNode)
        {
            // This adds a GET object and all its children to the treeViewNode.
            // The function is recursive... it calls itself for each child
            
            // Create a new node and assign the object as its tag
            TreeNode newNode = new TreeNode();
            newNode.Name = obj.Name;
            newNode.Tag = obj;
            
            // Figure out which image we should display for this node
            if (obj is Folder)
                newNode.ImageIndex = 1;
            else if (obj is Route)
                newNode.ImageIndex = 2;
            else if (obj is Waypoint)
                newNode.ImageIndex = 3;
            else if (obj is AirspaceRegion)
            {
                if (((AirspaceRegion)obj).AirspaceRegionType == AirspaceType.Circle)
                    newNode.ImageIndex = 4;
                else if (((AirspaceRegion)obj).AirspaceRegionType == AirspaceType.Rectangle)
                    newNode.ImageIndex = 5;
                else
                    newNode.ImageIndex = 6;
            }

            // Add this node to the appropriate parent
            parentNode.Nodes.Add(newNode);

            // Now the tricky part... if our new object has any children, we
            // need to make sure they get added to the tree as well.  By calling
            // this function recursively, we can ensure that happens.
            foreach (GETObject childobj in obj.Children)
                AddObjectToTree(childobj, newNode);
        }

        public TreeNode GetAddNode()
        {
            // This returns a tree node where a new object should be
            // placed.  It obeys the following logic:
            // 1. If no node is selected, use the root node
            // 2. If an invalid node is selected, use the root node
            // 3. If a folder is selected, use that folder
            // 4. In all other cases, an object is selectd.  Use that object's
            //    parent folder.
            
            // start by setting the node to the current selection
            selectedNode = objectTree.SelectedNode;
            if (selectedNode == null)
                return rootNode;
            else if (selectedNode.Tag == null || selectedNode.Tag is Style || selectedNode.Tag is string)
                return rootNode;
            else if (selectedNode.Tag is Folder)
                return selectedNode;
            else
                return selectedNode.Parent;
        }

        public void RefreshTreeView()
        {
            // Redraws the entire tree
            objectTree.Nodes.Clear();
            styleNode.Nodes.Clear();
            rootNode.Nodes.Clear();

            // Set up the root nodes
            objectTree.Nodes.Add(projectNode);

            // First, create a child of the root node for styles
            // and populate with all the styles
            foreach (Style s in currentProject.Styles)
            {
                TreeNode newNode = new TreeNode(s.Name);
                newNode.Tag = s;
                styleNode.Nodes.Add(newNode);
            }

            // Second, create a child of the root node for objects
            // and populate with all the objects
            foreach (GETObject obj in currentProject.RootObject.Children)
                AddObjectToTree(obj, rootNode);

            objectTree.Nodes[0].Expand();
        }

        private void OnTreeViewAfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            // This function checks to ensure that no fixed labels are
            // being changed.  If we're talking about an object, the object
            // is renamed to match the node.
            selectedNode = e.Node;
            if (e.Node.Tag is string)
            {
                // this will only be the case if the user has highlighted the
                // project or the styles tab
                e.CancelEdit = true;             
            }
            // here is if they click the KML objects tab
            else if (e.Node.Tag == currentProject.RootObject)
                e.CancelEdit = true;
            // here they're renaming a style
            else if (e.Node.Tag is Style)
            {
                Style obj = (Style)e.Node.Tag;
                obj.Name = e.Label;
            }
            // here they're renaming a KML object
            else if(e.Node.Tag is GETObject)
            {
                GETObject obj = (GETObject)e.Node.Tag;
                obj.Name = e.Label;
            }
        }

        /******************************************/
        /* Context Menu Functions                 */
        /******************************************/
        
        private void OnTreeViewClick(object sender, EventArgs e)
        {            
            // this code block is necessary for right clicks to select
            // the proper object
            Point p = Cursor.Position;
            p = objectTree.PointToClient(p);
            selectedNode = objectTree.GetNodeAt(p);            
            // this ensures the Text Pane gets redrawn to reflect the
            // new control
            Parent.Invalidate(true);
        }

        private void OnContextCut(object sender, EventArgs e)
        {            
            selectedNode = objectTree.SelectedNode;
            
            if(selectedNode == null)
                return;
            if (selectedNode.Tag == null)
                return;
            if(selectedNode.Tag is string)
                return;
            
            if(selectedNode.Tag is GETObject)
            {                
                GETObject obj = (GETObject)selectedNode.Tag;
                Clipboard.SetDataObject(obj,true);
                TreeNode parentNode = selectedNode.Parent;
                ((GETObject)parentNode.Tag).Children.Remove(obj);
                parentNode.Nodes.Remove(selectedNode);
            }
        }

        private void OnContextCopy(object sender, EventArgs e)
        {/*
            if (treeViewObjects.SelectedNode == null)
                return;
            if (treeViewObjects.SelectedNode.Tag == null)
                return;
            if (treeViewObjects.SelectedNode.Tag is string)
                return;
            if(treeViewObjects.SelectedNode.Tag is GeStyle)
            {
                GeStyle OldStyle = (GeStyle)treeViewObjects.SelectedNode.Tag;
                GeStyle NewStyle = new GeStyle();
                NewStyle.CopyStyle(OldStyle);
                Clipboard.SetDataObject(NewStyle, true);
            }
            if (treeViewObjects.SelectedNode.Tag is KMLObject)
            {
                TreeNode tn = treeViewObjects.SelectedNode;
                KMLObject obj = (KMLObject)treeViewObjects.SelectedNode.Tag;
                // TODO: create new object
                //Clipboard.SetDataObject(obj, true);                
            }
          */
        }


        private void OnContextDelete(object sender, EventArgs e)
        {
            DeleteHighlightedObject();
        }

        private void OnContextPaste(object sender, EventArgs e)
        {
        }

        private void OnContextRename(object sender, EventArgs e)
        {
            selectedNode = objectTree.SelectedNode;
            selectedNode.BeginEdit();
        }

        #endregion

        private void OnContextProperties(object sender, EventArgs e)
        {
            selectedNode = objectTree.SelectedNode;
            if (selectedNode == null)
                return;
            OnEditProperties((GETObject)objectTree.SelectedNode.Tag);
        }

    }
}
