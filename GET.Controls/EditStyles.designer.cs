namespace GET.Controls
{
    partial class EditStyles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonIconLibraries = new System.Windows.Forms.Button();
            this.webBrowserIcon = new System.Windows.Forms.WebBrowser();
            this.labelIconPath = new System.Windows.Forms.Label();
            this.textBoxIconPath = new System.Windows.Forms.TextBox();
            this.numericIconScale = new System.Windows.Forms.NumericUpDown();
            this.numericLabelScale = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonIconBrowse = new System.Windows.Forms.Button();
            this.labelLabelScale = new System.Windows.Forms.Label();
            this.numericIconOpacity = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericLabelOpacity = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.colorIcon = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.colorLabel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxExtrude = new System.Windows.Forms.CheckBox();
            this.numericWidth = new System.Windows.Forms.NumericUpDown();
            this.labelWidth = new System.Windows.Forms.Label();
            this.numericFillOpacity = new System.Windows.Forms.NumericUpDown();
            this.labelOpacity = new System.Windows.Forms.Label();
            this.colorFill = new System.Windows.Forms.Panel();
            this.labelPathFillColor = new System.Windows.Forms.Label();
            this.colorLine = new System.Windows.Forms.Panel();
            this.labelPathLineColor = new System.Windows.Forms.Label();
            this.textBoxIdentifier = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.openFileDialogIcons = new System.Windows.Forms.OpenFileDialog();
            this.imageListActive = new System.Windows.Forms.ImageList(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIconScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLabelScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIconOpacity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLabelOpacity)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFillOpacity)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.buttonIconLibraries);
            this.groupBox1.Controls.Add(this.webBrowserIcon);
            this.groupBox1.Controls.Add(this.labelIconPath);
            this.groupBox1.Controls.Add(this.textBoxIconPath);
            this.groupBox1.Controls.Add(this.numericIconScale);
            this.groupBox1.Controls.Add(this.numericLabelScale);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.buttonIconBrowse);
            this.groupBox1.Controls.Add(this.labelLabelScale);
            this.groupBox1.Controls.Add(this.numericIconOpacity);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.numericLabelOpacity);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.colorIcon);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.colorLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 195);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Waypoint Style";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(200, 125);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // buttonIconLibraries
            // 
            this.buttonIconLibraries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonIconLibraries.Location = new System.Drawing.Point(84, 167);
            this.buttonIconLibraries.Name = "buttonIconLibraries";
            this.buttonIconLibraries.Size = new System.Drawing.Size(110, 22);
            this.buttonIconLibraries.TabIndex = 20;
            this.buttonIconLibraries.Text = "&Icon Libraries";
            this.buttonIconLibraries.UseVisualStyleBackColor = true;
            this.buttonIconLibraries.Click += new System.EventHandler(this.OnClickIconLibraries);
            // 
            // webBrowserIcon
            // 
            this.webBrowserIcon.Location = new System.Drawing.Point(200, 125);
            this.webBrowserIcon.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserIcon.Name = "webBrowserIcon";
            this.webBrowserIcon.ScrollBarsEnabled = false;
            this.webBrowserIcon.Size = new System.Drawing.Size(64, 64);
            this.webBrowserIcon.TabIndex = 19;
            // 
            // labelIconPath
            // 
            this.labelIconPath.AutoSize = true;
            this.labelIconPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIconPath.Location = new System.Drawing.Point(6, 125);
            this.labelIconPath.Name = "labelIconPath";
            this.labelIconPath.Size = new System.Drawing.Size(168, 13);
            this.labelIconPath.TabIndex = 15;
            this.labelIconPath.Text = "Icon Path (Local File or Web Link)";
            // 
            // textBoxIconPath
            // 
            this.textBoxIconPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxIconPath.Location = new System.Drawing.Point(7, 141);
            this.textBoxIconPath.Name = "textBoxIconPath";
            this.textBoxIconPath.Size = new System.Drawing.Size(187, 20);
            this.textBoxIconPath.TabIndex = 16;
            this.textBoxIconPath.Validated += new System.EventHandler(this.OnValidateIconPath);
            // 
            // numericIconScale
            // 
            this.numericIconScale.DecimalPlaces = 1;
            this.numericIconScale.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericIconScale.Location = new System.Drawing.Point(214, 75);
            this.numericIconScale.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericIconScale.Name = "numericIconScale";
            this.numericIconScale.Size = new System.Drawing.Size(50, 20);
            this.numericIconScale.TabIndex = 13;
            this.numericIconScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericIconScale.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // numericLabelScale
            // 
            this.numericLabelScale.DecimalPlaces = 1;
            this.numericLabelScale.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericLabelScale.Location = new System.Drawing.Point(84, 75);
            this.numericLabelScale.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericLabelScale.Name = "numericLabelScale";
            this.numericLabelScale.Size = new System.Drawing.Size(50, 20);
            this.numericLabelScale.TabIndex = 13;
            this.numericLabelScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericLabelScale.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(140, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Icon Scale:";
            // 
            // buttonIconBrowse
            // 
            this.buttonIconBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonIconBrowse.Location = new System.Drawing.Point(6, 167);
            this.buttonIconBrowse.Name = "buttonIconBrowse";
            this.buttonIconBrowse.Size = new System.Drawing.Size(75, 22);
            this.buttonIconBrowse.TabIndex = 18;
            this.buttonIconBrowse.Text = "Browse";
            this.buttonIconBrowse.UseVisualStyleBackColor = true;
            this.buttonIconBrowse.Click += new System.EventHandler(this.OnClickIconBrowse);
            // 
            // labelLabelScale
            // 
            this.labelLabelScale.AutoSize = true;
            this.labelLabelScale.Location = new System.Drawing.Point(6, 77);
            this.labelLabelScale.Name = "labelLabelScale";
            this.labelLabelScale.Size = new System.Drawing.Size(66, 13);
            this.labelLabelScale.TabIndex = 12;
            this.labelLabelScale.Text = "Label Scale:";
            // 
            // numericIconOpacity
            // 
            this.numericIconOpacity.Location = new System.Drawing.Point(214, 50);
            this.numericIconOpacity.Name = "numericIconOpacity";
            this.numericIconOpacity.Size = new System.Drawing.Size(50, 20);
            this.numericIconOpacity.TabIndex = 8;
            this.numericIconOpacity.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericIconOpacity.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(140, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Icon Opacity:";
            // 
            // numericLabelOpacity
            // 
            this.numericLabelOpacity.Location = new System.Drawing.Point(84, 50);
            this.numericLabelOpacity.Name = "numericLabelOpacity";
            this.numericLabelOpacity.Size = new System.Drawing.Size(50, 20);
            this.numericLabelOpacity.TabIndex = 8;
            this.numericLabelOpacity.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericLabelOpacity.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(140, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Icon Color:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Label Opacity:";
            // 
            // colorIcon
            // 
            this.colorIcon.BackColor = System.Drawing.Color.Red;
            this.colorIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.colorIcon.Location = new System.Drawing.Point(235, 24);
            this.colorIcon.Name = "colorIcon";
            this.colorIcon.Size = new System.Drawing.Size(29, 20);
            this.colorIcon.TabIndex = 2;
            this.colorIcon.Click += new System.EventHandler(this.OnClickIconColor);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Label Color:";
            // 
            // colorLabel
            // 
            this.colorLabel.BackColor = System.Drawing.Color.Red;
            this.colorLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.colorLabel.Location = new System.Drawing.Point(84, 24);
            this.colorLabel.Name = "colorLabel";
            this.colorLabel.Size = new System.Drawing.Size(29, 20);
            this.colorLabel.TabIndex = 2;
            this.colorLabel.Click += new System.EventHandler(this.OnClickLabelColor);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name:";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.checkBoxExtrude);
            this.groupBox3.Controls.Add(this.numericWidth);
            this.groupBox3.Controls.Add(this.labelWidth);
            this.groupBox3.Controls.Add(this.numericFillOpacity);
            this.groupBox3.Controls.Add(this.labelOpacity);
            this.groupBox3.Controls.Add(this.colorFill);
            this.groupBox3.Controls.Add(this.labelPathFillColor);
            this.groupBox3.Controls.Add(this.colorLine);
            this.groupBox3.Controls.Add(this.labelPathLineColor);
            this.groupBox3.Location = new System.Drawing.Point(12, 236);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(270, 122);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Path/Polygon Style";
            // 
            // checkBoxExtrude
            // 
            this.checkBoxExtrude.AutoSize = true;
            this.checkBoxExtrude.Location = new System.Drawing.Point(7, 30);
            this.checkBoxExtrude.Name = "checkBoxExtrude";
            this.checkBoxExtrude.Size = new System.Drawing.Size(190, 17);
            this.checkBoxExtrude.TabIndex = 10;
            this.checkBoxExtrude.Text = "Extrude from Surface to Flight Path";
            this.checkBoxExtrude.UseVisualStyleBackColor = true;
            this.checkBoxExtrude.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // numericWidth
            // 
            this.numericWidth.DecimalPlaces = 1;
            this.numericWidth.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericWidth.Location = new System.Drawing.Point(67, 90);
            this.numericWidth.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericWidth.Name = "numericWidth";
            this.numericWidth.Size = new System.Drawing.Size(50, 20);
            this.numericWidth.TabIndex = 16;
            this.numericWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericWidth.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // labelWidth
            // 
            this.labelWidth.AutoSize = true;
            this.labelWidth.Location = new System.Drawing.Point(4, 92);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(61, 13);
            this.labelWidth.TabIndex = 15;
            this.labelWidth.Text = "Line Width:";
            // 
            // numericFillOpacity
            // 
            this.numericFillOpacity.Location = new System.Drawing.Point(214, 90);
            this.numericFillOpacity.Name = "numericFillOpacity";
            this.numericFillOpacity.Size = new System.Drawing.Size(50, 20);
            this.numericFillOpacity.TabIndex = 18;
            this.numericFillOpacity.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericFillOpacity.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // labelOpacity
            // 
            this.labelOpacity.AutoSize = true;
            this.labelOpacity.Location = new System.Drawing.Point(149, 92);
            this.labelOpacity.Name = "labelOpacity";
            this.labelOpacity.Size = new System.Drawing.Size(61, 13);
            this.labelOpacity.TabIndex = 17;
            this.labelOpacity.Text = "Fill Opacity:";
            // 
            // colorFill
            // 
            this.colorFill.BackColor = System.Drawing.Color.Red;
            this.colorFill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.colorFill.Location = new System.Drawing.Point(214, 64);
            this.colorFill.Name = "colorFill";
            this.colorFill.Size = new System.Drawing.Size(29, 20);
            this.colorFill.TabIndex = 14;
            this.colorFill.Click += new System.EventHandler(this.OnClickFillColor);
            // 
            // labelPathFillColor
            // 
            this.labelPathFillColor.AutoSize = true;
            this.labelPathFillColor.Location = new System.Drawing.Point(149, 64);
            this.labelPathFillColor.Name = "labelPathFillColor";
            this.labelPathFillColor.Size = new System.Drawing.Size(49, 13);
            this.labelPathFillColor.TabIndex = 13;
            this.labelPathFillColor.Text = "Fill Color:";
            // 
            // colorLine
            // 
            this.colorLine.BackColor = System.Drawing.Color.Red;
            this.colorLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.colorLine.Location = new System.Drawing.Point(67, 64);
            this.colorLine.Name = "colorLine";
            this.colorLine.Size = new System.Drawing.Size(29, 20);
            this.colorLine.TabIndex = 12;
            this.colorLine.Click += new System.EventHandler(this.OnClickLineColor);
            // 
            // labelPathLineColor
            // 
            this.labelPathLineColor.AutoSize = true;
            this.labelPathLineColor.Location = new System.Drawing.Point(4, 64);
            this.labelPathLineColor.Name = "labelPathLineColor";
            this.labelPathLineColor.Size = new System.Drawing.Size(57, 13);
            this.labelPathLineColor.TabIndex = 11;
            this.labelPathLineColor.Text = "Line Color:";
            // 
            // textBoxIdentifier
            // 
            this.textBoxIdentifier.Location = new System.Drawing.Point(65, 8);
            this.textBoxIdentifier.Name = "textBoxIdentifier";
            this.textBoxIdentifier.Size = new System.Drawing.Size(217, 20);
            this.textBoxIdentifier.TabIndex = 16;
            this.textBoxIdentifier.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(111, 379);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 17;
            this.buttonOK.Text = "&OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\Documents and Settings\\Mark\\My Documents\\Visual Studio 2005\\Projects\\PFPS Goog" +
                "le Earth Tool\\PFPS Google Earth Tool\\Help\\Help.chm";
            // 
            // openFileDialogIcons
            // 
            this.openFileDialogIcons.Filter = "Picture or Icon Files (*.bmp,*.gif,*.ico,*.jpg,*.jpeg,*.png,*.tiff,*.tga)|*.bmp;*" +
                ".gif;*.ico;*.jpg;*.jpeg;*.png;*.tiff;*.tga";
            // 
            // imageListActive
            // 
            this.imageListActive.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListActive.ImageSize = new System.Drawing.Size(32, 32);
            this.imageListActive.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // EditStyles
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::GET.Controls.Properties.Resources.skyblue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(293, 414);
            this.ControlBox = false;
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxIdentifier);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "EditStyles";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Style Properties";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIconScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLabelScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericIconOpacity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericLabelOpacity)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericFillOpacity)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel colorLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericLabelOpacity;
        private System.Windows.Forms.NumericUpDown numericLabelScale;
        private System.Windows.Forms.Label labelLabelScale;
        private System.Windows.Forms.NumericUpDown numericIconScale;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericIconOpacity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel colorIcon;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxExtrude;
        private System.Windows.Forms.NumericUpDown numericWidth;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.NumericUpDown numericFillOpacity;
        private System.Windows.Forms.Label labelOpacity;
        private System.Windows.Forms.Panel colorFill;
        private System.Windows.Forms.Label labelPathFillColor;
        private System.Windows.Forms.Panel colorLine;
        private System.Windows.Forms.Label labelPathLineColor;
        private System.Windows.Forms.TextBox textBoxIdentifier;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.Label labelIconPath;
        private System.Windows.Forms.TextBox textBoxIconPath;
        private System.Windows.Forms.Button buttonIconBrowse;
        private System.Windows.Forms.WebBrowser webBrowserIcon;
        private System.Windows.Forms.OpenFileDialog openFileDialogIcons;
        private System.Windows.Forms.ImageList imageListActive;
        private System.Windows.Forms.Button buttonIconLibraries;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}