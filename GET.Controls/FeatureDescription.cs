using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GET.Core;
using GET.Core.Objects;

namespace GET.Controls
{
    public partial class FeatureDescription : UserControl
    {
        #region Private Member Data
        private TreeNode currentNode;        
        private Font HeaderFont = new Font("Arial", 14);
        private Font BodyFont = new Font("Arial", 10);
        private Font BoldFont = new Font("Arial", 10, FontStyle.Bold);
        private Font PointFont = new Font("MS LineDraw", 10);
        #endregion

        #region Public Properties
        public TreeNode CurrentNode
        {
            set
            {
                currentNode = value;
                GenerateText();
            }
            get { return currentNode; }
        }
        #endregion


        public FeatureDescription()
        {
            InitializeComponent();
        }

        public void GenerateText()
        {
            richTextBox1.Text = "";

            if (CurrentNode == null)
                return;

            if (CurrentNode.Tag is Project)
            {
                Project p = (Project)CurrentNode.Tag;

                richTextBox1.SelectionFont = HeaderFont;
                richTextBox1.SelectionColor = Color.Blue;
                richTextBox1.SelectedText = "\n\t" + ((Project)CurrentNode.Tag).Name + "\n\n";
                richTextBox1.SelectionColor = Color.Black;

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tSnippet:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Snippet + "\n\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tAuthor:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Author + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tOrganization: \t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Organization + "\n";


                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tE-Mail:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Email + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tPhone:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Phone + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tWebsite:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Website + "\n\n";
                
                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tClassification:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Classification + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tCreation Date:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Date.ToShortDateString() + "\n\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tDescription:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += p.Description + "\n";

                
                return;
            }
            if (CurrentNode.Text == "Styles" || CurrentNode.Text == "Objects")
                return;

            if (CurrentNode.Tag is GETObject)
                GenerateHeader();
            
            if (CurrentNode.Tag is Route)
            {
                Route r = (Route)CurrentNode.Tag;

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tAltitude:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += GenerateAltitude(r.Altitude, r.AltitudeType) + "\n\n";                

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tPoints:\n";
                foreach (Waypoint routePoint in r.Points)
                {
                    richTextBox1.SelectionFont = PointFont;
                    StringBuilder sb = new StringBuilder("\t",50);
                    sb.Append(routePoint.Name);
                    sb.Append("                              ");
                    sb.Insert(15,routePoint.Coordinates.ToString());
                    richTextBox1.SelectedText += sb.ToString() + "\n";
                }
            }

            else if (CurrentNode.Tag is Waypoint)
            {
                Waypoint p = (Waypoint)CurrentNode.Tag;

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tCoordinates:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += (p.Coordinates.ToString() + "\n");

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tAltitude:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += GenerateAltitude(p.Altitude, p.AltitudeType) + "\n";                
            }
            else if (CurrentNode.Tag is AirspaceRegion)
            {
                AirspaceRegion ar = (AirspaceRegion)CurrentNode.Tag;
                
                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tGeometry:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                if (ar.AirspaceRegionType == AirspaceType.Circle)
                    richTextBox1.SelectedText += "Circle\n";
                else if (ar.AirspaceRegionType == AirspaceType.Rectangle)
                    richTextBox1.SelectedText += "Rectangle\n";
                else if (ar.AirspaceRegionType == AirspaceType.Polygon)
                    richTextBox1.SelectedText += "Polygon\n";
                else if (ar.AirspaceRegionType == AirspaceType.Ellipse)
                    richTextBox1.SelectedText += "Ellipse\n";
                else if (ar.AirspaceRegionType == AirspaceType.Wedge)
                    richTextBox1.SelectedText += "Wedge\n";


                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tFloor Alt:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += GenerateAltitude(ar.AltitudeFloor,ar.AltitudeType) + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tCeiling Alt:\t\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += GenerateAltitude(ar.AltitudeCeiling,ar.AltitudeType) + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tGen. Placemark:\t";
                richTextBox1.SelectionFont = BodyFont;
                if(ar.GeneratePlacemark == true)
                    richTextBox1.SelectedText += "Yes\n";
                else
                    richTextBox1.SelectedText += "No\n";

                if (ar.AirspaceRegionType == AirspaceType.Circle || ar.AirspaceRegionType == AirspaceType.Wedge)
                {
                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tInner Radius:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.InnerRadius + " NM\n";

                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tOuter Radius:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.OuterRadius + " NM\n";

                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tStart Radial:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.StartRadial + " deg true\n";

                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tStop Radial:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.StopRadial + " deg true\n";

                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tResolution:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.Resolution + "\n\n";
                }

                if(ar.AirspaceRegionType == AirspaceType.Ellipse)
                {
                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tHorz Radius:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.HorizontalRadius + " NM\n";

                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tVert Radius:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.VerticalRadius + " NM\n";

                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tAngle:\t\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.Angle + " degrees\n";


                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tResolution:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.Resolution + "\n";
                }

                if (ar.AirspaceRegionType == AirspaceType.Rectangle)
                {
                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tRect Width:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.RectWidth + " NM\n";

                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tRect Height:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.RectHeight + " NM\n";

                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tAngle:\t\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.Angle + " degrees\n\n";
                }

                if (ar.AirspaceRegionType == AirspaceType.Polygon)
                {
                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\n\tPoints:\n";
                    foreach (LatLon coord in ar.PolygonPoints)
                    {
                        richTextBox1.SelectionFont = BodyFont;
                        richTextBox1.SelectedText += "\t" + coord.ToString() + "\n";
                    }

                }
                else
                {
                    richTextBox1.SelectionFont = BoldFont;
                    richTextBox1.SelectedText += "\tCenter Point:\t\t";
                    richTextBox1.SelectionFont = BodyFont;
                    richTextBox1.SelectedText += ar.CenterPoint.ToString() + "\n\n";
                }                
            }
            else if (CurrentNode.Tag is Style)
            {
                Style s = (Style)CurrentNode.Tag;
                richTextBox1.SelectionFont = HeaderFont;
                richTextBox1.SelectionColor = Color.Blue;
                richTextBox1.SelectedText += "\n\t" + s.Name + "\n\n";
                richTextBox1.SelectionColor = Color.Black;

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tLine Color:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.LineColor.Name + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tLine Width:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.LineWidth.ToString() + "\n\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tFill Color:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.FillColor.Name + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tFill Opacity:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.FillOpacity.ToString() + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tExtrude:\t";
                richTextBox1.SelectionFont = BodyFont;
                if (s.Extrude == true)
                    richTextBox1.SelectedText += "Yes\n\n";
                else
                    richTextBox1.SelectedText += "No\n\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tLabel Color:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.LabelColor.Name + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tLabel Opacity:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.LabelOpacity.ToString() + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tLabel Scale:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.LabelScale.ToString() + "\n\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tIcon Color:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.IconColor.Name + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tIcon Opacity:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.IconOpacity.ToString() + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tIcon Path:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.IconPath + "\n";

                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tIcon Scale:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += s.IconScale.ToString() + "\n\n";
            }
            else if (CurrentNode.Tag is Folder)
            {
            }
        }

        private string GenerateAltitude(int alt, AltType altType)
        {
            string s;
            if (altType == AltType.ClampToGround)
                s = "Clamp to Ground";
            else if (altType == AltType.FixedAGL)
                s = alt + " AGL";
            else
                s =  alt + " MSL";

            return s;
        }

        private void GenerateHeader()
        {
            GETObject getObject = (GETObject)CurrentNode.Tag;
            richTextBox1.SelectionFont = HeaderFont;
            richTextBox1.SelectionColor = Color.Blue;
            richTextBox1.SelectedText += "\n\t" + getObject.Name + "\n\n";
            richTextBox1.SelectionColor = Color.Black;

            richTextBox1.SelectionFont = BoldFont;
            richTextBox1.SelectedText += "\tObject Type:\t\t";
            richTextBox1.SelectionFont = BodyFont;
            string[] parts = (getObject.GetType().ToString()).Split(new char[] {'.'});            
            richTextBox1.SelectedText += parts[parts.Length - 1] + "\n";

            richTextBox1.SelectionFont = BoldFont;
            richTextBox1.SelectedText += "\tSnippet:\t\t";
            richTextBox1.SelectionFont = BodyFont;
            richTextBox1.SelectedText += getObject.Snippet + "\n";

            richTextBox1.SelectionFont = BoldFont;
            richTextBox1.SelectedText += "\tDescription:\t\t";
            richTextBox1.SelectionFont = BodyFont;
            richTextBox1.SelectedText += getObject.Description + "\n";

            richTextBox1.SelectionFont = BoldFont;
            richTextBox1.SelectedText += "\tStyle:\t\t\t";
            richTextBox1.SelectionFont = BodyFont;
            richTextBox1.SelectedText += getObject.StyleOptions + "\n";

            richTextBox1.SelectionFont = BoldFont;
            richTextBox1.SelectedText += "\tExport:\t\t\t";
            richTextBox1.SelectionFont = BodyFont;
            if (getObject.IncludeInExport == true)
                richTextBox1.SelectedText += "Yes\n";
            else
                richTextBox1.SelectedText += "No\n";

            richTextBox1.SelectionFont = BoldFont;
            richTextBox1.SelectedText += "\tShow Visible:\t\t";
            richTextBox1.SelectionFont = BodyFont;            
            if(getObject.ShowVisible == true)
                richTextBox1.SelectedText += "Yes\n";
            else
                richTextBox1.SelectedText += "No\n";

            richTextBox1.SelectionFont = BoldFont;
            richTextBox1.SelectedText += "\tUse Timestamp:\t";
            richTextBox1.SelectionFont = BodyFont;
            if (getObject.UseTimeStamp == true)
            {
                richTextBox1.SelectedText += "Yes\n";
                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tStart Time:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += "\t" + getObject.StartTime.ToLongTimeString() + "\n";
                richTextBox1.SelectionFont = BoldFont;
                richTextBox1.SelectedText += "\tEnd Time:\t";
                richTextBox1.SelectionFont = BodyFont;
                richTextBox1.SelectedText += "\t" + getObject.EndTime.ToLongTimeString() + "\n\n";
            }
            else
                richTextBox1.SelectedText += "No\n\n";
            
        }
    }
}
