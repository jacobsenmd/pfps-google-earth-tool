// CONTROL: Feature Properties

// This control is used to adjust the basic properties for any feature.  These
// properties include name, snippet, description and graphical style.
//
// Public Properties:
//    Name
//    Description
//    Snippet
//    StyleOptions
//
// Methods:
//    FeatureProperties(Project)        The constructor needs a reference to the
//                                      project so it can access the list of styles
//
// Events:
//    Validated()                       Triggered when any change is validated

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GET.Core;
using GET.Core.Objects;

namespace GET.Controls
{
    public partial class FeatureProperties : UserControl
    {        
        #region Private Member Data
        private Project project;        
        private GETObject getObjectTag;
        
        #endregion

        #region Public Properties        
        public override string Text
        {
            set { groupBox1.Text = value; }
        }
        public GETObject Value
        {
            get { return getObjectTag; }
            set
            {
                if (value != null)
                {
                    GETObject obj = value;
                    getObjectTag = obj.GetCopy();
                }
                UpdateFormValues();
            }
        }
        public Project Project
        {
            set
            {
                project = value;
                BuildComboBox();
            }
            get { return project; }
        }        
        #endregion

        #region Methods

        public FeatureProperties()
        {
            InitializeComponent();
        }
        
        private void BuildComboBox()   
        {
            if (project == null)
                return;

            // Build combo box of styles
            comboBoxStyles.Items.Clear();
            comboBoxStyles.Items.Add("<Custom>");
            foreach (Style s in project.Styles)
                comboBoxStyles.Items.Add(s);
        }

        public void UpdateFormValues()
        {
            // This function updates the controls to match the getObjectTag
            if (getObjectTag == null)
            {
                // Control enabling
                this.textBoxDescription.Enabled = false;
                this.textBoxName.Enabled = false;
                this.textBoxSnippet.Enabled = false;
                this.comboBoxStyles.Enabled = false;
                this.checkBoxIncludeInExport.Enabled = false;
                this.checkBoxShowVisible.Enabled = false;
                this.checkBoxUseTimeStamp.Enabled = false;
                this.dateTimeBegin.Enabled = false;
                this.dateTimeEnd.Enabled = false;
                // Control values
                this.textBoxDescription.Text = "";
                this.textBoxName.Text = "";
                this.textBoxSnippet.Text = "";
                this.comboBoxStyles.Text = "";
            }
            else
            {
                // Control enabling
                this.textBoxDescription.Enabled = true;
                this.textBoxName.Enabled = true;
                this.textBoxSnippet.Enabled = true;
                this.comboBoxStyles.Enabled = true;
                this.checkBoxIncludeInExport.Enabled = true;
                this.checkBoxShowVisible.Enabled = true;
                this.checkBoxUseTimeStamp.Enabled = true;
                if (getObjectTag.UseTimeStamp == true)
                {
                    this.dateTimeBegin.Enabled = true;
                    this.dateTimeEnd.Enabled = true;
                    this.dateTimeBegin.Value = getObjectTag.StartTime;
                    this.dateTimeEnd.Value = getObjectTag.EndTime;
                }                                
                else
                {
                    this.dateTimeBegin.Enabled = false;
                    this.dateTimeEnd.Enabled = false;
                }

                // Control values
                this.textBoxDescription.Text = getObjectTag.Description;
                this.textBoxName.Text = getObjectTag.Name;
                this.textBoxSnippet.Text = getObjectTag.Snippet;
                this.checkBoxIncludeInExport.Checked = getObjectTag.IncludeInExport;
                this.checkBoxShowVisible.Checked = getObjectTag.ShowVisible;
                this.checkBoxUseTimeStamp.Checked = getObjectTag.UseTimeStamp;
                if (getObjectTag.UseTimeStamp == true)
                {
                    this.dateTimeBegin.Value = getObjectTag.StartTime;
                    this.dateTimeEnd.Value = getObjectTag.EndTime;
                }

                if (getObjectTag.StyleOptions.Name == "<Custom>")
                    comboBoxStyles.SelectedIndex = 0;
                else
                {
                    //comboBoxStyles.SelectedItem = getObjectTag.StyleOptions;
                    int i = comboBoxStyles.Items.IndexOf(getObjectTag.StyleOptions);
                    comboBoxStyles.SelectedIndex = i;
                }
            }
        }

        public void UpdateObject()
        {
            if (getObjectTag is GETObject)
            {
                getObjectTag.Description = textBoxDescription.Text;
                getObjectTag.Name = textBoxName.Text;
                getObjectTag.Snippet = textBoxSnippet.Text;
                getObjectTag.IncludeInExport = checkBoxIncludeInExport.Checked;
                getObjectTag.ShowVisible = checkBoxShowVisible.Checked;
                getObjectTag.UseTimeStamp = checkBoxUseTimeStamp.Checked;
                if (getObjectTag.UseTimeStamp == true)
                {
                    getObjectTag.StartTime = dateTimeBegin.Value;
                    getObjectTag.EndTime = dateTimeEnd.Value;
                }

                if (this.comboBoxStyles.SelectedIndex == 0 && getObjectTag.StyleOptions.Name != "<Custom>")
                {
                    getObjectTag.StyleOptions = new Style();
                    getObjectTag.StyleOptions.Name = "<Custom>";
                }

                // if a custom style is already selected, leave as is
                else if (this.comboBoxStyles.SelectedIndex == 0)
                {
                }
                // For this else, we are dealing with pre-defined styles
                else
                    getObjectTag.StyleOptions = (Style)comboBoxStyles.SelectedItem;
                
                this.Validate();
            }
        }        

        private void OnStyleComboBoxChanged(object sender, EventArgs e)
        {
            if(getObjectTag == null)
                return;
            // If the user switches to a custom style from something else,
            // we create a new custom style.
            if (this.comboBoxStyles.SelectedIndex == 0 && getObjectTag.StyleOptions.Name != "<Custom>")
            {
                getObjectTag.StyleOptions = new Style();
                getObjectTag.StyleOptions.Name = "<Custom>";                
            }

            // if a custom style is already selected, leave as is
            else if (this.comboBoxStyles.SelectedIndex == 0)
            {
            }
            // For this else, we are dealing with pre-defined styles
            else                            
                getObjectTag.StyleOptions = (Style)comboBoxStyles.SelectedItem;

            this.Validate();
        }

        private void OnButtonEditStyle(object sender, EventArgs e)
        {
            if (getObjectTag.StyleOptions == null)
            {
                getObjectTag.StyleOptions = new Style();
                getObjectTag.StyleOptions.Name = "<Custom>";                
                comboBoxStyles.SelectedIndex = 0;
            }
            EditStyles editStyle = new EditStyles(getObjectTag.StyleOptions, true);            
            editStyle.ShowDialog();
        }
        
        private void OnValidateTimeStampCheckBox(object sender, EventArgs e)
        {            
            if (checkBoxUseTimeStamp.Checked == false)
            {
                dateTimeBegin.Enabled = false;
                dateTimeEnd.Enabled = false;
            }
            else
            {
                dateTimeBegin.Enabled = true;
                dateTimeEnd.Enabled = true;
            }

            this.Validate();
        }
        #endregion

        private void OnValidateProperty(object sender, EventArgs e)
        {
            UpdateObject();
            this.Validate();
        }
    }
}
