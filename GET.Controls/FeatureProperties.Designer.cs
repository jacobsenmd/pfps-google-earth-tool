namespace GET.Controls
{
    partial class FeatureProperties
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.labelTimeEnd = new System.Windows.Forms.Label();
            this.dateTimeBegin = new System.Windows.Forms.DateTimePicker();
            this.labelTimeBegin = new System.Windows.Forms.Label();
            this.checkBoxUseTimeStamp = new System.Windows.Forms.CheckBox();
            this.checkBoxShowVisible = new System.Windows.Forms.CheckBox();
            this.checkBoxIncludeInExport = new System.Windows.Forms.CheckBox();
            this.buttonEditStyle = new System.Windows.Forms.Button();
            this.comboBoxStyles = new System.Windows.Forms.ComboBox();
            this.labelStyle = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxSnippet = new System.Windows.Forms.TextBox();
            this.labelSnippet = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimeEnd);
            this.groupBox1.Controls.Add(this.labelTimeEnd);
            this.groupBox1.Controls.Add(this.dateTimeBegin);
            this.groupBox1.Controls.Add(this.labelTimeBegin);
            this.groupBox1.Controls.Add(this.checkBoxUseTimeStamp);
            this.groupBox1.Controls.Add(this.checkBoxShowVisible);
            this.groupBox1.Controls.Add(this.checkBoxIncludeInExport);
            this.groupBox1.Controls.Add(this.buttonEditStyle);
            this.groupBox1.Controls.Add(this.comboBoxStyles);
            this.groupBox1.Controls.Add(this.labelStyle);
            this.groupBox1.Controls.Add(this.textBoxDescription);
            this.groupBox1.Controls.Add(this.labelDescription);
            this.groupBox1.Controls.Add(this.textBoxSnippet);
            this.groupBox1.Controls.Add(this.labelSnippet);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Controls.Add(this.labelName);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(321, 314);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Basic Feature Properties";
            // 
            // dateTimeEnd
            // 
            this.dateTimeEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimeEnd.CustomFormat = "MMMM dd \',\' yyyy \'     \' HH:mm:ss";
            this.dateTimeEnd.Enabled = false;
            this.dateTimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeEnd.Location = new System.Drawing.Point(93, 280);
            this.dateTimeEnd.Name = "dateTimeEnd";
            this.dateTimeEnd.Size = new System.Drawing.Size(199, 20);
            this.dateTimeEnd.TabIndex = 15;
            this.dateTimeEnd.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // labelTimeEnd
            // 
            this.labelTimeEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTimeEnd.AutoSize = true;
            this.labelTimeEnd.Location = new System.Drawing.Point(50, 284);
            this.labelTimeEnd.Name = "labelTimeEnd";
            this.labelTimeEnd.Size = new System.Drawing.Size(29, 13);
            this.labelTimeEnd.TabIndex = 14;
            this.labelTimeEnd.Text = "End:";
            // 
            // dateTimeBegin
            // 
            this.dateTimeBegin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimeBegin.CustomFormat = "MMMM dd \',\' yyyy \'     \' HH:mm:ss";
            this.dateTimeBegin.Enabled = false;
            this.dateTimeBegin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeBegin.Location = new System.Drawing.Point(93, 254);
            this.dateTimeBegin.Name = "dateTimeBegin";
            this.dateTimeBegin.Size = new System.Drawing.Size(199, 20);
            this.dateTimeBegin.TabIndex = 13;
            this.dateTimeBegin.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // labelTimeBegin
            // 
            this.labelTimeBegin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTimeBegin.AutoSize = true;
            this.labelTimeBegin.Location = new System.Drawing.Point(50, 258);
            this.labelTimeBegin.Name = "labelTimeBegin";
            this.labelTimeBegin.Size = new System.Drawing.Size(37, 13);
            this.labelTimeBegin.TabIndex = 12;
            this.labelTimeBegin.Text = "Begin:";
            // 
            // checkBoxUseTimeStamp
            // 
            this.checkBoxUseTimeStamp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxUseTimeStamp.AutoSize = true;
            this.checkBoxUseTimeStamp.Location = new System.Drawing.Point(21, 234);
            this.checkBoxUseTimeStamp.Name = "checkBoxUseTimeStamp";
            this.checkBoxUseTimeStamp.Size = new System.Drawing.Size(104, 17);
            this.checkBoxUseTimeStamp.TabIndex = 11;
            this.checkBoxUseTimeStamp.Text = "Use Time Stamp";
            this.checkBoxUseTimeStamp.UseVisualStyleBackColor = true;
            this.checkBoxUseTimeStamp.Validated += new System.EventHandler(this.OnValidateProperty);
            this.checkBoxUseTimeStamp.CheckedChanged += new System.EventHandler(this.OnValidateTimeStampCheckBox);
            // 
            // checkBoxShowVisible
            // 
            this.checkBoxShowVisible.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxShowVisible.AutoSize = true;
            this.checkBoxShowVisible.Checked = true;
            this.checkBoxShowVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxShowVisible.Location = new System.Drawing.Point(21, 211);
            this.checkBoxShowVisible.Name = "checkBoxShowVisible";
            this.checkBoxShowVisible.Size = new System.Drawing.Size(213, 17);
            this.checkBoxShowVisible.TabIndex = 10;
            this.checkBoxShowVisible.Text = "Show Visible by Default in Google Earth";
            this.checkBoxShowVisible.UseVisualStyleBackColor = true;
            this.checkBoxShowVisible.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // checkBoxIncludeInExport
            // 
            this.checkBoxIncludeInExport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxIncludeInExport.AutoSize = true;
            this.checkBoxIncludeInExport.Checked = true;
            this.checkBoxIncludeInExport.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncludeInExport.Location = new System.Drawing.Point(21, 188);
            this.checkBoxIncludeInExport.Name = "checkBoxIncludeInExport";
            this.checkBoxIncludeInExport.Size = new System.Drawing.Size(130, 17);
            this.checkBoxIncludeInExport.TabIndex = 9;
            this.checkBoxIncludeInExport.Text = "Include in KML Export";
            this.checkBoxIncludeInExport.UseVisualStyleBackColor = true;
            this.checkBoxIncludeInExport.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // buttonEditStyle
            // 
            this.buttonEditStyle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEditStyle.Location = new System.Drawing.Point(259, 145);
            this.buttonEditStyle.Name = "buttonEditStyle";
            this.buttonEditStyle.Size = new System.Drawing.Size(56, 23);
            this.buttonEditStyle.TabIndex = 8;
            this.buttonEditStyle.Text = "&Edit";
            this.buttonEditStyle.UseVisualStyleBackColor = true;
            this.buttonEditStyle.Click += new System.EventHandler(this.OnButtonEditStyle);
            // 
            // comboBoxStyles
            // 
            this.comboBoxStyles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxStyles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStyles.FormattingEnabled = true;
            this.comboBoxStyles.Location = new System.Drawing.Point(93, 147);
            this.comboBoxStyles.Name = "comboBoxStyles";
            this.comboBoxStyles.Size = new System.Drawing.Size(164, 21);
            this.comboBoxStyles.TabIndex = 7;
            this.comboBoxStyles.SelectedIndexChanged += new System.EventHandler(this.OnStyleComboBoxChanged);
            // 
            // labelStyle
            // 
            this.labelStyle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelStyle.AutoSize = true;
            this.labelStyle.Location = new System.Drawing.Point(18, 150);
            this.labelStyle.Name = "labelStyle";
            this.labelStyle.Size = new System.Drawing.Size(33, 13);
            this.labelStyle.TabIndex = 6;
            this.labelStyle.Text = "Style:";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDescription.Location = new System.Drawing.Point(93, 79);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(222, 60);
            this.textBoxDescription.TabIndex = 5;
            this.textBoxDescription.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(18, 82);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(60, 13);
            this.labelDescription.TabIndex = 4;
            this.labelDescription.Text = "Description";
            // 
            // textBoxSnippet
            // 
            this.textBoxSnippet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSnippet.Location = new System.Drawing.Point(93, 53);
            this.textBoxSnippet.Name = "textBoxSnippet";
            this.textBoxSnippet.Size = new System.Drawing.Size(222, 20);
            this.textBoxSnippet.TabIndex = 3;
            this.textBoxSnippet.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // labelSnippet
            // 
            this.labelSnippet.AutoSize = true;
            this.labelSnippet.Location = new System.Drawing.Point(18, 56);
            this.labelSnippet.Name = "labelSnippet";
            this.labelSnippet.Size = new System.Drawing.Size(43, 13);
            this.labelSnippet.TabIndex = 2;
            this.labelSnippet.Text = "Snippet";
            // 
            // textBoxName
            // 
            this.textBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxName.Location = new System.Drawing.Point(93, 27);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(222, 20);
            this.textBoxName.TabIndex = 1;
            this.textBoxName.Validated += new System.EventHandler(this.OnValidateProperty);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(18, 30);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Name";
            // 
            // FeatureProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "FeatureProperties";
            this.Size = new System.Drawing.Size(321, 314);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonEditStyle;
        private System.Windows.Forms.ComboBox comboBoxStyles;
        private System.Windows.Forms.Label labelStyle;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxSnippet;
        private System.Windows.Forms.Label labelSnippet;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.CheckBox checkBoxIncludeInExport;
        private System.Windows.Forms.CheckBox checkBoxShowVisible;
        private System.Windows.Forms.DateTimePicker dateTimeBegin;
        private System.Windows.Forms.Label labelTimeBegin;
        private System.Windows.Forms.CheckBox checkBoxUseTimeStamp;
        private System.Windows.Forms.DateTimePicker dateTimeEnd;
        private System.Windows.Forms.Label labelTimeEnd;
    }
}
