namespace GET.Controls
{
    partial class ObjectTreeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ObjectTreeControl));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.contextMenuProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuRename = new System.Windows.Forms.ToolStripMenuItem();
            this.imageListObjects = new System.Windows.Forms.ImageList(this.components);
            this.objectTree = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextMenuCopy,
            this.contextMenuPaste,
            this.contextMenuCut,
            this.toolStripMenuItem4,
            this.contextMenuProperties,
            this.contextMenuDelete,
            this.contextMenuRename});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(135, 142);
            // 
            // contextMenuCopy
            // 
            this.contextMenuCopy.Name = "contextMenuCopy";
            this.contextMenuCopy.Size = new System.Drawing.Size(134, 22);
            this.contextMenuCopy.Text = "&Copy";
            this.contextMenuCopy.Click += new System.EventHandler(this.OnContextCopy);
            // 
            // contextMenuPaste
            // 
            this.contextMenuPaste.Name = "contextMenuPaste";
            this.contextMenuPaste.Size = new System.Drawing.Size(134, 22);
            this.contextMenuPaste.Text = "&Paste";
            this.contextMenuPaste.Click += new System.EventHandler(this.OnContextPaste);
            // 
            // contextMenuCut
            // 
            this.contextMenuCut.Name = "contextMenuCut";
            this.contextMenuCut.Size = new System.Drawing.Size(134, 22);
            this.contextMenuCut.Text = "Cu&t";
            this.contextMenuCut.Click += new System.EventHandler(this.OnContextCut);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(131, 6);
            // 
            // contextMenuProperties
            // 
            this.contextMenuProperties.Name = "contextMenuProperties";
            this.contextMenuProperties.Size = new System.Drawing.Size(134, 22);
            this.contextMenuProperties.Text = "&Properties";
            this.contextMenuProperties.ToolTipText = "Edit the properties of the currently selected object";
            this.contextMenuProperties.Click += new System.EventHandler(this.OnContextProperties);
            // 
            // contextMenuDelete
            // 
            this.contextMenuDelete.Name = "contextMenuDelete";
            this.contextMenuDelete.Size = new System.Drawing.Size(134, 22);
            this.contextMenuDelete.Text = "&Delete";
            this.contextMenuDelete.ToolTipText = "Delete the currently selected object";
            this.contextMenuDelete.Click += new System.EventHandler(this.OnContextDelete);
            // 
            // contextMenuRename
            // 
            this.contextMenuRename.Name = "contextMenuRename";
            this.contextMenuRename.Size = new System.Drawing.Size(134, 22);
            this.contextMenuRename.Text = "&Rename";
            this.contextMenuRename.Click += new System.EventHandler(this.OnContextRename);
            // 
            // imageListObjects
            // 
            this.imageListObjects.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListObjects.ImageStream")));
            this.imageListObjects.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListObjects.Images.SetKeyName(0, "");
            this.imageListObjects.Images.SetKeyName(1, "folder-closed_16.png");
            this.imageListObjects.Images.SetKeyName(2, "");
            this.imageListObjects.Images.SetKeyName(3, "");
            this.imageListObjects.Images.SetKeyName(4, "");
            this.imageListObjects.Images.SetKeyName(5, "");
            this.imageListObjects.Images.SetKeyName(6, "");
            this.imageListObjects.Images.SetKeyName(7, "world.bmp");
            // 
            // objectTree
            // 
            this.objectTree.ContextMenuStrip = this.contextMenuStrip1;
            this.objectTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectTree.FullRowSelect = true;
            this.objectTree.HideSelection = false;
            this.objectTree.ImageIndex = 0;
            this.objectTree.ImageList = this.imageListObjects;
            this.objectTree.LabelEdit = true;
            this.objectTree.Location = new System.Drawing.Point(0, 0);
            this.objectTree.Name = "objectTree";
            this.objectTree.SelectedImageIndex = 0;
            this.objectTree.Size = new System.Drawing.Size(193, 183);
            this.objectTree.TabIndex = 1;
            // 
            // ObjectTreeControl
            // 
            this.Controls.Add(this.objectTree);
            this.Name = "ObjectTreeControl";
            this.Size = new System.Drawing.Size(193, 183);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem contextMenuCopy;
        private System.Windows.Forms.ToolStripMenuItem contextMenuPaste;
        private System.Windows.Forms.ToolStripMenuItem contextMenuCut;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem contextMenuProperties;
        private System.Windows.Forms.ToolStripMenuItem contextMenuDelete;
        private System.Windows.Forms.ToolStripMenuItem contextMenuRename;
        private System.Windows.Forms.ImageList imageListObjects;
        private System.Windows.Forms.TreeView objectTree;
    }
}
