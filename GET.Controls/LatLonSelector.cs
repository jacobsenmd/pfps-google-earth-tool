/********************************************************************************/
/* Control:   GET.Controls.LatLonSelector                                       */
/*                                                                              */
/* This control lets a user input a lat-lon in one of several different         */
/* formats.  The code is entirely self-contained and revolves around a LatLon   */
/* object called "Coordinates"                                                  */
/********************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GET.Core;

namespace GET.Controls
{
    public partial class LatLonSelector : UserControl
    {
        #region Delegates
        public delegate void LatLonDelegate();

        public LatLonDelegate OnCoordinatesChanged;

        #endregion

        #region Private Member Data
        private LatLon coordinateTag = new LatLon();
        #endregion

        public LatLon Value
        {
            get { return coordinateTag; }
            set
            {
                LatLon coords = value;
                coordinateTag = coords.GetCopy();
                SetFormValues();                
            }
        }
        public LatLonSelector()
        {
            // this code is confusing as hell, because I accidentally used different numbering systems
            // for the format checkboxes and the different coordinate options available in Statics.CoordinateStyle.
            // This ugly bit of code ensures the LatLonSelector uses the correct style
            InitializeComponent();
            if (Statics.CoordinateStyle == 0)
                radioButtonFormatDDMMSS.Checked = true;
            else if (Statics.CoordinateStyle == 1)
                radioButtonFormatDDMM.Checked = true;
            else
                radioButtonFormatDDDD.Checked = true;
        }

        private void OnValidation(object sender, EventArgs e)
        {
            if (radioButtonFormatDDMMSS.Checked == true)
            {
                // DD MM SS.SSS
                double LatDD = Convert.ToDouble(this.numericLat1Degrees.Value);
                double LatMM = Convert.ToDouble(this.numericLat1Minutes.Value);
                double LatSS = Convert.ToDouble(this.numericLat1Seconds.Value);
                double LonDD = Convert.ToDouble(this.numericLon1Degrees.Value);
                double LonMM = Convert.ToDouble(this.numericLon1Minutes.Value);
                double LonSS = Convert.ToDouble(this.numericLon1Seconds.Value);                

                Value.Assign(this.textBoxLat1NS.Text, LatDD, LatMM, LatSS, this.textBoxLon1EW.Text, LonDD, LonMM, LonSS);
            }
            else if (radioButtonFormatDDDD.Checked == true)
            {
                // DD.DDDDD
                Value.Assign((double)this.numericLat2.Value, (double)this.numericLon2.Value);
            }
            else
            {
                // DD MM.MMM
                double LatDD = Convert.ToDouble(this.numericLat3Degrees.Value);
                // All the casting in the next line truncates the decimal places, giving a round minute
                double LatMM = (double)((int)Convert.ToDouble(this.numericLat3Minutes.Value));
                double LatSS = (Convert.ToDouble(this.numericLat3Minutes.Value) - LatMM) * 60;
                double LonDD = Convert.ToDouble(this.numericLon3Degrees.Value);
                // All the casting in the next line truncates the decimal places, giving a round minute
                double LonMM = (double)((int)Convert.ToDouble(this.numericLon3Minutes.Value));
                double LonSS = (Convert.ToDouble(this.numericLon3Minutes.Value) - LonMM) * 60;
                Value.Assign(this.textBoxLat3NS.Text, LatDD, LatMM, LatSS, this.textBoxLon3EW.Text, LonDD, LonMM, LonSS);
            }
            SetFormValues();
            // Raise an OnCoordinatesChanged event
            if(OnCoordinatesChanged != null)
                OnCoordinatesChanged();
        }

        private void OnFormatChange(object sender, EventArgs e)
        {
            if (radioButtonFormatDDMMSS.Checked == true)
            {
                // DD MM SS.SSS
                this.textBoxLat1NS.Enabled = true;
                this.numericLat1Degrees.Enabled = true;
                this.numericLat1Minutes.Enabled = true;
                this.numericLat1Seconds.Enabled = true;
                this.textBoxLon1EW.Enabled = true;
                this.numericLon1Degrees.Enabled = true;
                this.numericLon1Minutes.Enabled = true;
                this.numericLon1Seconds.Enabled = true;

                this.numericLat2.Enabled = false;
                this.numericLon2.Enabled = false;

                this.textBoxLat3NS.Enabled = false;
                this.numericLat3Degrees.Enabled = false;
                this.numericLat3Minutes.Enabled = false;
                this.textBoxLon3EW.Enabled = false;
                this.numericLon3Degrees.Enabled = false;
                this.numericLon3Minutes.Enabled = false;
            }
            else if (radioButtonFormatDDDD.Checked == true)
            {
                // DD.DDD
                this.textBoxLat1NS.Enabled = false;
                this.numericLat1Degrees.Enabled = false;
                this.numericLat1Minutes.Enabled = false;
                this.numericLat1Seconds.Enabled = false;
                this.textBoxLon1EW.Enabled = false;
                this.numericLon1Degrees.Enabled = false;
                this.numericLon1Minutes.Enabled = false;
                this.numericLon1Seconds.Enabled = false;

                this.numericLat2.Enabled = true;
                this.numericLon2.Enabled = true;


                this.textBoxLat3NS.Enabled = false;
                this.numericLat3Degrees.Enabled = false;
                this.numericLat3Minutes.Enabled = false;
                this.textBoxLon3EW.Enabled = false;
                this.numericLon3Degrees.Enabled = false;
                this.numericLon3Minutes.Enabled = false;
            }
            else
            {
                // DD MM.MMM
                this.textBoxLat1NS.Enabled = false;
                this.numericLat1Degrees.Enabled = false;
                this.numericLat1Minutes.Enabled = false;
                this.numericLat1Seconds.Enabled = false;
                this.textBoxLon1EW.Enabled = false;
                this.numericLon1Degrees.Enabled = false;
                this.numericLon1Minutes.Enabled = false;
                this.numericLon1Seconds.Enabled = false;

                this.numericLat2.Enabled = false;
                this.numericLon2.Enabled = false;


                this.textBoxLat3NS.Enabled = true;
                this.numericLat3Degrees.Enabled = true;
                this.numericLat3Minutes.Enabled = true;
                this.textBoxLon3EW.Enabled = true;
                this.numericLon3Degrees.Enabled = true;
                this.numericLon3Minutes.Enabled = true;
            }
            if (OnCoordinatesChanged != null)
                OnCoordinatesChanged();
        }


        private void SetFormValues()
        {
            // Centerpoint options                        
            this.textBoxLat1NS.Text = Value.LatitudeDirection;
            this.numericLat1Degrees.Value = (decimal)Value.LatitudeDegrees;
            this.numericLat1Minutes.Value = (decimal)Value.LatitudeMinutes;
            this.numericLat1Seconds.Value = (decimal)Value.LatitudeSeconds;

            this.textBoxLon1EW.Text = Value.LongitudeDirection;
            this.numericLon1Degrees.Value = (decimal)Value.LongitudeDegrees;
            this.numericLon1Minutes.Value = (decimal)Value.LongitudeMinutes;
            this.numericLon1Seconds.Value = (decimal)Value.LongitudeSeconds;

            this.numericLat2.Value = (decimal)Value.LatitudeDouble;
            this.numericLon2.Value = (decimal)Value.LongitudeDouble;

            // This code is a bit tricky.  If the user enters a "N" or "S" or "E" or "W" and the rest of the digits
            // are still zero (i.e. the user is working left-to-right as they enter a lat lon), we need to leave the
            // N/S/E/W as is... it should not revert to one or the other because the coordinate is zero.  That's what
            // these if/else-if structures accomplish.
            if (Value.LatitudeDouble != 0)
                this.textBoxLat3NS.Text = Value.LatitudeDirection;
            else if (this.textBoxLat3NS.Text != "N" && this.textBoxLat3NS.Text != "S")
                this.textBoxLat3NS.Text = Value.LatitudeDirection;
            this.numericLat3Degrees.Value = (decimal)Value.LatitudeDegrees;
            this.numericLat3Minutes.Value = (decimal)Value.LatitudeMinutes + (decimal)Value.LatitudeSeconds / 60;
            if(Value.LongitudeDouble != 0)
                this.textBoxLon3EW.Text = Value.LongitudeDirection;
            else if (this.textBoxLon3EW.Text != "E" && this.textBoxLon3EW.Text != "W")
                this.textBoxLon3EW.Text = Value.LongitudeDirection;
            this.numericLon3Degrees.Value = (decimal)Value.LongitudeDegrees;
            this.numericLon3Minutes.Value = (decimal)Value.LongitudeMinutes + (decimal)Value.LongitudeSeconds / 60;
        }

        private void OnValidatingNS(object sender, CancelEventArgs e)
        {
            // This function ensures that the user only enters "N" or "S".
            // Lowercase are automatically converted to uppercase.
            textBoxLat1NS.Text = textBoxLat1NS.Text.ToUpper();
            if (textBoxLat1NS.Text != "N" && textBoxLat1NS.Text != "S")            
                e.Cancel = true;

            textBoxLat3NS.Text = textBoxLat3NS.Text.ToUpper();
            if (textBoxLat3NS.Text != "N" && textBoxLat3NS.Text != "S")
                e.Cancel = true;


            return;
        }

        private void OnValidatingEW(object sender, CancelEventArgs e)
        {
            // This function ensures that the user only enters "E" or "W".
            // Lowercase are automatically converted to uppercase.
            textBoxLon1EW.Text = textBoxLon1EW.Text.ToUpper();
            textBoxLon3EW.Text = textBoxLon3EW.Text.ToUpper();
            if (textBoxLon1EW.Text != "E" && textBoxLon1EW.Text != "W")
                e.Cancel = true;

            textBoxLon3EW.Text = textBoxLon3EW.Text.ToUpper();
            if (textBoxLon3EW.Text != "E" && textBoxLon3EW.Text != "W")
                e.Cancel = true;
            return;
        }
    }
}