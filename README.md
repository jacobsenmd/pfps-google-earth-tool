# PFPS Google Earth Tool

![PFPS Google Earth Tool Example Output](https://i2.wp.com/markdjacobsen.com/wp-content/uploads/2020/04/pfps-google-earth.png?w=1596&ssl=1 "PFPS Google Earth Tool Example Output")

This is legacy Code from a tool I developed in 2008 for integrating DoD flight planning data with Google Earth. 

More information: https://markdjacobsen.com/portfolio/pfps-google-earth-tool/

This is old code, preserved here only for posterity. I was young, self-taught, and wrote this tool during my free time while working full time as a C-17 pilot. The code consequently does not reflect my current abilities. It was not source-controlled, I manually (and painstakingly) constructed all the documentation, and I did not understand design patterns well.

Nonetheless, the tool met a legitimate need, was widely used, and grew into a larger U.S. Air Force project.